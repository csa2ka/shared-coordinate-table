from GUI.Screens.Screen import Screen
import tkinter as tk
import tkinter.ttk as ttk
from GUI.NumberSetter import NumberSetter
#from GUI.OptionSetter import OptionSetter
from GUI.Screens.ScreenType import ScreenType
from Memory.MemoryManager import MemoryManager as mm
from tkinter import Frame, Button, Label
from utils.domi_funcs import fraction_string_to_float
import threading
from utils.domi_funcs import popup_message
from subprocess import call
from utils.balazs_funcs import motor_moving_message

class Communicator:
    # TODO activizálni a dolgokat a settingsben

    FONT_SIZE = 12
    Screen.WIDTH = 800
    Screen.HEIGHT = 480

    ACTIVE_COLOR = (255, 255, 255)  # TODO Gui_elements-be tenni
    INACTIVE_COLOR = (150, 150, 150)

    screens = []
    numpad = None
    keyboard = None
    rpi = None
    app = None
    motor_moving_warning = None
    remembered_value = None

    @classmethod
    def class_init(cls, screens, rpi, app):
        cls.screens = screens
        cls.rpi = rpi
        cls.app = app
        cls.motor_up()  # Motor init

    @classmethod
    def quit_and_shutdown(cls):
        cls.rpi.save_map()
        cls.rpi.turn_off_both()
        cls.app.destroy()
        call("sudo shutdown -h now", shell=True)
    
    @classmethod    
    def quit_to_desktop(cls):
        cls.rpi.save_map()
        cls.rpi.turn_off_both()
        cls.app.destroy()

    @classmethod
    def release_the_poni(cls):
        pass
        # cls.get_screen_by_type(ScreenType.MAIN_MENU).qqqqqqqqqqqq()

    @classmethod
    def refresh_gui_in_settings_3(cls):
        cls.get_screen_by_type(ScreenType.SETTINGS3).reload()

    """
            ==================================
            ||       Gombok Függvényei:     ||
            ==================================
    """

    @classmethod
    def motor_up(cls):
        """
        This is the first function of the motor_thread, this is where the soul of the motor controlling starts,
        the on_tick
        :return:
        """

        motor_thread = threading.Thread(target=cls.rpi.on_tick)
        motor_thread.setDaemon(True)
        motor_thread.start()

        # cls.rpi.set_control_mode(cls.rpi.LINEAR)
        # cls.rpi.on_tick()

    @classmethod
    def go100150(cls):
        """
        A useless function to test cls.rpi.move, and button functions
        :return:
        """
        cls.rpi.move(0, 0)

    @classmethod
    def go150100(cls):
        """
            A useless function to test cls.rpi.move, and button functions
        :return:
        """
        cls.rpi.move(0, 0)

    @classmethod
    def motor_on(cls):
        cls.rpi.x_motor.turn_on()

    @classmethod
    def motor_off(cls):
        cls.rpi.x_motor.turn_off()

    @classmethod
    def joymode_on(cls):
        cls.rpi.switch_to_joystick()

    @classmethod
    def joymode_off(cls):
        cls.rpi.switch_to_idle()

    @classmethod
    def popup(cls, t):
        # print("simple_popup", t)
        cls.get_popup_working(t)
        #popup_message(t)

    @classmethod
    def motor_moving_popup(cls, msg):   # TODO ugly that this is here, sorry should be a Screen or in utils, my bad
        cls.get_motor_moving_popup_working(msg)
        """
        if cls.motor_moving_warning is None:
            width = 800
            height = 480
            large_font = ("Arial", 22)
            popup = tk.Tk()
            popup.overrideredirect(1)
            popup.geometry("%dx%d+0+0" % (width, height))
            popup.wm_title("Message")

            label_wrapper = ttk.Frame(popup, width=width, height=100)
            label_wrapper.place(relx=0.5, rely=0.3, anchor=tk.CENTER)
            label_wrapper.pack_propagate(0)

            label = ttk.Label(label_wrapper, font=large_font, text=msg)
            label.pack(side=tk.BOTTOM)
            #print(2)
            size_wrapper = ttk.Frame(popup, width=150, height=100)
            size_wrapper.place(relx=0.5, rely=0.8, anchor=tk.CENTER)
            size_wrapper.pack_propagate(0)
            B1 = tk.Button(size_wrapper, text="Állj!", command=cls.end_motor_moving_popup)
            B1.pack(side=tk.BOTTOM, expand=True, fill=tk.BOTH)
            B1['font'] = large_font

            B1.bind("<Button-1>", Screen.pitty)
            cls.motor_moving_warning = popup
            popup.mainloop()
            #print(3)
            cls.joymode_on()  # TODO Is this the best way to end a moving command?
        """


    @classmethod
    def end_motor_moving_popup(cls):
        if Screen.previous_screen == ScreenType.MOTOR_MOVING_POPUP:
            cls.get_screen_by_type(ScreenType.MOTOR_MOVING_POPUP).close()
        """
        # print("ended")
        if cls.motor_moving_warning is not None:
            cls.motor_moving_warning.destroy()
            cls.motor_moving_warning = None
            cls.joymode_on()
        """

    @classmethod
    def finnish_setup(cls):
        cls.rpi.switch_to_joystick()
        cls.switch_screen(ScreenType.MAIN_MENU)

    @classmethod
    def x_reset(cls):
        #print("x reset")
        cls.rpi.reset_x()

    @classmethod
    def y_reset(cls):
        #print("y reset")
        cls.rpi.reset_y()

    """
            ==================================
            ||   Settigs és egyéb adathoz   ||
            ||       kellő függvények:      ||
            ==================================
    """

    @classmethod
    def updating_text_value(cls, value_getter):
        return cls.rpi.thread_safe_coordinate_handler(value_getter, 'r')

    @classmethod
    def set_x_min_from_border(cls, value):
        cls.rpi.thread_safe_coordinate_handler(cls.rpi.map.set_until_x_min_border, 'w',
                                               round(float(
                                                   value) * cls.rpi.get_y_step_mm() * cls.rpi.get_microstep_software()))
        # mm.get_instance().change(mm.FROM_X_MIN_BORDER, value)

    @classmethod
    def set_x_plus_from_border(cls, value):
        cls.rpi.thread_safe_coordinate_handler(cls.rpi.map.set_until_x_plus_border, 'w',
                                               round(float(
                                                   value) * cls.rpi.get_x_step_mm() * cls.rpi.get_microstep_software()))
        # mm.get_instance().change(mm.FROM_X_PLUS_BORDER, value)

    @classmethod
    def set_y_min_from_border(cls, value):
        cls.rpi.thread_safe_coordinate_handler(cls.rpi.map.set_until_y_min_border, 'w',
                                               round(float(
                                                   value) * cls.rpi.get_y_step_mm() * cls.rpi.get_microstep_software()))
        # mm.get_instance().change(mm.FROM_Y_MIN_BORDER, value)

    @classmethod
    def set_y_plus_from_border(cls, value):
        cls.rpi.thread_safe_coordinate_handler(cls.rpi.map.set_until_y_plus_border, 'w',
                                               round(float(
                                                   value) * cls.rpi.get_y_step_mm() * cls.rpi.get_microstep_software()))
        # mm.get_instance().change(mm.FROM_Y_PLUS_BORDER, value)

    @classmethod
    def set_current_position_x(cls, value):
        cls.rpi.thread_safe_coordinate_handler(cls.rpi.map.set_x, 'w', round(
            float(value) * cls.rpi.get_x_step_mm() * cls.rpi.get_microstep_software()))
        # mm.get_instance().change(mm.CUR_X_POS, value)

    @classmethod
    def set_current_position_y(cls, value):
        cls.rpi.thread_safe_coordinate_handler(cls.rpi.map.set_y, 'w', round(
            float(value) * cls.rpi.get_y_step_mm() * cls.rpi.get_microstep_software()))
        # mm.get_instance().change(mm.CUR_Y_POS, value)

    @classmethod
    def save_map_variable(cls, value, key):
        mm.get_instance().change(key, value)

    @classmethod
    def set_control_mode(cls, value, init=False):
        if value == mm.AXIAL:
            cls.rpi.set_control_mode(cls.rpi.AXIAL)
        elif value == mm.LINEAR:
            cls.rpi.set_control_mode(cls.rpi.LINEAR)
        elif value == mm.DEG45:
            cls.rpi.set_control_mode(cls.rpi.DEG45)
        mm.get_instance().change(mm.CONTROL_MODE, value)
        return True

    @classmethod
    def manual_setup(cls, phase):
        cls.rpi.manual_setup(phase)

    @classmethod
    def set_microstep_setter_enable(cls, value, init=False):  # bool kell
        cls.rpi.set_microstep_setter_enable(value == mm.YES)  # lehet hogy a nem Igen, nem Nem
        mm.get_instance().change(mm.MICROSTEP_ENABLE, value)
        return True

    @classmethod
    def set_software_microstep(cls, value, init=False):
        v = 1 / fraction_string_to_float(value)
        # if cls.rpi.get_microstep_hardware() < v and not init:
        #    return False
        cls.rpi.set_microstep_software(v)
        mm.get_instance().change(mm.MICROSTEP_SW, value)
        return True

    @classmethod
    def set_hardware_microstep(cls, value, init=False):
        v = 1 / fraction_string_to_float(value)
        # if cls.rpi.get_microstep_software() > v and not init:
        #    return False
        cls.rpi.set_microstep_hardware(v)
        mm.get_instance().change(mm.MICROSTEP_HW, value)
        cls.set_software_microstep(value)
        cls.switch_screen(ScreenType.MICROSTEP_SETUP)
        return True

    @classmethod
    def set_xmotor_step_per_mm(cls, value):
        cls.rpi.set_x_step_mm(float(value))
        mm.get_instance().change(mm.XMOTOR_STEP_MM, value)
        cls.switch_screen(ScreenType.MICROSTEP_SETUP)

    @classmethod
    def set_ymotor_step_per_mm(cls, value):
        cls.rpi.set_y_step_mm(float(value))
        mm.get_instance().change(mm.YMOTOR_STEP_MM, value)
        cls.switch_screen(ScreenType.MICROSTEP_SETUP)

    @classmethod
    def set_overtravel_switch(cls, value, init=False):  # bool kell
        cls.rpi.set_overtravel_switch(value == mm.YES)
        mm.get_instance().change(mm.OVERTRAVEL_SWITCH, value)
        return True

    @classmethod
    def set_x_motor_base_direction(cls, value, init=False):
        real_value = None
        if value == mm.X_RIGHT:
            real_value = 1
        if value == mm.X_LEFT:
            real_value = -1
        cls.rpi.set_x_motor_base_direction(real_value)
        mm.get_instance().change(mm.X_MOTOR_DIRECTION, value)
        if cls.remembered_value != value:
            cls.switch_screen(ScreenType.MICROSTEP_SETUP)
        return True

    @classmethod
    def set_y_motor_base_direction(cls, value, init=False):
        real_value = None
        if value == mm.Y_UP:
            real_value = 1
        if value == mm.Y_DOWN:
            real_value = -1
        cls.rpi.set_y_motor_base_direction(real_value)
        mm.get_instance().change(mm.Y_MOTOR_DIRECTION, value)
        if cls.remembered_value != value:
            cls.switch_screen(ScreenType.MICROSTEP_SETUP)
        return True

    @classmethod
    def set_delay(cls, value):
        """
        :param value:
        :return:
        """
        cls.rpi.set_delay(float(value))
        mm.get_instance().change(mm.DELAY, value)

    @classmethod
    def set_text(cls, value):
        # print(value)
        pass

    """
            ==================================
            ||           Egyéb:             ||
            ==================================
    """

    @classmethod
    def to(cls):
        """
        It's the motor_off() but shorter because it's faster to type this when needed
        :return:
        """
        cls.rpi.x_motor.turn_off()

    """
    
            ==================================
            ||       Screennavigáció:       ||
            ==================================
    """

    @classmethod
    def numpad_come_out(cls, target, value_setter_function, arg=None, floating_digits=3):
        d = floating_digits
        cur_screen = Screen.previous_screen
        cls.remembered_value = target["text"]
        cls.switch_screen(ScreenType.NUMPAD)
        cls.get_screen_by_type(ScreenType.NUMPAD).get_numpad_working(target, target['text'], cur_screen,
                                                                     value_setter_function, arg, floating_digits)

    @classmethod
    def keyboard_come_out(cls, target, value_setter_function, arg=None, blacklist=None):
        cur_screen = Screen.previous_screen
        cls.remembered_value = target["text"]
        cls.switch_screen(ScreenType.KEYBOARD)
        cls.get_screen_by_type(ScreenType.KEYBOARD).start_keyboard(target, target['text'], cur_screen,
                                                                   value_setter_function, arg, blacklist)

    @classmethod
    def get_popup_working(cls, warning_message):
        # print("get_popup_working")
        prev_screen = Screen.previous_screen
        cls.switch_screen(ScreenType.POPUP)
        cls.get_screen_by_type(ScreenType.POPUP).open_up(warning_message, prev_screen)

    @classmethod
    def close_popup(cls, target_screen):
        cls.switch_screen(target_screen)

    @classmethod
    def get_motor_moving_popup_working(cls, warning_message):
        # print("get_popup_working")
        prev_screen = Screen.previous_screen
        cls.switch_screen(ScreenType.MOTOR_MOVING_POPUP)
        cls.get_screen_by_type(ScreenType.MOTOR_MOVING_POPUP).open_up(warning_message, prev_screen)

    @classmethod
    def close_motor_moving_popup(cls, target_screen):
        cls.switch_screen(target_screen)
        cls.rpi.switch_to_joystick()
        cls.rpi.save_map()

    @classmethod
    def get_screen_by_type(cls, sc_type):
        for sc in cls.screens:
            if sc.type == sc_type:
                return sc
        raise NameError("Nincs ilyen screen!")

    @classmethod
    def switch_screen(cls, target):
        """
        Hides every screen, shows target screen, sets current_screen to target
        :param target: a ScreenType enum
        :return:
        """
        next_screen = cls.get_screen_by_type(target)
        next_screen.show()
        for screen in cls.screens:
            if next_screen is not screen:
                screen.hide()
        Screen.previous_screen = target
        cls.joystickhandling(next_screen)

    @classmethod
    def joystickhandling(cls, next_screen):
        if next_screen == cls.get_screen_by_type(
                ScreenType.MAIN_MENU):
            cls.joymode_off()
        elif next_screen == cls.get_screen_by_type(
                ScreenType.MANUAL_SETUP) or next_screen == cls.get_screen_by_type(
                ScreenType.COORDINATE_MODE) or next_screen == cls.get_screen_by_type(
                ScreenType.ROTARY_TABLE_MODE) or next_screen == cls.get_screen_by_type(
                ScreenType.CAM_N_DRILL_MODE):
            cls.joymode_on()
        if next_screen == cls.get_screen_by_type(
                ScreenType.MANUAL_SETUP):
            cls.rpi._MANUAL = True
        else:
            cls.rpi._MANUAL = False

    """
            =====================================
            ||       Irányítási parancsok:     ||
            =====================================
    """

    @classmethod
    def mid_setup(cls):
        # print("commimidsetup")
        cls.rpi.set_setup_mode("mid")
        cls.rpi.switch_to_setup()
        cls.motor_moving_popup("Határok felvétele. Leállítás esetén újra kell csinálni!")

    @classmethod
    def corner_setup(cls):
        # print("commimidsetup")
        cls.rpi.set_setup_mode("corner")
        cls.rpi.switch_to_setup()
        cls.motor_moving_popup("Határok felvétele. Leállítás esetén újra kell csinálni!")

    @classmethod
    def go_to(cls, x, y):
        cls.rpi.target_move(x, y)

    @classmethod
    def go_to_rotary(cls, r, p, s, n, c):
        return cls.rpi.rotary_move(r, p, s, n, c)  # it returns False if not possible

    @classmethod
    def go_this_much(cls, x, y):
        cls.rpi.go_this_much(x, y)

    @classmethod
    def read_SD(cls):
        returning_value = mm.get_instance().read_SD()
        everything_is_fine = returning_value[0]
        message = returning_value[1]
        if not everything_is_fine:
            cls.popup(message)

    @classmethod
    def write_SD(cls):
        mm.get_instance().write_SD()
