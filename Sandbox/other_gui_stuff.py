import tkinter as tk
from functools import partial


def show(what, hiding_frame):
    what.lift(hiding_frame)


def hide(what, hiding_frame):
    what.lower(hiding_frame)


def btn1(a):
    hide(fullFrame, fullFrameHider)
    show(fullFrame2, fullFrameHider)
    some_button2.bind("<Button-1>", printer)
    # com = some_button2['command']
    # some_button2.configure(command=lambda: [com, printer()])


def printer(event):
    print("ja")


def btn2():
    hide(fullFrame2, fullFrameHider)
    show(fullFrame, fullFrameHider)


root = tk.Tk()

root.title('The game')
#You can set the geometry attribute to change the root windows size
root.geometry("700x500") #You want the size of the app to be 500x500
root.resizable(0, 0) #Don't allow resizing in the x or y direction
root.configure(cursor='none')

fullFrameHider = tk.Frame(root, width=500, height=500, relief='raised', borderwidth=5)
fullFrameHider.pack_propagate(0)
fullFrame = tk.Frame(root, width=385, height=460, relief='raised', borderwidth=10)
fullFrame.pack_propagate(0)
btnHider = tk.Frame(fullFrame, width=385, height=460, relief='raised', borderwidth=5)

fullFrameHider.pack()
# fullFrame.pack(in_=fullFrameHider)
fullFrame.place(in_=fullFrameHider, relx=0.5, rely=0.5, anchor=tk.CENTER, relheight=1, relwidth=1)
btnHider.pack()

fullFrame2 = tk.Frame(root, width=385, height=460, relief='raised', borderwidth=10)
btnHider2 = tk.Frame(fullFrame2, width=385, height=460, relief='raised', borderwidth=5)

# fullFrame2.pack(in_=fullFrameHider)
fullFrame2.place(in_=fullFrameHider, relx=0.5, rely=0.5, anchor=tk.CENTER, relheight=1, relwidth=1)
btnHider2.pack()


some_frame = tk.Frame(fullFrame, relief='raised', borderwidth=1)
# Creating a photoimage object to use image
photo = tk.PhotoImage(file=r"..\img\Icons\thrash2.png")

some_button = tk.Button(some_frame, text='Button', command=partial(btn1, "semmi"))
some_button2 = tk.Button(fullFrame2, image=photo, text='Button2', command=btn2)

some_frame.pack(in_=btnHider)
some_button.pack()
some_button2.pack(in_=btnHider2)

#hide(some_button2, btnHider)
#show(some_button2, btnHider)
hide(fullFrame2, fullFrameHider)
show(fullFrame, fullFrameHider)

root.mainloop()
