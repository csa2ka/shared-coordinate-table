import RPi.GPIO as GPIO
from time import sleep
import threading
import math
from Raspberry_control.Map import Map
from Raspberry_control.Buzzer import Buzzer
from Raspberry_control.Buttonlike import Buttonlike
from Raspberry_control.Joystick import Joystick
from Raspberry_control.StepperMotor import StepperMotor
from Raspberry_control.CleverButton import CleverButton
from Communicator import Communicator


class RaspberryController:
    a_malnan_vagyunk = False

    # counter=0

    """
    Két motor mozgatásáért felelős class
    """
    # Motorok:
    _X_DIR_PIN_NUMBER = 22
    _X_STEP_PIN_NUMBER = 24
    _Y_DIR_PIN_NUMBER = 27
    _Y_STEP_PIN_NUMBER = 23
    _ENABLE_PIN_NUMBER = 17

    _MICRO1_PIN_NUMBER = 14
    _MICRO2_PIN_NUMBER = 15
    _MICRO3_PIN_NUMBER = 18

    _BUZZER_PIN_NUMBER = 4

    _SWITCH_X_MIN_PIN_NUMBER = 16  # végálláskapcsolók pinjei
    _SWITCH_X_PLUS_PIN_NUMBER = 13
    _SWITCH_Y_MIN_PIN_NUMBER = 12
    _SWITCH_Y_PLUS_PIN_NUMBER = 6

    _J_X_MIN_PIN_NUMBER = 21  # joystick pinjei
    _J_X_PLUS_PIN_NUMBER = 26
    _J_Y_MIN_PIN_NUMBER = 20
    _J_Y_PLUS_PIN_NUMBER = 19

    Y_NULL_BUTTON_PIN_NUMBER = 8  # not sure wich is wich

    X_NULL_BUTTON_PIN_NUMBER = 5

    JOYSTICK_SPEED1 = 0.3

    JOYSTICK_SPEED_CHANGE1 = 5
    JOYSTICK_SPEED2 = 0.08

    JOYSTICK_SPEED_CHANGE2 = 15
    JOYSTICK_SPEED3 = 0.01

    JOYSTICK_SPEED_CHANGE3 = 25
    JOYSTICK_SPEED4 = 0.005

    # counter = 0

    base_tick = 0.1
    joystick_tick = 0.3

    # Beállítások:                              (hosszú nevük van, mert ritkán kell leírni őket)
    """Vezérlés, point-to-point control. 3 lesz: AXIAL, DEG45 és LINEAR. Szakasz, Pont és Pálya"""
    LINEAR = "LINEAR"
    DEG45 = "DEG45"
    AXIAL = "AXIAL"
    _p2p_control = AXIAL
    """Egy bool, hogy van-e vagy nincs végálláskapcsoló."""
    _overtravel_switch = True  # lehetne beszédesebb az elnevezés
    """Microstep: Hű, ne ez vacak egy kicsit, mert pinekkel lehetne állítgatni, de valszeg be lesz állítva
    hardweresen egy félére és lehet kérni, hogy szoftveresen azt hagyjuk figyelmen kívül.
        Hardveres: Full step --> 16-os     Egyre halkabb, kevesebb rezgés a motortól
        Softveres: Ha már a hardweresen nagy (4,k,16) microstep van akkor csoportosan léptetve alacsony microsteppet
                lehet csinálni, negítívuma az hogy lassabb (van minimimum delay: 1 micro sec)
    A softveres microstep  adott mozgatási fajtától is függhet."""
    _microstep_setter_enabled = True

    _microstep_hardware = 1
    _microstep_software = 1
    """step/mm"""
    _x_step_mm = 100
    _y_step_mm = 100
    """léptetési idő, min 0.000001"""
    _delay = 0.001  # 0.001  egészen optimális fullsteppes hardveres microstep esetén

    """Irányítási konstansok:"""
    # _J_On = False
    # _SETUP = False
    # _TARGET_MOVE = False
    _JOYSTICK_MODE = "JOYSTICK"
    _SETUP_MODE = "SETUP"
    _TARGET_MODE = "TARGET"
    _ROTATION_MODE = "ROTATION"
    _IDLE_MODE = "IDLE"
    _END_MODE = "END"
    _MODE = "STARTING"
    x_t = 0
    y_t = 0
    radius = 0
    _setup_mode = "mid"

    coordinate_lock = threading.Lock()
    mode_lock = threading.Lock()

    def __init__(self):

        # print("controlinit!!")
        self.map = Map(0, 0, self)  # TODO ez hogy működik??
        GPIO.setmode(GPIO.BCM)

        self.buzzer = Buzzer(self._BUZZER_PIN_NUMBER)
        self.x_motor = StepperMotor(self, self._X_STEP_PIN_NUMBER, self._X_DIR_PIN_NUMBER, self._ENABLE_PIN_NUMBER,
                                    self._microstep_setter_enabled, self._MICRO1_PIN_NUMBER, self._MICRO2_PIN_NUMBER,
                                    self._MICRO3_PIN_NUMBER)
        self.y_motor = StepperMotor(self, self._Y_STEP_PIN_NUMBER, self._Y_DIR_PIN_NUMBER, self._ENABLE_PIN_NUMBER,
                                    self._microstep_setter_enabled, self._MICRO1_PIN_NUMBER, self._MICRO2_PIN_NUMBER,
                                    self._MICRO3_PIN_NUMBER)