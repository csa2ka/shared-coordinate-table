import string


def exception_check():
    while True:
        try:
            x = int(input("Please enter a number: "))
            break
        except ValueError:
            pass
            # print("Oops!  That was no valid number.  Try again...")


def files_are_valid(f1_n, f2_n, f3_n):
    # settings_filename = "settings.txt"
    # points_filename = "point_groups.txt"
    # rotary_filename = "rotary_tables.txt"
    # file_names = [f1_n, f2_n, f3_n]
    try:
        file_names = ["settings.txt", "point_groups.txt", "rotary_tables.txt"]
        arrays = []
        a = None
        for i in range(3):  # 0,1,2
            an_array = []
            arrays.append(an_array)
            with open(file_names[i], 'r') as f:
                for line in f.readlines():
                    arrays[i].append(line.split(','))
            f.close()
            for j in range(len(arrays[i])):
                data_number = 4  # in case of points, and rotary
                if i == 0:
                    data_number = 2  # in case of settings
                for k in range(data_number):
                    a = arrays[i][j][k]
                    if len(a.translate({ord(c): None for c in string.whitespace})) == 0:
                        # print("ATYAGATYA!!")
                        # print(i, j, k, a)
                        # print("0 hosszú")
                        return False
                    # print(i, j, k, a)
                    # print(len(a), len(a.translate({ord(c): None for c in string.whitespace})))
        return True
    except IndexError:
        # print("Exception!!")
        return False


# print(files_are_valid(1, 2, 3))
