import os
import shutil
from pathlib import Path, PureWindowsPath
import time


def copy_memory_files(to_path):
    to_settings = universal_path(to_path + "\\settings.txt")
    to_point_groups = universal_path(to_path + "\\point_groups.txt")
    to_rotary_tables = universal_path(to_path + "\\rotary_tables.txt")

    shutil.copy(universal_path("..\\settings.txt"), to_settings)
    shutil.copy(universal_path("..\\point_groups.txt"), to_point_groups)
    shutil.copy(universal_path("..\\rotary_tables.txt"), to_rotary_tables)


def universal_path(file_path):
    filename_in = PureWindowsPath(file_path)
    return Path(filename_in)


def is_this_exist(file_path):
    # I've explicitly declared my path as being in Windows format, so I can use forward slashes in it.
    filename_in = PureWindowsPath(file_path)
    # filename_out = PureWindowsPath("settings.txt")

    # Convert path to the right format for the current operating system
    correct_path_in = Path(filename_in)
    # correct_path_out = Path(filename_out)

    print(correct_path_in)
    # print(correct_path_out)

    if os.path.exists(correct_path_in):
        # print("Létezik!")
        pass

    # if os.path.exists("alma.txt"):
    #    print("a.txt")


def stringing_date(number):
    if number < 10:
        return "0" + str(number)
    else:
        return str(number)

def time_text():
    timi = time.localtime()
    string = stringing_date(timi.tm_year) \
             + stringing_date(timi.tm_mon) \
             + stringing_date(timi.tm_mday) \
             + stringing_date(timi.tm_hour) \
             + stringing_date(timi.tm_min) \
             + stringing_date(timi.tm_sec)
    return string

def backupsave():
    #getsmallest
    folder_path = "backup"
    smallest = "99991231245959"
    for i in os.listdir(folder_path):
        try:
            if int(i) < int(smallest):
                smallest = i
        except ValueError:
            pass
    path = folder_path + "\\" + smallest
    #print(path)
    #renamesmallest
    copy_memory_files(path)
    #savestuffthere
    os.rename(universal_path(path), universal_path(folder_path + "\\" + time_text()))

backupsave()

#print(time_text())

# copy_memory_files("backup\\1")
# for i in range(k):
# os.mkdir(universal_path("backup\\" + str(k)))
