#!/usr/bin/python3
import RPi.GPIO as GPIO
from time import sleep


def turn_off(eni):
    GPIO.output(eni, GPIO.HIGH)


def turn_on(eni):
    GPIO.output(eni, GPIO.LOW)


dire = 22
step = 24
enable = 17

GPIO.setmode(GPIO.BCM)
GPIO.setup(enable, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(step, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(dire, GPIO.OUT, initial=GPIO.LOW)


turn_on(enable)

for i in range(100):
    sleep(1)
    pass
