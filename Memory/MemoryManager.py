from Memory.PointGroup import PointGroup
from Memory.RotaryTable import RotaryTable
from utils.balazs_funcs import spec_format, copy_memory_files, universal_path, is_this_exist, stringing_date, time_text
import os
import sys
import string


class MemoryManager:
    FORMAT = 3
    """Kezeli a fileba írást és olvasát, és az adatokat"""
    """segéd vackok"""
    array2D = []
    array2D_floats = []

    CONTROL_MODE = "vezerles"
    DEG45 = "pont"
    LINEAR = "palya"
    AXIAL = "szakasz"

    MICROSTEP_HW = "microstep hw"
    MICROSTEP_SW = "microstep sw"
    FULLSTEP = "1"
    HALFSTEP = "1/2"
    QUARTERSTEP = "1/4"
    EIGHTHSTEP = "1/8"
    SIXTEENTHSTEP = "1/16"

    MICROSTEP_ENABLE = "microstep allithato"
    OVERTRAVEL_SWITCH = "vegallaskapcsolo"
    YES = "igen"
    NO = "nem"

    XMOTOR_STEP_MM = "x motor speed (step/mm)"
    YMOTOR_STEP_MM = "y motor speed (step/mm)"

    DELAY = "lepes ido (s)"  # TODO this has changed now it's mm/s, a speed

    FROM_X_PLUS_BORDER = "pozitiv x hatar (mm)"
    FROM_X_MIN_BORDER = "negativ x hatar (mm)"
    FROM_Y_PLUS_BORDER = "pozitiv y hatar (mm)"
    FROM_Y_MIN_BORDER = "negativ y hatar (mm)"

    CUR_X_POS = "aktualis x pozicio (mm)"
    CUR_Y_POS = "aktualis y pozicio (mm)"

    X_MOTOR_DIRECTION = "x motor direction"
    Y_MOTOR_DIRECTION = "y motor direction"

    X_LEFT = "xleft"
    X_RIGHT = "xright"
    Y_UP = "yup"
    Y_DOWN = "ydown"

    """
    vezérlés,szakasz
    microstep sw, 1
    microstep hw, 1
    """
    base_settings = [
        ["vezérlés", "szakasz"],
        ["microstep sw", "1"],
        ["microstep hw", "1"]
    ]

    """fontos dolgok"""
    settings_data = []
    settings_data_file_name = ""

    """
    Fromátum:  (ékezetek nélkül lehetőleg)
    név, adat
    név2, adat2
    ...
    """

    points_data = []
    points_data_file_name = ""

    groups = []

    """
       Fromátum:  (ékezetek nélkül lehetőleg)
       csoportnév1, pontnév1, x, y
       csoportnév1, pontnév2, x, y
       csoportnév1, pontnév3, x, y
       csoportnév2, pontnév1, x, y
       csoportnév2, pontnév2, x, y
       ...
    """

    rotary_tables_data = []
    rotary_tables_file_name = ""
    rotary_tables = []

    """
        Fromátum:  (ékezetek nélkül lehetőleg)
       körasztalnév 1, sugár, osztás, kihagy
       körasztalnév 2, sugár, osztás, kihagy
       körasztalnév 3, sugár, osztás, kihagy
       ...
    """
    SECURITY_COPY_TEXT = "_security_copy"
    __instance = None

    def __init__(self, settings_data_file_name, points_data_file_name, rotary_tables_file_name):
        self.settings_data_file_name = settings_data_file_name
        self.points_data_file_name = points_data_file_name
        self.rotary_tables_file_name = rotary_tables_file_name

        # self.refresh_all_data()

        self.points_data = self.get_data(self.points_data_file_name)
        self.settings_data = self.get_data(self.settings_data_file_name)
        self.rotary_tables_data = self.get_data(self.rotary_tables_file_name)

        self.group_up_the_points()
        self.arrange_rotary_tables()

        self.save_data(self.points_data_file_name, self.points_data)
        self.save_data(self.rotary_tables_file_name, self.rotary_tables_data)

        self.__class__.__instance = self

    @classmethod
    def get_instance(cls):
        if cls.__instance is not None:
            return cls.__instance
        else:
            raise NotImplementedError("Hé, csinálj előbb példányt!")

    def arrange_rotary_tables(self):
        """
            Dinamukis rendszrbe írja át a két dimnenziós tömböta körsztal esetében
        """
        for line in self.rotary_tables_data:
            rt = RotaryTable(line[0], line[1], line[2], line[3])
            self.rotary_tables.append(rt)

    def overwrite_groups_data(self, array):
        # print("itt jártam")
        self.groups = array
        self.save_data(self.points_data_file_name, self.points_data)

    def overwrite_rtables_data(self, array):
        # print("itt jártam")
        self.rotary_tables = array
        self.save_data(self.rotary_tables_file_name, self.rotary_tables_data)

    def change_group_name(self, prev_name, new_name):
        for g in self.groups:
            if g.get_name() == prev_name:
                g.set_name(new_name)
        self.save_data(self.points_data_file_name, self.points_data)

    def change_point(self, group_name, prev_name, new_name, new_x, new_y):
        for g in self.groups:
            if g.get_name() == group_name:
                for p in g.points:
                    if p.get_name() == prev_name:
                        p.set_name(new_name)
                        p.set_x(new_x)
                        p.set_y(new_y)
        self.save_data(self.points_data_file_name, self.points_data)

    def add_group(self, name, starting_point, x, y):
        pg = PointGroup(name)
        pg.add_point(starting_point, x, y)
        self.groups.append(pg)

    def remove_group(self, name):
        groups = self.groups.copy()
        self.groups.clear()
        for g in groups:
            if not g.name == name:
                self.groups.append(g)

    def is_floatable(self, value):
        try:
            float(value)
            return True
        except:
            return False

    def is_toStringable(self, value):
        try:
            str(value)
            return True
        except:
            return False

    def array2D_to_string(self, array2D):
        s = ""
        for line in array2D:
            l = ""
            for e in range(len(line)):
                if not line[e] == "\n":
                    if self.is_toStringable(line[e]):
                        l += str(line[e]) + ","
                    else:
                        l += line[e] + ","
            # l=l[:-1]
            s += l + "\n"
        return s

    def group_up_the_points(self):
        """
        A dinamukis rendszrbe írja át a két dimnenziós tömböt
        """
        # print(self.points_data)
        for line in self.points_data:
            there_is_one_with_this_name_already = False
            for g in self.groups:
                if g.get_name() == line[0]:
                    there_is_one_with_this_name_already = True
                    g.add_point(line[1], line[2], line[3])
            if not there_is_one_with_this_name_already:
                pg = PointGroup(line[0])
                pg.add_point(line[1], line[2], line[3])
                self.groups.append(pg)
        # print("jjjj")
        # print(self.groups)
        # self.groups[0].set_name("hali")

    def save_into_array_points(self):
        self.points_data.clear()
        for g in self.groups:
            for p in g.points:
                self.points_data.append([p.group, p.name, spec_format(p.x, self.FORMAT), spec_format(p.y, self.FORMAT)])

    def save_into_array_rtables(self):
        self.rotary_tables_data.clear()
        for rt in self.rotary_tables:
            # print([rt.name, rt.radius, rt.partition, rt.skip])
            self.rotary_tables_data.append([rt.name, spec_format(rt.radius, self.FORMAT), rt.partition, rt.skip])

    def get_data(self, file_name):
        self.array2D.clear()
        with open(file_name, 'r') as f:
            for line in f.readlines():
                self.array2D.append(line.split(','))
        f.close()

        self.array2D_floats.clear()
        for line in self.array2D:
            s_floats = []
            for s in line:
                if self.is_floatable(s):
                    s_floats.append(float(s))
                else:
                    s_floats.append(s)
            self.array2D_floats.append(s_floats)
        return self.array2D.copy()

    def save_data(self, file_name, array, with_over_writing=True):  # ,filename,array):
        """
        :param with_over_writing: Normally true, but for the refreshing this needs to be done in a previous step
        :param array: ezt a tömböt fogja beleírni (2D array, 2D List)
        :param file_name: ebbe a file-ba írja (string)
        """
        if with_over_writing:
            if file_name == self.points_data_file_name:
                self.save_into_array_points()
            elif file_name == self.rotary_tables_file_name:
                self.save_into_array_rtables()
        with open(file_name, 'w') as f:
            f.write(self.array2D_to_string(array))
        f.close()
        self.backup_memory()

    def backup_memory(self):
        # getsmallest
        folder_path = "backup"
        smallest = "99991231245959"
        biggest = "0"
        for i in os.listdir(folder_path):
            try:
                if int(i) < int(smallest):
                    smallest = i
                if int(i) > int(biggest):
                    biggest = i
            except ValueError:
                pass
        path = folder_path + "\\" + smallest
        if biggest == time_text():
            pass
        else:
            # print(path)
            # renamesmallest
            copy_memory_files(path)
            # savestuffthere
            os.rename(universal_path(path), universal_path(folder_path + "\\" + time_text()))
        # bashCommand = "sudo cp -r ~/CoordinateTable/backup/ ~/../../boot/backup"
        # os.system(bashCommand)

    def change(self, key, new_value):
        # print("change: ", key, new_value)
        for line in self.settings_data:
            if line[0] == key:
                line[1] = new_value
        self.save_data(self.settings_data_file_name, self.settings_data)

    def files_are_valid(self):
        # settings_filename = "settings.txt"
        # points_filename = "point_groups.txt"
        # rotary_filename = "rotary_tables.txt"
        # file_names = [f1_n, f2_n, f3_n]
        # file_names = [self.settings_data_file_name, self.points_data_file_name, self.rotary_tables_file_name]
        # file_names = ["settings.txt", "point_groups.txt", "rotary_tables.txt"]
        current_i = 0
        current_j = 0
        current_k = 0
        try:
            sc = self.SECURITY_COPY_TEXT
            file_names = [self.settings_data_file_name[:-4] + sc + ".txt",
                          self.points_data_file_name[:-4] + sc + ".txt",
                          self.rotary_tables_file_name[:-4] + sc + ".txt"]
            self.copy_SD_card_files(security_copy=True)
            arrays = []
            for i in range(3):  # 0,1,2
                current_i = i
                an_array = []
                arrays.append(an_array)
                with open(file_names[i], 'r') as f:
                    for line in f.readlines():
                        arrays[i].append(line.split(','))
                f.close()
                for j in range(len(arrays[i])):
                    current_j = j
                    data_number = 4  # in case of points, and rotary
                    if i == 0:
                        data_number = 2  # in case of settings
                    for k in range(data_number):
                        current_k = k
                        a = arrays[i][j][k]
                        if len(a.translate({ord(c): None for c in string.whitespace})) == 0:
                            # print("ATYAGATYA!!")
                            # print(i, j, k, a)
                            # print("0 hosszú")
                            return False, self.compute_error_message(current_i, current_j, current_k)
                        # print(i, j, k, a)
                        # print(len(a), len(a.translate({ord(c): None for c in string.whitespace})))
            return True, ""
        except IndexError:
            # print("Exception!!")
            return False, self.compute_error_message(current_i, current_j, current_k)

    def compute_error_message(self, i, j, k):
        filename = ""
        if i == 0:
            filename = self.settings_data_file_name
        if i == 1:
            filename = self.points_data_file_name
        if i == 2:
            filename = self.rotary_tables_file_name
        return "A " + filename + " " + str(j) + ". sora nem megfelelő."

    def copy_SD_card_files(self, security_copy=False):
        sc = ""
        if security_copy:
            sc = self.SECURITY_COPY_TEXT
        bash_commands = ["sudo rm ~/CoordinateTable/settings" + sc + ".txt",
                         "sudo rm ~/CoordinateTable/point_groups" + sc + ".txt",
                         "sudo rm ~/CoordinateTable/rotary_tables" + sc + ".txt",
                         "sudo cp ~/../../boot/CoordinateTableData/in/settings.txt ~/CoordinateTable/settings" + sc + ".txt",
                         "sudo cp ~/../../boot/CoordinateTableData/in/point_groups.txt ~/CoordinateTable/point_groups" + sc + ".txt",
                         "sudo cp ~/../../boot/CoordinateTableData/in/rotary_tables.txt ~/CoordinateTable/rotary_tables" + sc + ".txt",
                         "sudo chmod 777 ~/CoordinateTable/settings" + sc + ".txt",
                         "sudo chmod 777 ~/CoordinateTable/point_groups" + sc + ".txt",
                         "sudo chmod 777 ~/CoordinateTable/rotary_tables" + sc + ".txt",
                         ]
        for b_c in bash_commands:
            os.system(b_c)

    def read_SD(self):  # dangerous
        returning_values = self.files_are_valid()
        # print(returning_values)
        valid = returning_values[0]
        error_message = returning_values[1]
        if valid:
            self.copy_SD_card_files()
            # bash_commands = ["sudo rm ~/CoordinateTable/settings.txt",
            #                 "sudo rm ~/CoordinateTable/point_groups.txt",
            #                 "sudo rm ~/CoordinateTable/rotary_tables.txt",
            #                 "sudo cp ~/../../boot/CoordinateTableData/in/settings.txt ~/CoordinateTable/settings.txt",
            #                 "sudo cp ~/../../boot/CoordinateTableData/in/point_groups.txt ~/CoordinateTable/point_groups.txt",
            #                 "sudo cp ~/../../boot/CoordinateTableData/in/rotary_tables.txt ~/CoordinateTable/rotary_tables.txt",
            #                 "sudo chmod 777 ~/CoordinateTable/settings.txt",
            #                 "sudo chmod 777 ~/CoordinateTable/point_groups.txt",
            #                 "sudo chmod 777 ~/CoordinateTable/rotary_tables.txt",
            #                 ]
            # for b_c in bash_commands:
            #    os.system(b_c)

            os.execv(sys.executable, ['python'] + sys.argv)
            # self.refresh_all_data()  # ugly phuj!!
            # print("reeee")
            return True, ""
        else:
            # everything_is_fine, message   aka  was there an error in the file, and what
            return False, error_message

    def refresh_all_data(self):
        pass
        """
        print("MM1: ", len(self.points_data))
        # self.points_data.clear()
        self.rotary_tables_data.clear()
        self.settings_data.clear()

        print("MM2: ", len(self.points_data))
        self.points_data = self.get_data(self.points_data_file_name)
        self.settings_data = self.get_data(self.settings_data_file_name)
        self.rotary_tables_data = self.get_data(self.rotary_tables_file_name)

        print("MM3: ", len(self.points_data))
        self.group_up_the_points()
        self.arrange_rotary_tables()

        print("MM4: ", len(self.points_data))
        self.overwrite_groups_data(self.groups)
        # self.save_data(self.points_data_file_name, self.points_data, with_over_writing=False)
        self.save_data(self.rotary_tables_file_name, self.rotary_tables_data, with_over_writing=False)

        print("MM5: ", len(self.points_data))
        """

    def write_SD(self):
        bash_commands = ["sudo rm ~/../../boot/data/settings.txt",
                         "sudo rm ~/../../boot/data/point_groups.txt",
                         "sudo rm ~/../../boot/data/rotary_tables.txt",
                         "sudo cp -r ~/CoordinateTable/settings.txt ~/../../boot/CoordinateTableData/out/settings.txt",
                         "sudo cp -r ~/CoordinateTable/point_groups.txt ~/../../boot/CoordinateTableData/out/point_groups.txt",
                         "sudo cp -r ~/CoordinateTable/rotary_tables.txt ~/../../boot/CoordinateTableData/out/rotary_tables.txt", ]

        # bashCommand1 = "sudo cp -r ~/CoordinateTable/settings.txt ~/../../boot/data/settings.txt"
        # bashCommand2 = "sudo cp -r ~/CoordinateTable/point_groups.txt ~/../../boot/data/point_groups.txt"
        # bashCommand3 = "sudo cp -r ~/CoordinateTable/rotary_tables.txt ~/../../boot/data/rotary_tables.txt"
        # os.system(bashCommand1)
        # os.system(bashCommand2)
        # os.system(bashCommand3)

        for b_c in bash_commands:
            os.system(b_c)
