from Memory.Point import Point


class PointGroup:
    def __init__(self, name):
        self.name = name
        self.points = []

    def add_point(self, name, x, y):
        self.points.append(Point(self.name, name, x, y))

    def remove_point(self, name):
        points = self.points.copy()
        self.points.clear()
        for p in points:
            if not p.name == name:
                self.points.append(p)

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name
        for p in self.points:
            p.set_group(self.name)

    def get_points(self):
        return self.points.copy()

    def get_point_array(self):
        return self.points