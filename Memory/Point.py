class Point:
    def __init__(self, group, name, x, y):
        self.group = group
        self.name = name
        self.x = x
        self.y = y

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def set_name(self, name):
        self.name = name

    def set_group(self, group):
        self.group = group

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_name(self):
        return self.name

    def get_group(self):
        return self.group
