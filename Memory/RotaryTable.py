class RotaryTable:
    def __init__(self, name, radius, partition, skip):
        self.name = name
        self.radius = radius
        self.partition = partition
        self.skip = skip

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_radius(self):
        return self.radius

    def set_radius(self, radius):
        self.radius = radius

    def get_partition(self):
        return self.partition

    def set_partition(self, partition):
        self.partition = partition

    def get_skip(self):
        return self.skip

    def set_skip(self, skip):
        self.skip = skip
