#!/bin/bash

cd ~/CoordinateTable/

if ! ps ax | grep -q "[A]pplication.py"; then
  XAUTHORITY=/home/pi/.Xauthority DISPLAY=:0 python3 Application.py
fi

