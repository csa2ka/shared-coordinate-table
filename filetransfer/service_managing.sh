#!/bin/bash

declare -a arr=(
"systemd-timesyncd.service"
"wpa_supplicant.service"
#"udisks2.service"
"hcpcd.service"
#"systemd-user-sessions.service"
"networking.service"
"hciuart.service"

)

for i in "${arr[@]}"
do 
 sudo systemctl disable "$i"
done