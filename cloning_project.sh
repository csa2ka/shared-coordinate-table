#!/bin/bash

#--copy files from somewhere, github maybe?
sudo rm -r ~/CoordinateTable
git clone https://csa2ka:coordinate@bitbucket.org/csa2ka/shared-coordinate-table.git ~/CoordinateTable


#-- setting up Application for unix exec

chmod 777 ~/CoordinateTable/Application.py
dos2unix ~/CoordinateTable/Application.py

chmod 777 /home/pi/CoordinateTable/filetransfer/application_start.sh
# chmod 777 /home/pi/CoordinateTable/setup.sh
chmod 777 /home/pi/CoordinateTable/cloning_project.sh

#XAUTHORITY=/home/pi/.Xauthority DISPLAY=:0 python3 Application.py