import tkinter as tk
import tkinter.ttk as ttk
import GUI.icons as icons
from Memory.MemoryManager import MemoryManager as mm
from Communicator import Communicator
from functools import partial
# from tkinter.messagebox import *

from utils.domi_funcs import popup_message


class OptionSetter:
    """
    Egyedi widget, több guizero widget-ből.
    Létrehoz egy ttk.Frame-ot, benne egy ttk.Label-et és gomblista alapján az opciókat is, amiket, ha
    a gombok aktiváló függvényében megmondjuk neki, megfelelően is tud kezelni.
    ___________________________________
    |TEXT                       OPCIÓK|
    ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘ˇˇˇˇˇˇˇˇˇ
    """
    BTN_WIDTH = 90

    def __init__(
            self, gui_parent, screen_parent, command, button_texts, text="Lehetőségek:", explain_text_text = " Lehetőségek",
            width=None, height=None, align="top", border=False, expand=0, fill=None, side=tk.TOP,
            pictures=False, error_message="Ezt nem engedem!"
    ):
        self.ACTIVE_COLOR = '#00E055'
        self.INACTIVE_COLOR = '#77E055'
        self.command = command
        self.button_texts = button_texts
        self.screen_parent = screen_parent
        self.gui_parent = gui_parent
        self.text = text
        self.explain_text_text = explain_text_text
        self.width = width
        self.height = height
        self.align = align
        self.border = border
        self.expand = expand
        self.fill = fill
        self.side = side
        self.option_buttons = []
        self.error_message = error_message

        self.frame = ttk.Frame(gui_parent, width=self.width, height=self.height)
        self.frame.pack_propagate(0)
        self.frame.pack(expand=1, fill=tk.BOTH, side=tk.TOP)

        self.explain_text = ttk.Label(self.frame, text=self.explain_text_text)
        self.explain_text.pack(side=tk.LEFT)

        for btn_txt in button_texts:
            mini_frame = ttk.Frame(self.frame, height=self.height, width=self.BTN_WIDTH)
            mini_frame.pack_propagate(0)
            mini_frame.pack(side=tk.RIGHT, fill=tk.Y)
            btn = ttk.Button(mini_frame, command=partial(self.switch_to, btn_txt, False), style='OptionSetter.TButton',
                             text=btn_txt)
            if pictures:
                if btn_txt == mm.DEG45:
                    #print("OPTIONSETTER mm.DEG45 iconberakás")
                    btn.config(image=icons.DEG45)
                elif btn_txt == mm.LINEAR:
                    btn.config(image=icons.LINEAR)
                elif btn_txt == mm.AXIAL:
                    btn.config(image=icons.AXIAL)
                elif btn_txt == mm.Y_UP:
                    btn.config(image=icons.UP_MOTOR_DIR)
                elif btn_txt == mm.Y_DOWN:
                    btn.config(image=icons.DOWN_MOTOR_DIR)
                elif btn_txt == mm.X_RIGHT:
                    btn.config(image=icons.RIGHT_MOTOR_DIR)
                elif btn_txt == mm.X_LEFT:
                    btn.config(image=icons.LEFT_MOTOR_DIR)

            btn.pack(expand=1, fill=tk.BOTH)
            self.option_buttons.append(btn)

    def switch_to(self, t, is_init=True):
        t = str(t)
        if self.command(t, is_init) or is_init:
            self.pressed_one(t)
        else:
            Communicator.popup("Az így nem beállítható!")
            # showerror("Atya ég!", self.error_message)

    def pressed_one(self, t):
        for other_btn in self.option_buttons:
            if other_btn["text"] == t:
                other_btn.state(["disabled"])
            else:
                other_btn.state(["!disabled"])  #self.INACTIVE_COLOR
