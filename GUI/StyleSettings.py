import tkinter.ttk as ttk
import tkinter as tk
import tkinter.font as font
from os import path

import GUI.icons as icons


def style_init():
    font_family = 'Arial'
    font.nametofont('TkDefaultFont').configure(family=font_family, size=20)
    ttk.Style().theme_use("clam")

    t_btn_bg_color = '#F0ECEB'
    ttk.Style().configure('TButton', focuscolor=t_btn_bg_color, background=t_btn_bg_color)  # "disable" focus rectangle

    ttk.Style().configure('KeyBoard.TButton', font=(font_family, 30))
    ttk.Style().configure('KeyBoard.TLabel', font=(font_family, 40))

    ttk.Style().configure('RPIC.TButton', background='#EE3233')
    ttk.Style().configure('NumberSetter.TButton', background='#A3D6F5', font=(font_family, 25))
    ttk.Style().configure('TextSetter.TButton', background='#CEEBFB')
    ttk.Style().configure('ScreenControl.TButton', background='#66A7C5')
    # ttk.Style().configure('Back.ScreenControl.TButton', background='#11A7C5')
    ttk.Style().configure('MainMenu.TButton', font=(font_family, 20))

    ttk.Style().configure('NumberUpdater.TLabel', font=(font_family, 25))

    ttk.Style().map(
        "OptionSetter.TButton",
        foreground=[("disabled", "#000000"), ("!disabled", "#888888")],
        background=[("disabled", "#FFFFFF"), ("!disabled", "#CEEBFB")]
    )

    icons_path = path.join('.', 'img', 'Icons')

    icons.TRASH = tk.PhotoImage(file=path.join(icons_path, 'thrash2.png'))
    icons.UP = tk.PhotoImage(file=path.join(icons_path, 'up.png'))
    icons.DOWN = tk.PhotoImage(file=path.join(icons_path, 'down.png'))
    icons.ADD = tk.PhotoImage(file=path.join(icons_path, 'add.png'))
    icons.DEG45 = tk.PhotoImage(file=path.join(icons_path, '45deg.png'))
    icons.LINEAR = tk.PhotoImage(file=path.join(icons_path, 'linear.png'))
    icons.AXIAL = tk.PhotoImage(file=path.join(icons_path, 'axial.png'))
    icons.UP_MOTOR_DIR = tk.PhotoImage(file=path.join(icons_path, 'up_motor.png'))
    icons.DOWN_MOTOR_DIR = tk.PhotoImage(file=path.join(icons_path, 'down_motor.png'))
    icons.RIGHT_MOTOR_DIR = tk.PhotoImage(file=path.join(icons_path, 'right_motor.png'))
    icons.LEFT_MOTOR_DIR = tk.PhotoImage(file=path.join(icons_path, 'left_motor.png'))
    # icons.QQQQQQQQQQQQQQ = tk.PhotoImage(file=path.join(icons_path, 'csillarmika.png'))


# ARCHIVE
# print(ttk.Style().theme_names())
