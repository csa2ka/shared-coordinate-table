from functools import partial


class Repeat:
    funcs_to_call = []

    @classmethod
    def on_tick(cls, root):
        for f in cls.funcs_to_call:
            f()
        root.after(100, cls.on_tick, root)

    @classmethod
    def register_func(cls, func):
        cls.funcs_to_call.append(func)
