from tkinter import Frame, Label, Button, Tk, BOTH
import tkinter.ttk as ttk
from functools import partial
# App, Box, Text, PushButton


class Visibilitable:
    def show(self):
        self.lift()  # may use: self.pack_forget, vagy grid_forget

    def hide(self):
        self.lower()  # may use: self.pack, vagy grid - ehhez el kéne mentenie gondolom a koordinátáit


# app = App(title="Koordináta asztal", width=Screen.WIDTH, height=Screen.HEIGHT, bg=(254, 209, 93))
# screenName=None, baseName=None, className='Tk', useTk=1
class App(Tk):
    def __init__(self, title=None, baseName=None, className='Tk', useTk=1):
        super(App, self).__init__(title, baseName, className, useTk=1)

        self.configure(background='orange')

        # Full size
        w, h = self.winfo_screenwidth(), self.winfo_screenheight()
        # use the next line if you also want to get rid of the titlebar
        self.overrideredirect(1)
        self.geometry("%dx%d+0+0" % (w, h))

    def display(self):
        self.mainloop()


# btn = Button(window, text="Click Me", command=clicked)
# S2_go_right_btn = PushButton(S2_right_box, command=switch_screen, args=[SETTINGS1], text="=>", align="left",
#                              width="fill", height="fill")
class PushButton(Button, Visibilitable):
    def __init__(self, gui_parent, command, text, grid=None, align=None, width=100, height=30, args=None):
        command_with_args = partial(command, args)
        super(PushButton, self).__init__(gui_parent, text=text, command=command_with_args)
        # TODO much...
        if grid is not None:
            self.grid(row=grid[1], column=grid[0])  # TODO may have to swap
        else:
            self.pack()  # TODO may have to swap

# PushButton(test_screen.box, command=go100150, text="X:", grid=[2, 2], align="left")


# Text_x = Text(test_screen.box, "0", grid=[3, 2], align="left")
class Text(Label, Visibilitable):
    def __init__(self, box, text, width=None, height=None, grid=None, align=None):
        super(Text, self).__init__(box, text=text)
        if grid is not None:
            self.grid(row=grid[1], column=grid[0])  # TODO may have to swap
        else:
            self.pack()
        # TODO align


# self.box = Box(gui_parent, layout=self.layout, height="fill", width="fill")
# S1_bottom_box = Box(settings1_screen.box, align="bottom", width="fill", height=int(Screen.HEIGHT / k), border=True)
class Box(Frame, Visibilitable):
    def __init__(self, gui_parent, grid=None, height=None, width=None, layout=None, align=None, border=None):
        super(Box, self).__init__(gui_parent)
        if grid is not None:
            self.grid(row=grid[1], column=grid[0])  # TODO may have to swap
        else:
            self.pack()
        # TODO use other parameters
        # self.pack(fill=BOTH)


#
# class App(Frame):
#     def __init__(self, master=None, title="guizero", width=500, height=500, layout="auto", bgcolor=None, bg=None,
#                  visible=True):
#         Frame.__init__(self, master, bg=bg, height=height, width=width)
#         self.pack()
#
#         self.title = title
#         self.width = width
#         self.height = height
#         self.layout = layout
#         self.bgcolor = bgcolor
#         self.bg = bg
#         self.visible = visible
