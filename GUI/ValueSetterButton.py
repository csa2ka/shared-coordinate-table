#from guizero import PushButton
import tkinter as tk
import tkinter.ttk as ttk
from GUI.Screens.Screen import Screen


class ValueSetterButton:
    """
    Egyedi widget, több guizero widget-ből.
    Létrehoz egy Box-ot, benne egy cím Text-et és egy Számkiíró PushButtont, amire kattintva előjön egy Keyboard.
    ___________________________________
    |TEXT                  NUMBERnGOMB|
    ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘ˇˇˇˇˇˇˇˇˇ
    """

    def __init__(self, gui_parent, set_value_function, num_or_text="num", self_side=tk.TOP, text="Szöveg", grid=None,
                 additional_information=None, width=None, height=None, floating_digits=3):
        self.gui_parent = gui_parent
        self.num_or_text = num_or_text
        self.set_value_function = set_value_function
        self.text = text
        self.side = self_side
        self.width = width
        self.height = height
        # self.align = align
        # self.grid = grid
        self.additional_information = additional_information
        self.blacklist = []
        self.floating_digits = floating_digits
        self.in_value_setting = True

        # self.btn = PushButton(gui_parent, command=self.send_to_command, width=self.width, height=self.height,
        #                      align="right", text=str(self.text), grid=self.grid)
        if width is not None:
            self.frame = ttk.Frame(gui_parent, width=self.width, height=self.height)
            self.frame.pack_propagate(0)
            self.frame.pack(side=self.side)
        else:
            self.frame = ttk.Frame(gui_parent)
            self.frame.pack(side=self.side, expand=1, fill=tk.BOTH)

        btn_style = 'NumberSetter.TButton'
        if num_or_text == "text":
            btn_style = 'TextSetter.TButton'
        self.btn = ttk.Button(self.frame, style=btn_style, text=str(self.text), command=self.send_to_command)
        self.btn.pack(expand=1, fill=tk.BOTH)

    def set_blacklist(self, bl):
        self.blacklist = bl

    def hide(self):
        # self.btn.hide()
        self.btn.pack_forget()

    def show(self):
        self.btn.pack()

    def enable_value_setting(self):
        self.in_value_setting = True

    def disable_value_setting(self):
        self.in_value_setting = False

    def send_to_command(self):
        if self.in_value_setting:
            if self.num_or_text == "text":
                Screen.keyboard_command(self.btn, self.set_value_function, self.additional_information, self.blacklist)
            if self.num_or_text == "num":
                Screen.numpad_command(self.btn, self.set_value_function, self.additional_information, self.floating_digits)
        else:
            self.set_value_function(None)

    def trigger_set_value_function(self):
        self.set_value(self.btn["text"])

    def set_value(self, s):
        self.btn.config(text=s)
        if self.additional_information is None:
            self.set_value_function(s)
        else:
            self.set_value_function(s, self.additional_information)

    def set_initial_value(self, s):
        self.btn.config(text=s)
