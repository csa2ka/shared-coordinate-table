import tkinter as tk
import tkinter.ttk as ttk
from functools import partial

from GUI.ValueSetterButton import ValueSetterButton
from GUI.Screens.Screen import Screen
from Memory.MemoryManager import MemoryManager as mm
import GUI.icons as icons
from Communicator import Communicator as comm
from GUI.Screens.ScreenType import ScreenType
from utils.domi_funcs import *
from utils.balazs_funcs import spec_format

class PointsAndGroups:
    SLOT = 5
    HH = int(Screen.HEIGHT / (SLOT + 2))
    NAME_SETTER_WIDTH = 250
    X_SETTER_WIDTH = 150
    Y_SETTER_WIDTH = 150
    BOTTOM_BTN_WITH = 120
    GROUP_DEL_BTN_WIDTH = 110
    XY_LABEL_WID = 30
    GO_BTN_WIDTH = 120
    groups_data = []  # pointgroup

    first_group_to_show = 0
    first_point_to_show = 0
    in_this_group = None

    def __init__(self, gui_parent, go_function):
        self.gui_parent = gui_parent
        self.go_function = go_function

        self.bottom_frame = ttk.Frame(gui_parent, height=self.HH)
        self.bottom_frame.pack_propagate(0)
        self.bottom_frame.pack(side=tk.BOTTOM, expand=0, fill=tk.X)

        self.full_frame_hider = ttk.Frame(gui_parent)
        self.full_frame_hider.pack_propagate(0)
        self.full_frame_hider.pack(side=tk.TOP, expand=1, fill=tk.BOTH)
        self.inside_frame = ttk.Frame(gui_parent, height=self.HH)
        self.inside_frame.place(in_=self.full_frame_hider, relx=0.5, rely=0.5, anchor=tk.CENTER, relheight=1,
                                relwidth=1)
        self.inside_btn_hider = ttk.Frame(self.inside_frame)
        self.inside_btn_hider.pack_propagate(0)
        self.inside_btn_hider.pack(expand=1, fill=tk.BOTH)

        self.outside_frame = ttk.Frame(gui_parent, height=self.HH)
        self.outside_frame.place(in_=self.full_frame_hider, relx=0.5, rely=0.5, anchor=tk.CENTER, relheight=1,
                                 relwidth=1)
        self.outside_btn_hider = ttk.Frame(self.outside_frame)
        self.outside_btn_hider.pack_propagate(0)
        self.outside_btn_hider.pack(expand=1, fill=tk.BOTH)

        self.frames_outside = []
        for i in range(self.SLOT):
            frame = ttk.Frame(self.outside_frame, height=self.HH)
            frame.pack_propagate(0)
            frame.pack(in_=self.outside_btn_hider, side=tk.TOP, expand=1, fill=tk.BOTH)
            self.frames_outside.append(frame)

        self.outside_buttons = []
        for i in range(self.SLOT):
            button = ttk.Button(self.frames_outside[i], text="asd", command=partial(self.go_into_group, i))
            button.pack(expand=1, fill=tk.BOTH)
            self.outside_buttons.append(button)

        self.frames_inside = []
        for i in range(self.SLOT):
            frame = ttk.Frame(self.inside_frame, height=self.HH)
            frame.pack_propagate(0)
            frame.pack(in_=self.inside_btn_hider, side=tk.TOP, expand=1, fill=tk.BOTH)
            self.frames_inside.append(frame)

        coo_MainMenu_Frame = ttk.Frame(self.gui_parent, width=self.BOTTOM_BTN_WITH, height=self.HH)
        coo_MainMenu_Frame.pack_propagate(0)
        coo_MainMenu_Frame.pack(in_=self.bottom_frame, side=tk.LEFT, fill=tk.BOTH)
        coo_MainMenu_btn = ttk.Button(coo_MainMenu_Frame, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                      text="Főmenü", style='ScreenControl.TButton')
        coo_MainMenu_btn.pack(expand=1, fill=tk.BOTH)

        self.up_btn_wrapper = ttk.Frame(self.gui_parent, width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.up_btn_wrapper.pack_propagate(0)
        self.up_btn = ttk.Button(self.up_btn_wrapper, command=self.up, image=icons.DOWN, text="Le")
        self.up_btn.pack(expand=1, fill=tk.BOTH)
        self.up_btn_wrapper.pack(in_=self.bottom_frame, side=tk.RIGHT)

        self.down_btn_wrapper = ttk.Frame(self.gui_parent, width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.down_btn_wrapper.pack_propagate(0)
        self.down_btn = ttk.Button(self.down_btn_wrapper, command=self.down, image=icons.UP, text="Fel")
        self.down_btn.pack(expand=1, fill=tk.BOTH)
        self.down_btn_wrapper.pack(in_=self.bottom_frame, side=tk.RIGHT)

        self.add_btn_wrapper = ttk.Frame(self.gui_parent, width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.add_btn_wrapper.pack_propagate(0)
        self.add_btn = ttk.Button(self.add_btn_wrapper, command=self.add, image=icons.ADD, text="Hozzáad")
        self.add_btn.pack(expand=1, fill=tk.BOTH)
        self.add_btn_wrapper.pack(in_=self.bottom_frame, side=tk.LEFT)

        self.back_btn_wrapper = ttk.Frame(self.gui_parent, width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.back_btn_wrapper.pack_propagate(0)
        self.back_btn = ttk.Button(self.back_btn_wrapper, command=self.get_out_of_group, text="Vissza",
                                   style='Back.ScreenControl.TButton')
        self.back_btn.pack(expand=1, fill=tk.BOTH)
        self.back_btn_wrapper.pack(in_=self.bottom_frame, side=tk.RIGHT)

        self.delete_btn_array = []
        self.name_setter_array = []
        self.x_setter_array = []
        self.y_setter_array = []
        self.go_btn_array = []

        self.top_frame = ttk.Frame()
        self.group_name_setter = ValueSetterButton(self.frames_inside[0], self.set_group_name, num_or_text="text",
                                                   text="Név", self_side=tk.RIGHT)

        self.delete_btn_wrapper = ttk.Frame(self.frames_inside[0], width=self.GROUP_DEL_BTN_WIDTH, height=self.HH+2)
        self.delete_btn_wrapper.pack_propagate(0)
        self.delete_btn = ttk.Button(self.delete_btn_wrapper, command=self.delete_group, image=icons.TRASH,
                                     text="Csoport törlése")
        self.delete_btn.pack(side=tk.LEFT, expand=1, fill=tk.BOTH)
        self.delete_btn_wrapper.pack(side=tk.LEFT)  # in_=self.bottom_frame

        for i in range(1, self.SLOT):
            frame = self.frames_inside[i]
            size_wrapper = ttk.Frame(frame, width=self.GROUP_DEL_BTN_WIDTH, height=self.HH+2)
            size_wrapper.pack_propagate(0)
            size_wrapper.pack(side=tk.LEFT)
            delete_btn = ttk.Button(size_wrapper, command=partial(self.delete_point, i), image=icons.TRASH,
                                    text="Pont törlése")
            delete_btn.pack(expand=1, fill=tk.BOTH)
            self.delete_btn_array.append(delete_btn)

            size_wrapper = ttk.Frame(frame, width=self.GO_BTN_WIDTH, height=self.HH + 2)
            size_wrapper.pack_propagate(0)
            size_wrapper.pack(side=tk.RIGHT)
            go_btn = ttk.Button(size_wrapper, style='RPIC.TButton', command=partial(self.go, i), text="Rajta")
            go_btn.pack(expand=1, fill=tk.BOTH)
            self.go_btn_array.append(go_btn)

            y_setter = ValueSetterButton(frame, self.set_point_y, num_or_text="num", text="0", self_side=tk.RIGHT,
                                         height=self.HH+2,
                                         width=self.X_SETTER_WIDTH, additional_information=i)
            self.y_setter_array.append(y_setter)

            size_wrapper = ttk.Frame(frame, width=self.XY_LABEL_WID, height=self.HH + 2)
            size_wrapper.pack_propagate(0)
            size_wrapper.pack(side=tk.RIGHT)
            y_label = ttk.Label(size_wrapper, text="Y:")
            y_label.pack(expand=1, fill=tk.BOTH)

            x_setter = ValueSetterButton(frame, self.set_point_x, num_or_text="num", text="0", self_side=tk.RIGHT,
                                         height=self.HH+2,
                                         width=self.Y_SETTER_WIDTH, additional_information=i)
            self.x_setter_array.append(x_setter)

            size_wrapper = ttk.Frame(frame, width=self.XY_LABEL_WID, height=self.HH + 2)
            size_wrapper.pack_propagate(0)
            size_wrapper.pack(side=tk.RIGHT)
            x_label = ttk.Label(size_wrapper, text="X:")
            x_label.pack(expand=1, fill=tk.BOTH)

            name_setter = ValueSetterButton(frame, self.set_point_name, num_or_text="text", text="Név",
                                            self_side=tk.LEFT, height=self.HH+2, width=self.NAME_SETTER_WIDTH,
                                            additional_information=i)
            self.name_setter_array.append(name_setter)

        self.refresh_data()
        self.show_current_state()

    @staticmethod
    def frame_show(what, hiding_frame):
        what.lift(hiding_frame)

    @staticmethod
    def frame_hide(what, hiding_frame):
        what.lower(hiding_frame)

    def go(self, i):
        i = self.first_point_to_show + i - 1
        points = self.in_this_group.points
        self.go_function(float(points[i].get_x()), float(points[i].get_y()))

    def set_point_name(self, value, i):
        i = self.first_point_to_show + i - 1
        points = self.in_this_group.points
        points[i].set_name(value)
        self.save_data_change()

    def set_point_x(self, value, i):
        i = self.first_point_to_show + i - 1
        points = self.in_this_group.points
        points[i].set_x(value)
        self.save_data_change()

    def set_point_y(self, value, i):
        i = self.first_point_to_show + i - 1
        points = self.in_this_group.points
        points[i].set_y(value)
        self.save_data_change()

    def set_group_name(self, value):
        self.in_this_group.set_name(str(value))
        self.save_data_change()

    def add_group(self):
        x_coordinate = float(comm.rpi.map.get_x_mm())
        y_coordinate = float(comm.rpi.map.get_y_mm())
        group_name = new_unique_name(self.get_group_names(), "Csoport ")
        mm.get_instance().add_group(group_name, "Pont 1", spec_format(x_coordinate, 3), spec_format(y_coordinate, 3))
        self.refresh_data()
        self.increase_first_group_to_show()

    def delete_group(self):
        self.groups_data.remove(self.in_this_group)
        self.refresh_first_group_to_show()
        self.save_data_change()
        self.get_out_of_group()

    def add_point(self):
        x_coordinate = float(comm.rpi.map.get_x_mm())
        y_coordinate = float(comm.rpi.map.get_y_mm())
        point_name = new_unique_name(self.get_point_names(), "Pont ")
        self.in_this_group.add_point(point_name, spec_format(x_coordinate, 3), spec_format(y_coordinate, 3))
        self.increase_first_point_to_show()

    def delete_point(self, i):
        i = self.first_point_to_show + i - 1
        points = self.in_this_group.points
        self.in_this_group.remove_point(points[i].get_name())
        self.refresh_first_point_to_show()
        self.save_data_change()
        self.show_current_state()

    def refresh_data(self):
        # print("1: ", len(self.groups_data))
        mm.get_instance().refresh_all_data()
        # print("2: ", len(self.groups_data))
        self.groups_data.clear()
        # print("3: ", len(self.groups_data))
        self.groups_data = mm.get_instance().groups.copy()
        # print("4: ", len(self.groups_data))
        self.show_current_state()
        # print("5: ", len(self.groups_data))

    def get_group_names(self):
        return [group.name for group in self.groups_data]

    def get_point_names(self):
        return [p.name for p in self.in_this_group.points]

    def update_blacklists(self):
        for name_setter in self.name_setter_array:
            blacklist = []
            for point in self.in_this_group.get_points():
                if point.get_name() != name_setter.btn["text"]:
                    blacklist.append(point.get_name())
            name_setter.set_blacklist(blacklist)
        blacklist = []
        for group in self.groups_data:
            if group.get_name() != self.group_name_setter.btn["text"]:
                blacklist.append(group.get_name())
        self.group_name_setter.set_blacklist(blacklist)

    def save_data_change(self):
        mm.get_instance().overwrite_groups_data(self.groups_data.copy())
        if self.in_this_group is not None:  # because it would be done anyway in the other case
            self.update_blacklists()

    def show_this_group(self, i):
        self.outside_buttons[i]["text"] = self.groups_data[i + self.first_group_to_show].get_name()

    def show_this_point(self, i):
        points = self.in_this_group.get_points()
        index = self.first_point_to_show + i
        self.name_setter_array[i].set_initial_value(points[index].get_name())
        self.x_setter_array[i].set_initial_value(points[index].get_x())
        self.y_setter_array[i].set_initial_value(points[index].get_y())

    def show_current_state(self):
        if self.in_this_group is None:
            for i in range(self.SLOT):
                if len(self.groups_data) > i:
                    self.show_this_group(i)
                    self.frame_show(self.frames_outside[i], self.outside_btn_hider)
                else:
                    self.frame_hide(self.frames_outside[i], self.outside_btn_hider)
            self.frame_hide(self.inside_frame, self.full_frame_hider)
            self.frame_show(self.outside_frame, self.full_frame_hider)
            self.frame_hide(self.back_btn_wrapper, self.bottom_frame)
        else:
            self.group_name_setter.set_initial_value(self.in_this_group.get_name())
            self.frames_inside[0].pack()
            for i in range(self.SLOT - 1):
                if len(self.in_this_group.get_points()) > i:
                    self.show_this_point(i)
                    self.frame_show(self.frames_inside[i + 1], self.inside_btn_hider)
                else:
                    self.frame_hide(self.frames_inside[i + 1], self.inside_btn_hider)
            self.frame_show(self.inside_frame, self.full_frame_hider)
            self.frame_hide(self.outside_frame, self.full_frame_hider)
            self.frame_show(self.back_btn_wrapper, self.bottom_frame)

            self.update_blacklists()

    def refresh_first_group_to_show(self):
        """
            Checks if first_point_to_show isn't too big, or too small.
            len(self.in_this_group.get_points()) - (self.SLOT - 1) could be <0 so the order is important
        """
        if self.first_group_to_show >= len(self.groups_data) - self.SLOT + 1:
            self.first_group_to_show = len(self.groups_data) - self.SLOT
        if self.first_group_to_show <= -1:
            self.first_group_to_show = 0

    def increase_first_group_to_show(self):
        self.first_group_to_show += 1
        self.refresh_first_group_to_show()

    def decrease_first_group_to_show(self):
        self.first_group_to_show -= 1
        self.refresh_first_group_to_show()

    def refresh_first_point_to_show(self):
        """
        Checks if first_point_to_show isn't too big, or too small.
        len(self.in_this_group.get_points()) - (self.SLOT - 1) could be <0 so the order is important
        """
        if self.first_point_to_show >= len(self.in_this_group.get_points()) - (self.SLOT - 1) + 1:
            self.first_point_to_show = len(self.in_this_group.get_points()) - (self.SLOT - 1)
        if self.first_point_to_show <= -1:
            self.first_point_to_show = 0

    def increase_first_point_to_show(self):
        self.first_point_to_show += 1
        self.refresh_first_point_to_show()

    def decrease_first_point_to_show(self):
        self.first_point_to_show -= 1
        self.refresh_first_point_to_show()

    def up(self):
        if self.in_this_group is None:
            self.increase_first_group_to_show()
        else:
            self.increase_first_point_to_show()
        self.show_current_state()

    def down(self):
        if self.in_this_group is None:
            self.decrease_first_group_to_show()
        else:
            self.decrease_first_point_to_show()
        self.show_current_state()

    def add(self):
        if self.in_this_group is None:
            self.add_group()
            for i in range(len(self.groups_data)):
                self.increase_first_group_to_show()
        else:
            for i in range(len(self.in_this_group.get_points())):
                self.increase_first_point_to_show()
            self.add_point()
        self.save_data_change()
        self.show_current_state()

    def go_into_group(self, i):
        for g in self.groups_data:
            if g.get_name() == self.outside_buttons[i]["text"]:
                self.in_this_group = g
        self.first_point_to_show = 0
        self.show_current_state()

    def get_out_of_group(self):
        self.in_this_group = None
        self.show_current_state()
