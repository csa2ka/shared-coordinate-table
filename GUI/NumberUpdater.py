import tkinter as tk
import tkinter.ttk as ttk
from GUI.Screens.Screen import Screen
from GUI.Repeat import Repeat


class NumberUpdater:
    """
    Egyedi widget, több tkinter widget-ből.
    Létrehoz egy Frame-ot, benne egy cím Label-et és két számkiíró Label-et, amik egy-egy funkciót meghívva várnak egy
    értéket, amit kiírnak majd.
    ___________________________________
    |              TEXT               |
    |      Number         Number      |
    ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘ˇˇˇˇˇˇˇˇˇ
    """
    update_time = Screen.UPDATE_TIME

    def __init__(self, gui_parent, command, command1=None, command2=None, text="Szám:", border=False, myside=tk.LEFT,
                 is_horizontal=True, is_int=False):
        self.gui_parent = gui_parent
        self.command = command
        self.command1 = command1
        self.command2 = command2
        self.text = text
        self.border = border
        self.is_int = is_int

        self.frame = ttk.Frame(gui_parent, border=self.border)    # grid
        self.frame.pack(expand=1, fill=tk.BOTH, side=myside)

        if is_horizontal:
            self.explain_text = ttk.Label(self.frame, text=self.text, style='NumberUpdater.TLabel')
            self.explain_text.pack(side=tk.LEFT)

            self.text1 = ttk.Label(self.frame, text=self.text, style='NumberUpdater.TLabel')
            self.text1.pack(side=tk.LEFT)

            self.text2 = ttk.Label(self.frame, text=self.text, style='NumberUpdater.TLabel')
            self.text2.pack(side=tk.LEFT)
        else:
            self.explain_text = ttk.Label(self.frame, text=self.text, style='NumberUpdater.TLabel')
            self.explain_text.grid(column=0, row=0, columnspan=2, rowspan=1)

            self.text1 = ttk.Label(self.frame, text=self.text, style='NumberUpdater.TLabel')
            self.text1.grid(column=0, row=1)

            self.text2 = ttk.Label(self.frame, text=self.text, style='NumberUpdater.TLabel')
            self.text2.grid(column=1, row=1)

        Repeat.register_func(self.update_values)

    def update_values(self):
        format = "%.3f"
        if self.is_int:
            format="%d"
        if self.command1 is not None:
            self.text1["text"] = "X: " + format % self.command(self.command1)
            self.text2["text"] = "Y: " + format % self.command(self.command2)
        else:
            self.text1["text"] = format % self.command()
            self.text2["text"] = ""
