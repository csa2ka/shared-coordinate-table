import tkinter as tk
import tkinter.ttk as ttk
from GUI.Screens.Screen import Screen


class CamAndDrillPanel:
    SLOT = 6
    HH = int(Screen.HEIGHT / (SLOT + 2))

    camera_pos_x = 0
    camera_pos_y = 0
    drill_pos_x = 0
    drill_pos_y = 0

    delta_x = 0
    delta_y = 0

    def __init__(
            self, gui_parent, mm, th_command, ask_for_pos_x_command, ask_for_pos_y_command,
            go_to_command, border=False
    ):
        # print("DELTA init")
        self.mm = mm
        self.th_command = th_command
        self.gui_parent = gui_parent
        self.ask_for_pos_x_command = ask_for_pos_x_command
        self.ask_for_pos_y_command = ask_for_pos_y_command
        self.go_to_command = go_to_command
        self.border = border

        self.current_position = 0

        self.frame_inside = ttk.Frame(gui_parent, border=self.border)
        self.frame_inside.pack(expand=1, fill=tk.BOTH)

        self.frames = []

        for i in range(self.SLOT + 1):
            frame = ttk.Frame(self.frame_inside, border=self.border)
            frame.pack(expand=1, fill=tk.BOTH)
            self.frames.append(frame)

        self.drill_pos_text = ttk.Label(self.frames[1], text="0000.00")
        self.drill_pos_text.pack(side=tk.LEFT)

        self.camera_pos_text = ttk.Label(self.frames[2], text="0000.00")
        self.camera_pos_text.pack(side=tk.LEFT)

        self.delta_xy_text = ttk.Label(self.frames[3], text="000000000.00")
        self.delta_xy_text.pack(side=tk.TOP)

        self.set_drill_pos_btn = ttk.Button(self.frames[1], command=self.set_drill_pos, text="Fúró pozíció")
        self.set_drill_pos_btn.pack(side=tk.RIGHT)

        self.set_camera_pos_btn = ttk.Button(self.frames[2], command=self.set_camera_pos, text="Kamera pozíció")
        self.set_camera_pos_btn.pack(side=tk.RIGHT)

        self.go_to_drill_btn = ttk.Button(self.frames[self.SLOT - 1], command=self.go_to_drill, text="Fúróhozz!",
                                          style='RPIC.TButton')
        self.go_to_drill_btn.pack(side=tk.LEFT)

        self.go_to_camera_btn = ttk.Button(self.frames[self.SLOT - 1], command=self.go_to_camera, text="Kamerához!",
                                           style='RPIC.TButton')
        self.go_to_camera_btn.pack(side=tk.RIGHT)
        self.setup_delta_xy()

    def setup_delta_xy(self):
        # print("DELTA SETUP")
        for l in self.mm.settings_data:
            if l[0] == "kamera furo x":
                self.delta_x = float(l[1])
            if l[0] == "kamera furo y":
                self.delta_y = float(l[1])
        self.delta_xy_text.value = str(self.delta_x) + "; " + str(self.delta_y)
        self.camera_pos_x = self.delta_x
        self.camera_pos_y = self.delta_y
        self.drill_pos_text.value = str(0) + ";" + str(0)
        self.camera_pos_text.value = str(self.drill_pos_x) + "; " + str(self.drill_pos_y)

    def set_delta_xy(self):
        self.delta_x = self.drill_pos_x - self.camera_pos_x
        self.delta_y = self.drill_pos_y - self.camera_pos_y
        self.delta_xy_text.value = str(self.delta_x) + "; " + str(self.delta_y)
        self.mm.change("kamera furo x", str(self.delta_x))
        self.mm.change("kamera furo y", str(self.delta_y))

    def set_drill_pos(self):
        self.drill_pos_x = self.th_command(self.ask_for_pos_x_command)
        self.drill_pos_y = self.th_command(self.ask_for_pos_y_command)
        self.drill_pos_text.value = str(self.drill_pos_x) + "; " + str(self.drill_pos_y)
        self.set_delta_xy()

    def set_camera_pos(self):
        self.camera_pos_x = self.th_command(self.ask_for_pos_x_command)
        self.camera_pos_y = self.th_command(self.ask_for_pos_y_command)
        self.camera_pos_text.value = str(self.camera_pos_x) + ";" + str(self.camera_pos_y)

    def go_to_drill(self):
        self.go_to_command(self.drill_pos_x - self.camera_pos_x, self.drill_pos_y - self.camera_pos_y)

    def go_to_camera(self):
        self.go_to_command(self.camera_pos_x - self.drill_pos_x, self.camera_pos_y - self.drill_pos_y)
