from tkinter import Frame, Label
import tkinter as tk
import tkinter.ttk as ttk
from GUI.ValueSetterButton import ValueSetterButton


class NumberSetter:
    """
    Egyedi widget, több guizero widget-ből.
    Létrehoz egy Frame-ot, benne egy cím Label-et és egy Számkiíró PushButtont, amire kattintva előjön egy Numpad.
    -----------------------------------
    |TEXT                  NUMBERnGOMB|
    -----------------------------------
    """

    BTN_WIDTH = 200

    def __init__(
            self, gui_parent, command, text="Szám:", explain_text_text="Magyarázó szöveg", setter=True,
            width=None, height=None, border=False
    ):
        self.setter=setter
        self.gui_parent = gui_parent
        self.command = command
        self.text = text
        self.explain_text_text = explain_text_text
        self.width = width
        self.height = height
        self.border = border

        self.frame = ttk.Frame(gui_parent, height=self.height, width=self.width)
        self.frame.pack_propagate(0)
        self.frame.pack(expand=1, fill=tk.BOTH, side=tk.TOP)

        self.explain_text = ttk.Label(self.frame, text=self.explain_text_text)
        self.explain_text.pack(side=tk.LEFT)
        if self.setter:
            self.value_btn = ValueSetterButton(self.frame, self.command, self_side=tk.RIGHT, num_or_text="num",
                                           width=self.BTN_WIDTH, height=self.height, text=self.text)
        else:
            self.value_btn = ValueSetterButton(self.frame, self.command, self_side=tk.RIGHT, num_or_text="not_setter",
                                               width=self.BTN_WIDTH, height=self.height, text=self.text)
