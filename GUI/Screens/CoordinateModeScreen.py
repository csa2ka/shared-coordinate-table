from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from GUI.NumberUpdater import NumberUpdater
from Communicator import Communicator as comm
from GUI.PointsAndGroups import PointsAndGroups

import tkinter as tk
import tkinter.ttk as ttk
from functools import partial


class CoordinateModeScreen(Screen):

    def __init__(self, gui_parent, rpi, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.COORDINATE_MODE
        self.__rpi = rpi
        self.pointgrouppanel = None

    def _build(self):
        coo_title_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8), border=True)
        coo_title_frame.pack_propagate(0)
        coo_title_frame.pack(side=tk.TOP, expand=0, fill=tk.X)

        # coo_left_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 4), border=True)
        # coo_left_frame.pack_propagate(0)
        # coo_left_frame.pack(side=tk.LEFT, expand=0, fill=tk.Y)

        coo_right_frame = ttk.Frame(self.frame, border=True)
        coo_right_frame.pack_propagate(0)
        coo_right_frame.pack(expand=1, fill=tk.BOTH)  # side=tk.RIGHT,

        Coo_XY_display = NumberUpdater(coo_title_frame, comm.updating_text_value,
                                       self.__rpi.map.get_x_mm,
                                       self.__rpi.map.get_y_mm,
                                       text="Abs:", border=True)
        Coo_XY_display_step = NumberUpdater(coo_title_frame, comm.updating_text_value,
                                            self.__rpi.map.get_x_mm_relative,
                                            self.__rpi.map.get_y_mm_relative, text="Rel:", border=True,
                                            myside=tk.RIGHT)

        self.pointgrouppanel = PointsAndGroups(coo_right_frame, comm.go_to)

    def _refresh_data(self):
        pass
        # print("refreshing!!")
        # self.pointgrouppanel.refresh_data()

