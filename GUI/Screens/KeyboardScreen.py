from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm
from GUI.Keyboard import Keyboard


class KeyboardScreen(Screen):

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.KEYBOARD

    def _build(self):
        keyboard = Keyboard(self.frame, comm.switch_screen)
        self.keyboard = keyboard

    def _refresh_data(self):
        pass

    def start_keyboard(self, target, initial_text, prev_screen, command, arg=None, blacklist=None):
        self.keyboard.start_keyboard(target, initial_text, prev_screen, command, arg, blacklist)
