from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm

import tkinter as tk
import tkinter.ttk as ttk
from functools import partial


class ChooseSetupScreen(Screen):
    BUTTON_WIDTH = 300
    BUTTON_HEIGHT = 98

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.CHOOSE_SETUP

    def _build(self):
        size_wrapper = ttk.Frame(self.frame)
        size_wrapper.place(relx=0.5, rely=0, anchor=tk.CENTER, relheight=0.2, relwidth=1)
        question = ttk.Label(size_wrapper, text="Milyen módon szeretné beállítni a közepet?")
        question.pack(side=tk.BOTTOM)

        answer_sheet_frame_1 = ttk.Frame(self.frame, height=Screen.HEIGHT / 4, width=Screen.WIDTH)
        answer_sheet_frame_1.place(relx=0.5, rely=0.25, anchor=tk.CENTER, relheight=0.2, relwidth=1)

        size_wrapper = ttk.Frame(answer_sheet_frame_1, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        corner_btn = ttk.Button(size_wrapper, command=comm.corner_setup, text="Sarokba",
                                style='RPIC.TButton')
        corner_btn.pack(fill=tk.BOTH, expand=True)

        answer_sheet_frame_2 = ttk.Frame(self.frame, height=Screen.HEIGHT / 4, width=Screen.WIDTH)
        answer_sheet_frame_2.place(relx=0.5, rely=0.45, anchor=tk.CENTER, relheight=0.2, relwidth=1)

        size_wrapper = ttk.Frame(answer_sheet_frame_2, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        mid_btn = ttk.Button(size_wrapper, command=comm.mid_setup, text="Középre",
                             style='RPIC.TButton')
        mid_btn.pack(fill=tk.BOTH, expand=True)

        answer_sheet_frame_3 = ttk.Frame(self.frame, height=Screen.HEIGHT / 4, width=Screen.WIDTH)
        answer_sheet_frame_3.place(relx=0.5, rely=0.65, anchor=tk.CENTER, relheight=0.2, relwidth=1)

        size_wrapper = ttk.Frame(answer_sheet_frame_3, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        manual_btn = ttk.Button(size_wrapper, command=partial(comm.switch_screen, ScreenType.MANUAL_SETUP),
                                text="Manuálisan középre",
                                style='RPIC.TButton')
        manual_btn.pack(fill=tk.BOTH, expand=True)

        main_menu_btn_frame = ttk.Frame(self.frame, height=Screen.HEIGHT / 4, width=Screen.WIDTH)
        main_menu_btn_frame.place(relx=0.5, rely=0.9, anchor=tk.CENTER, relheight=0.2, relwidth=1)

        size_wrapper = ttk.Frame(main_menu_btn_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        mid_btn = ttk.Button(size_wrapper, command=partial(comm.switch_screen, ScreenType.MAIN_MENU), text="Főmenü",
                             style='ScreenControl.TButton')
        mid_btn.pack(fill=tk.BOTH, expand=True)

    def _refresh_data(self):
        pass
