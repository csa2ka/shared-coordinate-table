from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm
from utils.domi_funcs import popup_message

import tkinter as tk
import tkinter.ttk as ttk
import ctypes  # An included library with Python install.



class ManualSetup(Screen):
    BUTTON_WIDTH = 200
    BUTTON_HEIGHT = 100

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.MANUAL_SETUP
        self.setup_level = 1

    def _build(self):
        self.description = ttk.Label(self.frame, style='KeyBoard.TLabel')
        self.description.pack()
        self.description["text"] = "Menj az egyik horizontális szélhez!"
        comm.joymode_on()
        comm.manual_setup("reset")
        size_wrapper = ttk.Frame(self.frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.place(relx=0.5, rely=0.8, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        OK_button = ttk.Button(size_wrapper, style='ScreenControl.TButton',
                               command=self.proceed, text="OK")\
            .pack(side=tk.TOP, expand=True, fill=tk.BOTH)

    def proceed(self):
        self.setup_level += 1
        if self.setup_level == 2:
            # ctypes.windll.user32.MessageBoxW(0, "Your text", "Your title", 0)
            comm.manual_setup("x_min")
            self.description["text"] = "Menj a másik horizontális szélhez!"
            #popup_message("Test")

        elif self.setup_level == 3:
            comm.manual_setup("x_plus")
            self.description["text"] = "Menj az egyik vertikális szélhez!"
        elif self.setup_level == 4:
            comm.manual_setup("y_min")
            self.description["text"] = "Menj a másik vertikális szélhez!"
        else:
            comm.manual_setup("y_plus")
            comm.manual_setup("to_mid")
            self.description["text"] = "Menj az egyik horizontális szélhez!"
            self.setup_level = 1
            comm.switch_screen(ScreenType.MAIN_MENU)

    def _refresh_data(self):
        pass
