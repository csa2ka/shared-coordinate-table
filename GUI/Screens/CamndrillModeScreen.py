from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from GUI.NumberUpdater import NumberUpdater
from Communicator import Communicator as comm
from Memory.MemoryManager import MemoryManager as mmm
from GUI.CamAndDrillPanel import CamAndDrillPanel

import tkinter as tk
import tkinter.ttk as ttk
from functools import partial
from utils.balazs_funcs import spec_format


class CamndrillModeScreen(Screen):

    BUTTON_HEIGHT = 80
    BUTTON_WIDTH = 200
    BOTTOM_FRAME_HEIGHT = BUTTON_HEIGHT
    RIGHT_FRAME_WIDTH = 500
    LEFT_FRAME_WIDTH = Screen.WIDTH-RIGHT_FRAME_WIDTH

    camera_pos_x = 0
    camera_pos_y = 0
    drill_pos_x = 0
    drill_pos_y = 0

    delta_x = 0
    delta_y = 0
    FORMAT = 3

    def __init__(self, gui_parent, rpi, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.CAM_N_DRILL_MODE
        self.__rpi = rpi
        self.mm = mmm.get_instance()

    def _build(self):
        bottom_frame = ttk.Frame(self.frame, height=self.BOTTOM_FRAME_HEIGHT, width=Screen.WIDTH)
        bottom_frame.pack_propagate(0)
        bottom_frame.pack(side=tk.BOTTOM)

        left_frame = ttk.Frame(self.frame, width=self.LEFT_FRAME_WIDTH, height=Screen.HEIGHT-self.BOTTOM_FRAME_HEIGHT)
        left_frame.pack_propagate(0)
        left_frame.pack(side=tk.LEFT)

        right_frame = ttk.Frame(self.frame, width=self.RIGHT_FRAME_WIDTH,
                                height=Screen.HEIGHT - self.BOTTOM_FRAME_HEIGHT)
        right_frame.pack_propagate(0)
        right_frame.pack(side=tk.LEFT)

        Cnd_XY_display = NumberUpdater(left_frame, comm.updating_text_value, self.__rpi.map.get_x_mm,
                                       self.__rpi.map.get_y_mm, text="Abs:", border=True, myside=tk.TOP,
                                       is_horizontal=False)

        Cnd_XY_display = NumberUpdater(left_frame, comm.updating_text_value, self.__rpi.map.get_x_mm_relative,
                                       self.__rpi.map.get_y_mm_relative, text="Rel:", border=True, myside=tk.TOP,
                                       is_horizontal=False)

        # camndill_panel = CamAndDrillPanel(cnd_right_frame, mm, comm.updating_text_value,
        #                                   self.__rpi.map.get_x_mm, self.__rpi.map.get_y_mm, comm.go_this_much,
        #                                   border=False)

        size_wrapper = ttk.Frame(bottom_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.pack_propagate(0)
        size_wrapper.pack(side=tk.LEFT)
        cnd_MainMenu_btn = ttk.Button(size_wrapper, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                      text="Főmenü", style='ScreenControl.TButton')
        cnd_MainMenu_btn.pack(fill=tk.BOTH, expand=1)

        size_wrapper = ttk.Frame(bottom_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.pack_propagate(0)
        size_wrapper.pack(side=tk.RIGHT)
        go_to_drill_btn = ttk.Button(size_wrapper, command=self.go_to_drill, text="Fúróhoz!",
                                          style='RPIC.TButton')
        go_to_drill_btn.pack(fill=tk.BOTH, expand=1)

        size_wrapper = ttk.Frame(bottom_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.pack_propagate(0)
        size_wrapper.pack(side=tk.BOTTOM)
        go_to_camera_btn = ttk.Button(size_wrapper, command=self.go_to_camera, text="Kamerához!",
                                           style='RPIC.TButton')
        go_to_camera_btn.pack(fill=tk.BOTH, expand=1)

        self.drill_pos_text = ttk.Label(right_frame, text="0000.00")
        self.drill_pos_text.grid(column=2, row=1, sticky=tk.W)

        self.camera_pos_text = ttk.Label(right_frame, text="0000.00")
        self.camera_pos_text.grid(column=2, row=2, sticky=tk.W)

        self.delta_xy_text = ttk.Label(right_frame, text="0000.00")
        self.delta_xy_text.grid(column=2, row=3, sticky=tk.W)

        size_wrapper = ttk.Frame(right_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.pack_propagate(0)
        size_wrapper.grid(column=1, row=1, sticky=tk.W)
        self.set_drill_pos_btn = ttk.Button(size_wrapper, command=self.set_drill_pos, text="Fúró pozíció")
        self.set_drill_pos_btn.pack(fill=tk.BOTH, expand=1)

        size_wrapper = ttk.Frame(right_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.pack_propagate(0)
        size_wrapper.grid(column=1, row=2, sticky=tk.W)
        self.set_camera_pos_btn = ttk.Button(size_wrapper, command=self.set_camera_pos, text="Kamera pozíció")
        self.set_camera_pos_btn.pack(fill=tk.BOTH, expand=1)

        size_wrapper = ttk.Frame(right_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.pack_propagate(0)
        size_wrapper.grid(column=1, row=3, sticky=tk.W)
        self.delta_label = ttk.Label(size_wrapper, text="Delta:")
        self.delta_label.pack(fill=tk.BOTH, expand=1)
        self.setup_delta_xy()

    def _refresh_data(self):
        pass
        # self.setup_delta_xy()
        
    def spec_str(self, floaty_boy):
        return spec_format(floaty_boy, self.FORMAT)

    def setup_delta_xy(self):
        # print("SETUP DELTA")
        for l in self.mm.settings_data:
            if l[0] == "kamera furo x":
                self.delta_x = float(l[1])
                # print(l[1])
            if l[0] == "kamera furo y":
                # print(l[1])
                self.delta_y = float(l[1])
        self.delta_xy_text["text"] = self.spec_str(self.delta_x) + "; " + self.spec_str(self.delta_y)
        self.camera_pos_x = self.delta_x
        self.camera_pos_y = self.delta_y
        self.drill_pos_text["text"] = self.spec_str(0) + "; " + self.spec_str(0)
        self.camera_pos_text["text"] = self.spec_str(self.camera_pos_x) + "; " + self.spec_str(self.camera_pos_y)

    def set_delta_xy(self):
        self.delta_x = self.drill_pos_x - self.camera_pos_x
        self.delta_y = self.drill_pos_y - self.camera_pos_y
        self.delta_xy_text["text"] = self.spec_str(self.delta_x) + "; " + self.spec_str(self.delta_y)
        self.mm.get_instance().change("kamera furo x", self.spec_str(self.delta_x))
        self.mm.get_instance().change("kamera furo y", self.spec_str(self.delta_y))

    def set_drill_pos(self):
        self.drill_pos_x = comm.updating_text_value(self.__rpi.map.get_x_mm)
        self.drill_pos_y = comm.updating_text_value(self.__rpi.map.get_y_mm)
        self.drill_pos_text["text"] = self.spec_str(self.drill_pos_x) + ";" + self.spec_str(self.drill_pos_y)
        self.set_delta_xy()

    def set_camera_pos(self):
        self.camera_pos_x = comm.updating_text_value(self.__rpi.map.get_x_mm)
        self.camera_pos_y = comm.updating_text_value(self.__rpi.map.get_y_mm)
        self.camera_pos_text["text"] = self.spec_str(self.camera_pos_x) + ";" + self.spec_str(self.camera_pos_y)

    def go_to_drill(self):
        comm.go_this_much(self.drill_pos_x - self.camera_pos_x, self.drill_pos_y - self.camera_pos_y)

    def go_to_camera(self):
        comm.go_this_much(self.camera_pos_x - self.drill_pos_x, self.camera_pos_y - self.drill_pos_y)
