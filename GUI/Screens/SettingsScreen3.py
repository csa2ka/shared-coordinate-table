from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from GUI.NumberSetter import NumberSetter
from Communicator import Communicator as comm
from Memory.MemoryManager import MemoryManager as mm

from functools import partial
import tkinter as tk
import tkinter.ttk as ttk


class SettingsScreen3(Screen):

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.numbersetters = []
        self.type = ScreenType.SETTINGS3

    def _build(self):
        self.numbersetters = []
        self.memo = mm.get_instance().get_data(mm.get_instance().settings_data_file_name)
        # print(self.memo)

        s3_title_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8), border=True)
        s3_title_frame.pack_propagate(0)
        s3_title_frame.pack(side=tk.TOP, expand=1, fill=tk.BOTH)

        s3_bottom_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8), border=True)
        s3_bottom_frame.pack_propagate(0)
        s3_bottom_frame.pack(side=tk.BOTTOM, expand=1, fill=tk.X)

        s3_right_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 10), border=True)
        s3_right_frame.pack_propagate(0)
        s3_right_frame.pack(side=tk.RIGHT, expand=1, fill=tk.Y)
        s3_left_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 10), border=True)
        s3_left_frame.pack_propagate(0)
        s3_left_frame.pack(side=tk.LEFT, expand=1, fill=tk.Y)

        s3_middle_frame = ttk.Frame(self.frame, border=True)
        s3_middle_frame.pack(expand=1, fill=tk.BOTH)

        s3_mid_from_x_min = NumberSetter(
            s3_middle_frame, comm.set_x_min_from_border, setter=True, text=mm.get_instance().FROM_X_MIN_BORDER,explain_text_text="Negatív x határ (mm):",
            height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10)*8, border=True
        )
        self.numbersetters.append(s3_mid_from_x_min)
        s3_mid_from_x_plus = NumberSetter(
            s3_middle_frame, comm.set_x_plus_from_border, setter=True, text=mm.get_instance().FROM_X_PLUS_BORDER,explain_text_text="Pozitív x határ (mm):",
            height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8, border=True
        )
        self.numbersetters.append(s3_mid_from_x_plus)

        s3_middle_frame = ttk.Frame(self.frame, border=True)
        s3_middle_frame.pack(expand=1, fill=tk.BOTH)

        s3_mid_from_y_min = NumberSetter(
            s3_middle_frame, comm.set_y_min_from_border, setter=True, text=mm.get_instance().FROM_Y_MIN_BORDER,explain_text_text="Negatív y határ (mm):",
            height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8, border=True
        )
        self.numbersetters.append(s3_mid_from_y_min)
        s3_mid_from_y_plus = NumberSetter(
            s3_middle_frame, comm.set_y_plus_from_border, setter=True, text=mm.get_instance().FROM_Y_PLUS_BORDER,explain_text_text="Pozitív y határ (mm):",
            height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8, border=True
        )
        self.numbersetters.append(s3_mid_from_y_plus)

        s3_middle_frame = ttk.Frame(self.frame, border=True)
        s3_middle_frame.pack(expand=1, fill=tk.BOTH)

        s3_mid_from_x_middle = NumberSetter(
            s3_middle_frame, comm.set_current_position_x, setter=True, text=mm.get_instance().CUR_X_POS,explain_text_text="Aktuális x pozíció (mm):",
            height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8, border=True
        )
        self.numbersetters.append(s3_mid_from_x_middle)
        s3_mid_from_y_middle = NumberSetter(
            s3_middle_frame, comm.set_current_position_y, setter=True, text=mm.get_instance().CUR_Y_POS, explain_text_text="Aktuális y pozíció (mm):",
            height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8, border=True
        )
        self.numbersetters.append(s3_mid_from_y_middle)

        for numbersetter in self.numbersetters:
            for line in self.memo:
                if line[0] == numbersetter.text:
                    numbersetter.value_btn.set_value(str(line[1]))
            # load their data into rpi
            numbersetter.value_btn.trigger_set_value_function()

        s3_mid4_frame = ttk.Frame(s3_middle_frame, height=int(Screen.HEIGHT / 8), border=True)
        s3_mid4_frame.pack(side=tk.TOP, expand=1, fill=tk.Y)
        s3_mid5_frame = ttk.Frame(s3_middle_frame, height=int(Screen.HEIGHT / 8), border=True)
        s3_mid5_frame.pack(side=tk.TOP, expand=1, fill=tk.Y)
        s3_mid6_frame = ttk.Frame(s3_middle_frame, height=int(Screen.HEIGHT / 8), border=True)
        s3_mid6_frame.pack(side=tk.TOP, expand=1, fill=tk.Y)

        s3_MainMenu_btn = ttk.Button(s3_bottom_frame, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                    text="Főmenü", style='ScreenControl.TButton')
        s3_MainMenu_btn.pack(side=tk.BOTTOM, expand=1, fill=tk.Y)
        s3_title_txt = ttk.Label(s3_title_frame, text="Beállítások 3/3")
        s3_title_txt.pack(side=tk.TOP, expand=1, fill=tk.Y)
        s3_go_right_btn = ttk.Button(s3_right_frame, command=partial(comm.switch_screen, ScreenType.SETTINGS1),
                                    text="=>", style='ScreenControl.TButton')
        s3_go_right_btn.pack(side=tk.LEFT, expand=1, fill=tk.Y)
        s3_go_left_btn = ttk.Button(s3_left_frame, command=partial(comm.switch_screen, ScreenType.SETTINGS2), text="<=",
                                    style='ScreenControl.TButton')
        s3_go_left_btn.pack(side=tk.RIGHT, expand=1, fill=tk.Y)

    def reload(self):
        self.memo = mm.get_instance().get_data(mm.get_instance().settings_data_file_name)
        for numbersetter in self.numbersetters:
            for line in self.memo:
                if line[0] == numbersetter.text:
                    numbersetter.value_btn.set_value(str(line[1]))
            # load their data into rpi

    def _refresh_data(self):
        pass
