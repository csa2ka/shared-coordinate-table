from Communicator import Communicator as comm
from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType

import tkinter as tk
import tkinter.ttk as ttk
import threading
from functools import partial


class TestScreen(Screen):

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.TEST_SCREEN

    def _build(self):
        X_btn = ttk.Button(self.frame, command=comm.go100150, text="X:")
        X_btn.grid(column=2, row=2)
        Y_btn = ttk.Button(self.frame, command=comm.go150100, text="Y:")
        Y_btn.grid(column=2, row=3)
        Motor_off_btn = ttk.Button(self.frame, command=comm.motor_off, text="ENABLE off")
        Motor_off_btn.grid(column=2, row=6)
        Motor_on_btn = ttk.Button(self.frame, command=comm.motor_on, text="ENABLE on")
        Motor_on_btn.grid(column=3, row=6)
        Joy_mode_off_btn = ttk.Button(self.frame, command=comm.joymode_off, text="Joytick off")
        Joy_mode_off_btn.grid(column=2, row=7)
        Joy_mode_on_btn = ttk.Button(self.frame, command=comm.joymode_on, text="Joytick on")
        Joy_mode_on_btn.grid(column=3, row=7)
        Mid_setup = ttk.Button(self.frame, command=comm.mid_setup, text="Mid setup")
        Mid_setup.grid(column=7, row=2)
        Corner_setup = ttk.Button(self.frame, command=comm.corner_setup, text="Corner setup")
        Corner_setup.grid(column=7, row=3)
        Test_MainMenu_btn = ttk.Button(self.frame, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                      text="Főmenü")
        Test_MainMenu_btn.grid(column=7, row=11)
        #self.Start_btn = ttk.Button(command=self.start, text="Start")
        #self.Start_btn.grid(in_=self.frame, column=5, row=5)

        self.Label_x = ttk.Label(self.frame, text="0")
        self.Label_x.grid(column=3, row=2)
        self.Label_y = ttk.Label(self.frame, text="0")
        self.Label_y.grid(column=3, row=3)
        self.Label_x_micro = ttk.Label(self.frame, text="-")
        self.Label_x_micro.grid(column=5, row=2)
        self.Label_y_micro = ttk.Label(self.frame, text="-")
        self.Label_y_micro.grid(column=5, row=3)

        # self.Label_x.repeat(Screen.UPDATE_TIME, self.update_xy)  # TODO megoldást keresni rá

    def update_xy(self):
        """
        This func is used by the x, y coordinate display to refresh its value
        :return:
        """
        self.Label_x.value = str(comm.rpi.map.get_x())
        self.Label_y.value = str(comm.rpi.map.get_y())
        self.Label_x_micro.value = str(comm.rpi.map.get_x_m())
        self.Label_y_micro.value = str(comm.rpi.map.get_y_m())

    #    not in use anymore
    #def start(self):
    #    """
    #    This is the function which starts the motorcontrol thread, after you press the START button, also hides the
    #    START button, so you can't start a thread again
    #    :return:
    #    """
    #    motor_thread = threading.Thread(target=comm.motor_up)
    #    motor_thread.setDaemon(True)
    #    motor_thread.start()
    #    self.Start_btn.lower(self.frame)

    def _refresh_data(self):
        pass
