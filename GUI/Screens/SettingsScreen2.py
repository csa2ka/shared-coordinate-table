from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from GUI.NumberSetter import NumberSetter
from GUI.OptionSetter import OptionSetter
from Communicator import Communicator as comm
from Memory.MemoryManager import MemoryManager as mm

from functools import partial
import tkinter as tk
import tkinter.ttk as ttk


class SettingsScreen2(Screen):

    RPI_BTN_WIDTH = 200
    RPI_BTN_HEIGHT = 70

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.SETTINGS2



    def _build(self):
        numbersetters = []
        optionsetters = []

        memo = mm.get_instance().get_data(mm.get_instance().settings_data_file_name)
        # print(memo)

        s2_title_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8), border=True)
        s2_title_frame.pack_propagate(0)
        s2_title_frame.pack(side=tk.TOP, expand=1, fill=tk.BOTH)

        s2_bottom_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8), border=True)
        s2_bottom_frame.pack_propagate(0)
        s2_bottom_frame.pack(side=tk.BOTTOM, expand=1, fill=tk.X)

        s2_right_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 10), border=True)
        s2_right_frame.pack_propagate(0)
        s2_right_frame.pack(side=tk.RIGHT, expand=1, fill=tk.Y)
        s2_left_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 10), border=True)
        s2_left_frame.pack_propagate(0)
        s2_left_frame.pack(side=tk.LEFT, expand=1, fill=tk.Y)

        s2_middle_frame = ttk.Frame(self.frame, border=True)
        s2_middle_frame.pack(expand=1, fill=tk.BOTH)

        s2_xmotor_speed_setter = NumberSetter(
            s2_middle_frame, comm.set_xmotor_step_per_mm, text=mm.get_instance().XMOTOR_STEP_MM, explain_text_text="X motor step/mm arány:",
            height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10)*8, border=True
        )
        numbersetters.append(s2_xmotor_speed_setter)
        s2_ymotor_speed_setter = NumberSetter(
            s2_middle_frame, comm.set_ymotor_step_per_mm, text=mm.get_instance().YMOTOR_STEP_MM, explain_text_text="Y motor step/mm arány:",
            height=int(Screen.HEIGHT / 8), border=True
        )
        numbersetters.append(s2_ymotor_speed_setter)

        s2_delay_setter = NumberSetter(
            s2_middle_frame, comm.set_delay, text=mm.get_instance().DELAY, explain_text_text="Maximális sebesség mm/s:",
            height=int(Screen.HEIGHT / 8), border=True
        )
        numbersetters.append(s2_delay_setter)

        for numbersetter in numbersetters:
            for line in memo:
                if line[0] == numbersetter.text:
                    numbersetter.value_btn.set_value(str(line[1]))
            #load their data into rpi
            numbersetter.value_btn.trigger_set_value_function()

        s2_x_motor_base_direction_setter = OptionSetter(
            s2_middle_frame, self.widgets, comm.set_x_motor_base_direction, [mm.X_LEFT, mm.X_RIGHT], pictures=True,
            text=mm.X_MOTOR_DIRECTION, explain_text_text="X motor irány:", expand=1, fill=tk.Y, height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8,
            side=tk.TOP, border=True
        )
        optionsetters.append(s2_x_motor_base_direction_setter)

        s2_y_motor_base_direction_setter = OptionSetter(
            s2_middle_frame, self.widgets, comm.set_y_motor_base_direction, [mm.Y_UP, mm.Y_DOWN], pictures=True,
            text=mm.Y_MOTOR_DIRECTION, explain_text_text="Y motor irány:", expand=1, fill=tk.Y, height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8,
            side=tk.TOP, border=True
        )
        optionsetters.append(s2_y_motor_base_direction_setter)

        for optionsetter in optionsetters:
            for line in memo:
                if line[0] == optionsetter.text:
                    optionsetter.switch_to(line[1])

        size_wrapper = ttk.Frame(s2_middle_frame, width=self.RPI_BTN_WIDTH * 3, height=int(Screen.HEIGHT / 8))
        size_wrapper.pack_propagate(0)
        size_wrapper.pack(side=tk.BOTTOM)
        SD_read = ttk.Button(size_wrapper, command=comm.read_SD, text="Olvasás az SD kártyáról.", style='RPIC.TButton')
        SD_read.pack(side=tk.RIGHT, fill=tk.Y, expand=True)

        # size_wrapper = ttk.Frame(s2_middle_frame, width=self.RPI_BTN_WIDTH * 3, height=int(Screen.HEIGHT / 8))
        # size_wrapper.pack_propagate(0)
        # size_wrapper.pack(side=tk.BOTTOM)
        SD_write = ttk.Button(size_wrapper, command=comm.write_SD, text="Írása az SD kártyára.", style='RPIC.TButton')
        SD_write.pack(side=tk.LEFT, fill=tk.Y, expand=True)


        #s2_mid4_frame = ttk.Frame(s2_middle_frame, height=int(Screen.HEIGHT / 8), border=True)
        #s2_mid4_frame.pack(side=tk.TOP, expand=1, fill=tk.Y)
        s2_mid5_frame = ttk.Frame(s2_middle_frame, height=int(Screen.HEIGHT / 8), border=True)
        s2_mid5_frame.pack(side=tk.TOP, expand=1, fill=tk.Y)
        s2_mid6_frame = ttk.Frame(s2_middle_frame, height=int(Screen.HEIGHT / 8), border=True)
        s2_mid6_frame.pack(side=tk.TOP, expand=1, fill=tk.Y)

        s2_MainMenu_btn = ttk.Button(s2_bottom_frame, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                    text="Főmenü", style='ScreenControl.TButton')
        s2_MainMenu_btn.pack(side=tk.BOTTOM, expand=1, fill=tk.Y)
        s2_title_txt = ttk.Label(s2_title_frame, text="Beállítások 2/3")
        s2_title_txt.pack(side=tk.TOP, expand=1, fill=tk.Y)
        s2_go_right_btn = ttk.Button(s2_right_frame, command=partial(comm.switch_screen, ScreenType.SETTINGS3),
                                    text="=>", style='ScreenControl.TButton')
        s2_go_right_btn.pack(side=tk.LEFT, expand=1, fill=tk.Y)
        s2_go_left_btn = ttk.Button(s2_left_frame, command=partial(comm.switch_screen, ScreenType.SETTINGS1), text="<=",
                                    style='ScreenControl.TButton')
        s2_go_left_btn.pack(side=tk.RIGHT, expand=1, fill=tk.Y)

    def _refresh_data(self):
        pass
