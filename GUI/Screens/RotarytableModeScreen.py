from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from GUI.NumberUpdater import NumberUpdater
from Communicator import Communicator as comm
from Memory.MemoryManager import MemoryManager as mm
from GUI.RotaryTables import RotaryTables

import tkinter as tk
import tkinter.ttk as ttk
from functools import partial


class RotarytableModeScreen(Screen):

    def __init__(self, gui_parent, rpi, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.ROTARY_TABLE_MODE
        self.__rpi = rpi
        self.rotarytable_panel = None

    def _build(self):
        rot_title_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8), border=True)
        rot_title_frame.pack_propagate(0)
        rot_title_frame.pack(side=tk.TOP, expand=0, fill=tk.X)
        # rot_left_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 4), border=True)
        # rot_left_frame.pack_propagate(0)
        # rot_left_frame.pack(side=tk.LEFT, expand=0, fill=tk.Y)
        rot_right_frame = ttk.Frame(self.frame, border=True)
        rot_right_frame.pack(side=tk.RIGHT, expand=1, fill=tk.BOTH)

        Rot_XY_display = NumberUpdater(rot_title_frame, comm.updating_text_value, self.__rpi.map.get_x_mm,
                                       self.__rpi.map.get_y_mm, text="Abs:", border=True)
        rotarytable_panel = RotaryTables(rot_right_frame, mm, comm.go_to_rotary)
        Rot_position_display = NumberUpdater(rot_title_frame, rotarytable_panel.get_current_position, is_int=True,
                                             text="Pozíció:", border=True, myside=tk.RIGHT)

        # rot_title_txt = ttk.Label(rot_title_frame, text="Körasztal mód")
        # rot_title_txt.pack(side=tk.TOP, expand=1, fill=tk.Y)

    def _refresh_data(self):
        pass
        # if self.rotarytable_panel is not None:
        #    self.rotarytable_panel.refresh_data()
