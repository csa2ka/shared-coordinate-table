from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm

import tkinter as tk
import tkinter.ttk as ttk
from functools import partial
from os import path


class PopupScreen(Screen):

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.POPUP
        self.previous_screen = None

    def _build(self):
        # menu_frame2 = tk.Frame(self.frame, border=1)
        # menu_frame2.place(relx=0.5, rely=0.5, anchor=tk.CENTER, width=300, height=400)

        width = 800
        height = 480
        large_font = ("Arial", 22)
        #popup = tk.Tk()
        #popup.overrideredirect(1)
        #popup.geometry("%dx%d+0+0" % (width, height))
        #popup.wm_title("Message")
        label_wrapper = ttk.Frame(self.frame, width=width, height=100)
        label_wrapper.place(relx=0.5, rely=0.3, anchor=tk.CENTER)
        label_wrapper.pack_propagate(0)

        self.label = ttk.Label(label_wrapper, font=large_font, text="Felugró ablak")
        self.label.pack(side=tk.BOTTOM)

        size_wrapper = ttk.Frame(self.frame, width=150, height=100)
        size_wrapper.place(relx=0.5, rely=0.8, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        B1 = tk.Button(size_wrapper, text="OK", command=self.close)
        B1.pack(side=tk.BOTTOM, expand=True, fill=tk.BOTH)
        B1['font'] = large_font
        B1.bind("<Button-1>", Screen.pitty)

    def _refresh_data(self):
        pass

    def close(self):
        # print("closing the popup")
        comm.close_popup(self.previous_screen)

    def open_up(self, warning, prev_screen):
        # print("opened the popup")
        self.label['text'] = warning
        self.previous_screen = prev_screen
