from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm

import tkinter as tk
import tkinter.ttk as ttk
from functools import partial


class AskSetupScreen(Screen):
    BUTTON_WIDTH = 200
    BUTTON_HEIGHT = 100

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.ASK_SETUP

    def _build(self):
        size_wrapper = ttk.Frame(self.frame)
        size_wrapper.place(relx=0.5, rely=0.2, anchor=tk.CENTER, relheight=0.3, relwidth=1)
        question = ttk.Label(size_wrapper, text="Szeretne középreállítást csinálni?")
        question.pack(side=tk.BOTTOM)

        answer_sheet_frame = ttk.Frame(self.frame, height=Screen.HEIGHT/4, width=Screen.WIDTH)
        answer_sheet_frame.place(relx=0.5, rely=0.6, anchor=tk.CENTER, relheight=0.4, relwidth=1)

        size_wrapper = ttk.Frame(answer_sheet_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.place(relx=0.25, rely=0.3, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        yes_btn = ttk.Button(size_wrapper, command=partial(comm.switch_screen, ScreenType.CHOOSE_SETUP), text="Igen",
                             style='ScreenControl.TButton')
        yes_btn.pack(fill=tk.BOTH, expand=True)

        size_wrapper = ttk.Frame(answer_sheet_frame, width=self.BUTTON_WIDTH, height=self.BUTTON_HEIGHT)
        size_wrapper.place(relx=0.75, rely=0.3, anchor=tk.CENTER)
        size_wrapper.pack_propagate(0)
        no_btn = ttk.Button(size_wrapper, command=comm.finnish_setup, text="Nem",
                            style='ScreenControl.TButton')
        no_btn.pack(fill=tk.BOTH, expand=True)

    def _refresh_data(self):
        pass
