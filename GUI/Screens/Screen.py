import tkinter as tk
import tkinter.ttk as ttk
from abc import ABC, abstractmethod
from time import sleep

class Screen(ABC):
    """
    Tartalmazza az adott oldal elemeit, dobozok, gombok, stb.. Arra szolgál, hogy azokat elrejtse, megmutassa,
    csoportosítsa, hogy közös beállítások is évrényesülhessenek rájuk.
    Egy pár szerintem idetartozó globális változót is itt van.
    """
    previous_screen = None  # fincsi globális változó
    UPDATE_TIME = 30  # milyen gyakran frissítse a képernyőn lévő adatokat ez kb 33 FPS, 33-szor másodpercenként

    keyboard_command = None
    numpad_command = None

    WIDTH = 800
    HEIGHT = 480

    rpi = None

    type = None

    def __init__(self, gui_parent, layout="auto"):
        self.layout = layout
        self.frame = ttk.Frame(gui_parent)
        self.frame.layout = self.layout
        self.frame.pack()

        self.built = False

    @abstractmethod
    def _build(self):
        # print("Building screen")
        pass

    @abstractmethod
    def _refresh_data(self):
        #print("Refreshing Data)")
        pass

    def build_if_not_built(self):
        if not self.built:
            self._build()
            self.musician_btns()
        self.built = True

    def musician_btns(self):
        for widget in self.widgets:
            if type(widget) is ttk.Button:
                widget.bind("<Button-1>", self.pitty)

    @classmethod
    def pitty(cls, event):
        cls.rpi.turn_buzzer_on()
        #print("pitty eleje")
        #self.frame.after(10, self.end_pitty)
        sleep(0.01)
        cls.end_pitty()

    @classmethod
    def end_pitty(cls):
        cls.rpi.turn_buzzer_off()
        #print("pitty vége")

    @property
    def widgets(self):
        _list = self.frame.winfo_children()

        for item in _list:
            if item.winfo_children():
                _list.extend(item.winfo_children())

        return _list

    def hide(self):
        """
        Úgy tűntetünk el egy screen-t, hogy a box-át eltűntetjük
        """
        self.frame.pack_forget()

    def show(self):
        """
        A hide ellenkezője
        """
        self.build_if_not_built()
        # self._refresh_data()
        self.frame.pack(fill=tk.BOTH, expand=1)

    @classmethod
    def set_up_commands(cls, com1, com2):
        """
        Itt elmentek függvényeket, hogy később lehessen őket használni
        """
        cls.keyboard_command = com1
        cls.numpad_command = com2

    @classmethod
    def set_rpi(cls, rpi):
        cls.rpi = rpi
