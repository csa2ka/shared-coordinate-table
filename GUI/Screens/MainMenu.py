from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm

import tkinter as tk
import tkinter.ttk as ttk
from functools import partial
from os import path



class MainMenu(Screen):

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.MAIN_MENU

    def _build(self):
        #backy_path = path.join('.', 'img', 'background.png')
        #self.background_image = tk.PhotoImage(file=backy_path)
        #self.background_label = tk.Label(self.frame, image=self.background_image)
        #self.background_label.place(x=0, y=0, relwidth=1, relheight=1)

        menu_frame2 = tk.Frame(self.frame, border=1)
        menu_frame2.place(relx=0.5, rely=0.5, anchor=tk.CENTER, width=300, height=400)


        #Test_Screen_button = ttk.Button(menu_frame2, style='MainMenu.TButton',
        #                                command=partial(comm.switch_screen, ScreenType.TEST_SCREEN),
        #                                text="Teszt ")\
        #    .pack(side=tk.TOP, expand=True, fill=tk.BOTH)
        Settings1_Screen_button = ttk.Button(menu_frame2, command=partial(comm.switch_screen, ScreenType.SETTINGS1),
                                             text="Beállítások", style='MainMenu.TButton')\
            .pack(side=tk.TOP, expand=True, fill=tk.BOTH)
        Coordinate_Mode_Screen_button = ttk.Button(menu_frame2, style='MainMenu.TButton',
                                                   command=partial(comm.switch_screen, ScreenType.COORDINATE_MODE),
                                                   text="Koordináta mód")\
            .pack(side=tk.TOP, expand=True, fill=tk.BOTH)
        Rotary_Table_Mode_Screen_button = ttk.Button(menu_frame2, style='MainMenu.TButton',
                                                     command=partial(comm.switch_screen, ScreenType.ROTARY_TABLE_MODE),
                                                     text="Körasztal mód")\
            .pack(side=tk.TOP, expand=True, fill=tk.BOTH)
        Camndrill_Mode_Screen_button = ttk.Button(menu_frame2, style='MainMenu.TButton',
                                                  command=partial(comm.switch_screen, ScreenType.CAM_N_DRILL_MODE),
                                                  text="Kamera-fúró mód")\
            .pack(side=tk.TOP, expand=True, fill=tk.BOTH)
        # Quit_btn = ttk.Button(menu_frame2, style='MainMenu.TButton',
        #                                          command=comm.quit_to_desktop,
        #                                          text="Kilépés")\
        #    .pack(side=tk.TOP, expand=True, fill=tk.BOTH)
        Quit_btn = ttk.Button(menu_frame2, style='MainMenu.TButton',
                                                  command=comm.quit_and_shutdown,
                                                  text="Kikapcsolás")\
            .pack(side=tk.TOP, expand=True, fill=tk.BOTH)

    def _refresh_data(self):
        pass

    def qqqqqqqqqqqq(self):
        #backy_path = path.join('.', 'img', 'csillarmika.PNG')
        #background_image = tk.PhotoImage(file=backy_path)
        #self.background_label.configure(image=background_image)
        #self.background_label.photo = background_image
        pass