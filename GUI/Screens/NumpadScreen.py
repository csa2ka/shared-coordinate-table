from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm
from GUI.Numpad import Numpad


class NumpadScreen(Screen):

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.NUMPAD

    def _build(self):
        numpad = Numpad(self.frame, comm.switch_screen)
        self.numpad = numpad

    def get_numpad_working(self, target, initial_text, prev_screen, command, arg=None, floating_digits=3):
        self.numpad.get_numpad_working(target, initial_text, prev_screen, command, arg, floating_digits)

    def _refresh_data(self):
        pass
