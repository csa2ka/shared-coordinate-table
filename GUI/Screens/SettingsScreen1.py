from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from GUI.OptionSetter import OptionSetter
import GUI.icons as icons
from Communicator import Communicator as comm
from Memory.MemoryManager import MemoryManager as mm
from GUI.NumberSetter import NumberSetter

from functools import partial
import tkinter as tk
import tkinter.ttk as ttk


class SettingsScreen1(Screen):

    RPI_BTN_WIDTH = 200
    RPI_BTN_HEIGHT = 70

    def __init__(self, gui_parent, layout="auto"):
        super().__init__(gui_parent, layout)
        self.type = ScreenType.SETTINGS1

    def _build(self):
        """
        We also get the data from setting.txt, this is why I put the OptionSetters in a optionsetters array
        :return:
        """
        optionsetters = []
        memo = mm.get_instance().get_data(mm.get_instance().settings_data_file_name)
        # print(memo)

        s1_title_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8))
        s1_title_frame.pack_propagate(0)
        s1_title_frame.pack(side=tk.TOP, expand=1, fill=tk.BOTH)

        s1_bottom_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8))
        s1_bottom_frame.pack_propagate(0)
        s1_bottom_frame.pack(side=tk.BOTTOM, expand=1, fill=tk.BOTH)

        s1_right_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 10))
        s1_right_frame.pack_propagate(0)
        s1_right_frame.pack(side=tk.RIGHT, expand=1, fill=tk.Y)

        s1_left_frame = ttk.Frame(self.frame, width=int(Screen.WIDTH / 10))
        s1_left_frame.pack_propagate(0)
        s1_left_frame.pack(side=tk.LEFT, expand=1, fill=tk.Y)

        s1_middle_frame = ttk.Frame(self.frame, height=int(Screen.HEIGHT / 8) * 6, width=int(Screen.WIDTH / 10) * 8,
                                    relief='raised')
        s1_middle_frame.pack_propagate(0)
        s1_middle_frame.pack(expand=1, fill=tk.BOTH)

        s1_mid1_control_mode_setter = OptionSetter(
            s1_middle_frame, self.widgets, comm.set_control_mode, [mm.DEG45, mm.LINEAR, mm.AXIAL], pictures=True,
            text=mm.CONTROL_MODE, explain_text_text="Vezérlési mód:", expand=1, fill=tk.Y, height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8,
            side=tk.TOP, border=True
        )
        optionsetters.append(s1_mid1_control_mode_setter)

        # s1_mid2_software_microstep_setter = OptionSetter(
        #     s1_middle_frame, self.widgets, comm.set_software_microstep,
        #     [mm.FULLSTEP, mm.HALFSTEP, mm.QUARTERSTEP, mm.EIGHTHSTEP, mm.SIXTEENTHSTEP],
        #     text=mm.MICROSTEP_SW, expand=1, fill=tk.Y, height=int(Screen.HEIGHT / k), width=int(Screen.WIDTH / 10) * k,
        #     side=tk.TOP, border=True, error_message="A puha áru nem lehet kisebb a keménynél!"
        # )
        # optionsetters.append(s1_mid2_software_microstep_setter)
        #

        # , mm.EIGHTHSTEP, mm.SIXTEENTHSTEP],
        s1_mid3_hardware_microstep_setter = OptionSetter(
            s1_middle_frame, self.widgets, comm.set_hardware_microstep,
            [mm.FULLSTEP, mm.HALFSTEP, mm.QUARTERSTEP],
            text=mm.MICROSTEP_HW, explain_text_text="Mikrostep:", expand=1, fill=tk.Y, height=int(Screen.HEIGHT / 8), width=int(Screen.WIDTH / 10) * 8,
            side=tk.TOP, border=True, error_message="A kemény áru nem lehet nagyobb a puhánál!"
        )
        optionsetters.append(s1_mid3_hardware_microstep_setter)

        # s1_mid4_microstep_setting_enabler = OptionSetter(
        #     s1_middle_frame, self.widgets, comm.set_microstep_setter_enable, [mm.NO, mm.YES],
        #     text=mm.MICROSTEP_ENABLE, expand=1, fill=tk.Y, height=int(Screen.HEIGHT / k),
        #     width=int(Screen.WIDTH / 10) * k, side=tk.TOP, border=True
        # )
        # optionsetters.append(s1_mid4_microstep_setting_enabler)

        s1_mid5_overtravel_switch = OptionSetter(
            s1_middle_frame, self.widgets, comm.set_overtravel_switch, [mm.NO, mm.YES],text=mm.OVERTRAVEL_SWITCH,
            explain_text_text="Végálláskapcsoló:", expand=1, fill=tk.Y, height=int(Screen.HEIGHT / 8),
            width=int(Screen.WIDTH / 10) * 8, side=tk.TOP, border=True
        )
        optionsetters.append(s1_mid5_overtravel_switch)

        size_wrapper = ttk.Frame(s1_middle_frame, width=self.RPI_BTN_WIDTH*2, height=self.RPI_BTN_HEIGHT)
        size_wrapper.pack_propagate(0)
        size_wrapper.pack(side=tk.BOTTOM)
        Corner_setup = ttk.Button(size_wrapper, command=comm.corner_setup, text="Sarokba állítás", style='RPIC.TButton')
        Corner_setup.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)


        size_wrapper_2 = ttk.Frame(s1_middle_frame, width=self.RPI_BTN_WIDTH*2, height=self.RPI_BTN_HEIGHT)
        size_wrapper_2.pack_propagate(0)
        size_wrapper_2.pack(side=tk.BOTTOM)
        Mid_setup = ttk.Button(size_wrapper, command=comm.mid_setup, text="Középre állítás", style='RPIC.TButton')
        Mid_setup.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        Mid_manual_setup = ttk.Button(size_wrapper_2, command=partial(comm.switch_screen, ScreenType.MANUAL_SETUP), text="Kézi középre állítás", style='RPIC.TButton')
        Mid_manual_setup.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        for optionsetter in optionsetters:
            for line in memo:
                if line[0] == optionsetter.text:
                    #print(line[0], "   - - - -   ", optionsetter.text)
                    optionsetter.switch_to(line[1])


        s1_MainMenu_btn = ttk.Button(s1_bottom_frame, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                     text="Főmenü", style='ScreenControl.TButton')
        s1_MainMenu_btn.pack(side=tk.BOTTOM, fill=tk.Y, expand=1)
        s1_title_txt = ttk.Label(s1_title_frame, text="Beállítások 1/3")
        s1_title_txt.pack(side=tk.TOP, fill=tk.Y, expand=1)
        s1_go_right_btn = ttk.Button(s1_right_frame, command=partial(comm.switch_screen, ScreenType.SETTINGS2),
                                     text="=>", style='ScreenControl.TButton')
        s1_go_right_btn.pack(side=tk.LEFT, expand=1, fill=tk.Y)
        s1_go_left_btn = ttk.Button(s1_left_frame, command=partial(comm.switch_screen, ScreenType.SETTINGS3),
                                    text="<=", style='ScreenControl.TButton')
        s1_go_left_btn.pack(side=tk.RIGHT, expand=1, fill=tk.Y)

    def _refresh_data(self):
        pass
