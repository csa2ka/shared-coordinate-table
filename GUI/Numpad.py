import tkinter as tk
import tkinter.ttk as ttk
from functools import partial

from Communicator import Communicator as comm
from utils.domi_funcs import number_digit_length
from utils.domi_funcs import popup_message
from utils.balazs_funcs import spec_format


class Numpad:
    w = 4  # width
    h = 2  # height

    def __init__(self, guiparent, switchscreen_func):
        #print("numpadinit")
        self.starting_value = None

        self.floating_digits = 3
        self.arg = None
        self.command = None
        self.prev_screen = None
        self.guiparent = guiparent
        self.switchscreen_func = switchscreen_func
        self.max_length = 6
        self.target = None
        self.frame = ttk.Frame(guiparent, border=True)
        self.frame.pack()
        self.input = ttk.Label(self.frame, style='KeyBoard.TLabel', text="")
        self.input.grid(column=0, row=0, columnspan=3, rowspan=1, sticky="ns")

        wid = 150
        hei = 75

        wrapper = ttk.Frame(self.frame, width=wid, height=hei)
        wrapper.pack_propagate(0)
        self.submit_btn = ttk.Button(wrapper, style='KeyBoard.TButton', command=self.submit, text="OK")  #
        self.submit_btn.pack(fill=tk.BOTH, expand=1)
        wrapper.grid(column=2, row=1, sticky="nsew")

        wrapper = ttk.Frame(self.frame, width=wid, height=hei)
        wrapper.pack_propagate(0)
        self.cancel_btn = ttk.Button(wrapper, style='KeyBoard.TButton', command=self.cancel, text="Del")
        self.cancel_btn.pack(fill=tk.BOTH, expand=1)
        wrapper.grid(column=1, row=1, sticky="nsew")

        wrapper = ttk.Frame(self.frame, width=wid, height=hei)
        wrapper.pack_propagate(0)
        self.clear_btn = ttk.Button(wrapper, style='KeyBoard.TButton', command=self.clear, text="Clear")
        self.clear_btn.pack(fill=tk.BOTH, expand=1)
        wrapper.grid(column=0, row=1, sticky="nsew")

        for i in range(1, 10):
            wrapper = ttk.Frame(self.frame, width=wid, height=hei)
            wrapper.pack_propagate(0)
            btn = ttk.Button(wrapper, style='KeyBoard.TButton', command=partial(self.pressed_button, i), text=i)
            btn.pack(fill=tk.BOTH, expand=1)
            wrapper.grid(column=(i - 1) % 3, row=(i - 1) // 3 + 2, sticky="nsew")
        wrapper = ttk.Frame(self.frame, width=wid, height=hei)
        wrapper.pack_propagate(0)
        button0 = ttk.Button(wrapper, style='KeyBoard.TButton', command=partial(self.pressed_button, "0"), text="0")
        button0.pack(fill=tk.BOTH, expand=1)
        wrapper.grid(column=1, row=5, sticky="nsew")

        wrapper = ttk.Frame(self.frame, width=wid, height=hei)
        wrapper.pack_propagate(0)
        self.buttonDot = ttk.Button(wrapper, style='KeyBoard.TButton', command=partial(self.pressed_button, "."),
                                    text=".")
        self.buttonDot.pack(fill=tk.BOTH, expand=1)
        wrapper.grid(column=0, row=5, sticky="nsew")

        wrapper = ttk.Frame(self.frame, width=wid, height=hei)
        wrapper.pack_propagate(0)
        self.buttonPlusMinus = ttk.Button(wrapper, style='KeyBoard.TButton', command=self.plus_minus, text="(-)")
        self.buttonPlusMinus.pack(fill=tk.BOTH, expand=1)
        wrapper.grid(column=2, row=5, sticky="nsew")

    def cancel(self):
        self.input["text"] = self.input["text"][:-1]

    @staticmethod
    def intfloat(stringy):
        while (stringy[-1] == '.' or stringy[-1] == '0') and '.' in stringy:
            stringy = stringy[:-1]
        return float(stringy)

    def submit(self):
        submit_value = self.input["text"]
        if not submit_value == "":
            if submit_value == self.starting_value:
                pass
                #na mi legyen akkor?
            submit_value = self.intfloat(submit_value)
            # if self.floating_digits == 0:
            #    submit_value = int("{:3.0f}".format(submit_value))
            # else:
            #    submit_value = "{:3.2f}".format(submit_value)
            submit_value = spec_format(submit_value, self.floating_digits)
            if self.floating_digits == 0:
                submit_value = int(submit_value)
            self.target["text"] = submit_value
            self.switchscreen_func(self.prev_screen)
            if self.arg is None:
                self.command(submit_value)
            else:
                self.command(submit_value, self.arg)
        else:
            comm.popup("Nincs beírva semmi!")

    def plus_minus(self):
        if '.' in self.input["text"]:
            if self.input["text"][-1] == '.':
                self.input["text"] = str(int(float(self.input["text"]) * (-1))) + '.'
            else:
                self.input["text"] = str(float(self.input["text"]) * (-1))
        else:
            if not "" == self.input["text"]:
                self.input["text"] = str(int(float(self.input["text"]) * (-1)))

    def clear(self):
        self.input["text"] = ""

    def is_floatable(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    def pressed_button(self, v):
        if len(self.input["text"]) > 0:
            if number_digit_length(self.input["text"]) == self.max_length:
                return False
        elif (self.input["text"] == "0" or self.input["text"] == "0.") and v == '0':
            return False
        new = self.input["text"] + str(v)
        if self.is_floatable(new):
            self.input["text"] += (str(v))
        if len(self.input["text"]) > 1:
            if self.input["text"][0] == '0' and self.input["text"][1] == '0':
                self.input["text"] = self.input["text"][1:]

    def get_numpad_working(self, target, initial_text, prev_screen, command, arg=None, floating_digits=3):
        #print("numpad working gotten")
        self.input["text"] = str(initial_text)
        self.starting_value = str(initial_text)
        #print(self.input["text"])
        if self.input["text"] == "0.000":
            self.clear()
        self.prev_screen = prev_screen
        self.command = command
        self.target = target
        self.arg = arg
        self.floating_digits = floating_digits
