import tkinter as tk
import tkinter.ttk as ttk
from functools import partial

from Communicator import Communicator as comm
#from utils.domi_funcs import comm.popup


class Keyboard:
    w = 4  # width
    h = 2  # height
    limit = 8
    """
            ( '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
            ( 'Q', 'W', 'E', 'R', 'T', 'Z', 'U', 'I', 'O', 'P'),
            ( 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'),
            ( 'Z', 'X', 'C', 'V', 'B', 'N', 'M'),
            ( ' ')
    """
    button_array = [
        ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
        ['Q', 'W', 'E', 'R', 'T', 'Z', 'U', 'I', 'O', 'P'],
        ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'],
        ['Y', 'X', 'C', 'V', 'B', 'N', 'M', "OK"],
        ["CLEAR", ' ', "DEL"]
    ]

    def __init__(self, guiparent, switchscreen_func):
        self.arg = None
        self.command = None
        self.prev_screen = None
        self.guiparent = guiparent
        self.switchscreen_func = switchscreen_func
        self.target = None
        self.frame = ttk.Frame(self.guiparent, border=True)
        self.frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        self.input = ttk.Label(self.frame, style='KeyBoard.TLabel')
        self.input.grid(column=0, row=0, sticky="ns")
        self.input.text_size = 30
        self.black_list = []

        for line in Keyboard.button_array:
            frame_other = ttk.Frame(self.frame, border=True)
            frame_other.grid(column=0, row=Keyboard.button_array.index(line) + 1)

            for ch in line:
                w = 75
                h = w
                if ch == ' ':
                    w = 5*w
                if ch == "DEL":
                    w = 1.5*w
                if ch == "CLEAR":
                    w = 2 * w
                wrapper = ttk.Frame(frame_other, width=w, height=h)
                wrapper.pack_propagate(0)
                btn = ttk.Button(wrapper, style='KeyBoard.TButton', command=partial(self.pressed_button, ch), text=ch)
                btn.pack(fill=tk.BOTH, expand=1)
                wrapper.grid(column=line.index(ch), row=0)

    def delete(self):
        self.input["text"] = self.input["text"][:-1]

    def submit(self):
        submit_value = self.input.cget("text")
        if submit_value == "JULZSOMI":
            comm.quit_to_desktop()
        if submit_value == "QQQQQQQQ":
            comm.release_the_poni()
        if submit_value == "":
            comm.popup("Nincs beírva semmi!")
        elif submit_value not in self.black_list:
            self.target["text"] = submit_value
            self.switchscreen_func(self.prev_screen)
            if self.arg is None:
                self.command(submit_value)
            else:
                self.command(submit_value, self.arg)
        else:
            comm.popup("Ilyen név már létezik!")

    def clear(self):
        self.input["text"] = ""

    def pressed_button(self, v):
        if v == "DEL":
            self.delete()
        elif v == "OK":
            self.submit()
        elif v == "CLEAR":
            self.clear()
        else:
            if len(self.input["text"]) < self.limit:
                self.input["text"] += (str(v))
            else:
                comm.popup("Elérte a maximális karakterszámot!")

    def start_keyboard(self, target, initial_text, prev_screen, command, arg=None, blacklist=None):
        self.input["text"] = initial_text
        self.prev_screen = prev_screen
        self.command = command
        self.target = target
        self.arg = arg
        self.black_list = blacklist
        # self.black_list.append("")
