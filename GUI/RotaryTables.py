import tkinter as tk
import tkinter.ttk as ttk
from GUI.ValueSetterButton import ValueSetterButton
from GUI.Screens.Screen import Screen
from Memory.MemoryManager import MemoryManager as mm
from Memory.RotaryTable import RotaryTable
from functools import partial

import GUI.icons as icons
from GUI.Screens.ScreenType import ScreenType
from Communicator import Communicator as comm
from utils.domi_funcs import popup_message
from utils.domi_funcs import new_unique_name


class RotaryTables:
    """
            Egyedi widget, több guizero widget-ből.
            Létrehoz egy ttk.Frame-ot, benne egy cím ttk.Label-et és egy Számkiíró ttk.Buttont, amire kattintva előjön egy Numpad.
            ___________________________________
            |TEXT                  NUMBERnGOMB|
            ˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘˘ˇˇˇˇˇˇˇˇˇ
    """
    SLOT = 5
    HH = int(Screen.HEIGHT / (SLOT + 2))

    SETTERBTN_WIDTH = 200
    NAMESETTERBTNWIDTH = 320

    RED_BTNS_X = 515

    BOTTOM_BTN_WITH = 150

    rtables_data = []  # pointgroup
    rtable_widgets = []

    first_rtable_to_show = 0
    in_this_rtable = None

    jump_to = 0

    floating_digits = 3

    def __init__(
            self, gui_parent, memory_manager, command, border=0
    ):
        # self.mm = memory_manager
        self.gui_parent = gui_parent
        self.command = command
        self.border = border

        self.current_position = 0

        self.full_frame_hider = ttk.Frame(gui_parent, relief='raised')
        self.full_frame_hider.pack_propagate(0)
        self.full_frame_hider.pack(side=tk.TOP, expand=1, fill=tk.BOTH)

        self.frame_inside = ttk.Frame(gui_parent)
        self.frame_inside.pack_propagate(0)
        self.frame_inside.place(in_=self.full_frame_hider, relx=0.5, rely=0.5, anchor=tk.CENTER, relheight=1,
                                relwidth=1)

        self.frame_outside = ttk.Frame(gui_parent)
        self.frame_outside.pack_propagate(0)
        self.frame_outside.place(in_=self.full_frame_hider, relx=0.5, rely=0.5, anchor=tk.CENTER, relheight=1,
                                 relwidth=1)

        self.outside_btn_hider = ttk.Frame(self.frame_outside)
        self.outside_btn_hider.pack_propagate(0)
        self.outside_btn_hider.pack(expand=1, fill=tk.BOTH)

        self.frames_outside = []

        for i in range(self.SLOT + 1):
            frame = ttk.Frame(self.frame_outside, height=self.HH)
            frame.pack_propagate(0)
            frame.pack(in_=self.outside_btn_hider, side=tk.TOP, expand=1, fill=tk.BOTH)
            self.frames_outside.append(frame)

        self.outside_buttons = []
        for i in range(self.SLOT):
            button = ttk.Button(self.frames_outside[i], text="asd", command=partial(self.go_into_rtable, i))
            button.pack(expand=1, fill=tk.BOTH)
            self.outside_buttons.append(button)

        self.frames_inside = []

        for i in range(self.SLOT + 1):
            frame = ttk.Frame(self.frame_inside, height=self.HH)
            frame.pack_propagate(0)
            frame.pack(side=tk.TOP, expand=1, fill=tk.X)
            self.frames_inside.append(frame)

        self.up_btn_wrapper = ttk.Frame(self.frames_outside[self.SLOT], width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.up_btn_wrapper.pack_propagate(0)
        self.up_btn = ttk.Button(self.up_btn_wrapper, command=self.up, text="Le", image=icons.DOWN)
        self.up_btn.pack(expand=1, fill=tk.BOTH)
        self.up_btn_wrapper.pack(side=tk.RIGHT)
        # self.up_btn = ttk.Button(self.frames_outside[self.SLOT], command=self.up, text="Le")
        # self.up_btn.pack(side=tk.RIGHT, expand=1, fill=tk.Y)

        self.down_btn_wrapper = ttk.Frame(self.frames_outside[self.SLOT], width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.down_btn_wrapper.pack_propagate(0)
        self.down_btn = ttk.Button(self.down_btn_wrapper, command=self.down, text="Fel", image=icons.UP)
        self.down_btn.pack(expand=1, fill=tk.BOTH)
        self.down_btn_wrapper.pack(side=tk.RIGHT)
        # self.down_btn = ttk.Button(self.frames_outside[self.SLOT], command=self.down, text="Fel")
        # self.down_btn.pack(side=tk.RIGHT, expand=1, fill=tk.Y)

        # self.jump_btn = ttk.Button(self.frames_inside[self.SLOT-1], command=self.jump, text="Ugrás",
        #                            style='RPIC.TButton')
        # self.jump_btn.pack(side=tk.LEFT, fill=tk.Y)

        rot_MainMenu_Frame = ttk.Frame(self.frames_inside[self.SLOT], width=self.BOTTOM_BTN_WITH,
                                       height=RotaryTables.HH)
        rot_MainMenu_Frame.pack_propagate(0)
        rot_MainMenu_Frame.pack(side=tk.LEFT, fill=tk.X)
        rot_MainMenu_btn = ttk.Button(rot_MainMenu_Frame, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                      text="Főmenü", style='ScreenControl.TButton')
        rot_MainMenu_btn.pack(expand=1, fill=tk.BOTH)

        # self.next_btn = ttk.Button(self.frames_inside[self.SLOT], command=self.next, text="Következő")
        # self.next_btn.pack(side=tk.LEFT, expand=1, fill=tk.Y)

        rot_MainMenu_Frame = ttk.Frame(self.frames_outside[self.SLOT], width=self.BOTTOM_BTN_WITH,
                                       height=RotaryTables.HH)
        rot_MainMenu_Frame.pack_propagate(0)
        rot_MainMenu_Frame.pack(side=tk.LEFT, fill=tk.X)
        rot_MainMenu_btn = ttk.Button(rot_MainMenu_Frame, command=partial(comm.switch_screen, ScreenType.MAIN_MENU),
                                      text="Főmenü", style='ScreenControl.TButton')
        rot_MainMenu_btn.pack(expand=1, fill=tk.BOTH)

        self.add_btn_wrapper = ttk.Frame(self.frames_outside[self.SLOT], width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.add_btn_wrapper.pack_propagate(0)
        self.add_btn = ttk.Button(self.add_btn_wrapper, command=self.add, text="Hozzáad", image=icons.ADD)
        self.add_btn.pack(expand=1, fill=tk.BOTH)
        self.add_btn_wrapper.pack(side=tk.LEFT)

        self.back_btn_wrapper = ttk.Frame(self.frames_inside[self.SLOT], width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.back_btn_wrapper.pack_propagate(0)
        self.back_btn = ttk.Button(self.back_btn_wrapper, command=self.get_out_of_rtable, text="Vissza",
                                   style='ScreenControl.TButton')
        self.back_btn.pack(expand=1, fill=tk.BOTH)
        self.back_btn_wrapper.pack(side=tk.RIGHT)
        # self.back_btn = ttk.Button(self.frames_inside[self.SLOT], command=self.get_out_of_rtable, text="Vissza")
        # self.back_btn.pack(side=tk.RIGHT, expand=1, fill=tk.Y)

        # name, radius, partition, skip
        # self.name_text = ttk.Label(self.frames_inside[0], text="Név:", width=12)
        # self.name_text.pack(side=tk.LEFT)
        self.delete_btn_wrapper = ttk.Frame(self.frames_inside[0], width=75, height=self.HH)
        self.delete_btn_wrapper.pack_propagate(0)
        self.delete_btn = ttk.Button(self.delete_btn_wrapper, command=self.delete_rtable, text="Törlés",
                                     image=icons.TRASH)
        self.delete_btn.pack(expand=1, fill=tk.BOTH)
        self.delete_btn_wrapper.pack(side=tk.LEFT)
        self.name_setter = ValueSetterButton(self.frames_inside[0], self.set_name, num_or_text="text", text="Szöveg",
                                             width=self.NAMESETTERBTNWIDTH, height=self.HH, self_side=tk.LEFT)

        self.radius_text = ttk.Label(self.frames_inside[1], text="Sugár:", width=12)
        self.radius_text.pack(side=tk.LEFT)
        self.radius_setter = ValueSetterButton(self.frames_inside[1], self.set_radius, num_or_text="num", text="Szöveg",
                                               width=self.SETTERBTN_WIDTH, height=self.HH, self_side=tk.LEFT,
                                               floating_digits=self.floating_digits)

        self.partition_text = ttk.Label(self.frames_inside[2], text="Osztás:", width=12)
        self.partition_text.pack(side=tk.LEFT)
        self.partition_setter = ValueSetterButton(self.frames_inside[2], self.set_partition,
                                                  num_or_text="num", text="Szöveg",
                                                  width=self.SETTERBTN_WIDTH, height=self.HH, self_side=tk.LEFT,
                                                  floating_digits=0)

        self.skip_text = ttk.Label(self.frames_inside[3], text="Kihagy:", width=12)
        self.skip_text.pack(side=tk.LEFT)
        self.skip_setter = ValueSetterButton(self.frames_inside[3], self.set_skip, num_or_text="num", text="Szöveg",
                                             width=self.SETTERBTN_WIDTH, height=self.HH, self_side=tk.LEFT,
                                             floating_digits=0)

        self.jump_text = ttk.Label(self.frames_inside[4], text="Ugrás ide:", width=12)
        self.jump_text.pack(side=tk.LEFT)
        self.set_jump = ValueSetterButton(self.frames_inside[self.SLOT - 1], self.set_jump, num_or_text="num", text="0",
                                          width=self.SETTERBTN_WIDTH, height=self.HH, self_side=tk.LEFT,
                                          floating_digits=0)

        self.circ_btn_wrapper = ttk.Frame(self.frame_inside, width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.circ_btn_wrapper.pack_propagate(0)
        self.circ_btn = ttk.Button(self.circ_btn_wrapper, command=self.go_around, text="Kör", style='RPIC.TButton')
        self.circ_btn.pack(expand=1, fill=tk.BOTH)
        self.circ_btn_wrapper.place(x=self.RED_BTNS_X, y=self.HH * 1.5, anchor=tk.CENTER)

        self.next_btn_wrapper = ttk.Frame(self.frame_inside, width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.next_btn_wrapper.pack_propagate(0)
        self.next_btn = ttk.Button(self.next_btn_wrapper, command=self.next, text="Következő", style='RPIC.TButton')
        self.next_btn.pack(expand=1, fill=tk.BOTH)
        self.next_btn_wrapper.place(x=self.RED_BTNS_X, y=self.HH * 3, anchor=tk.CENTER)

        self.jump_btn_wrapper = ttk.Frame(self.frame_inside, width=self.BOTTOM_BTN_WITH, height=self.HH)
        self.jump_btn_wrapper.pack_propagate(0)
        self.jump_btn = ttk.Button(self.jump_btn_wrapper, command=self.jump, text="Ugrás", style='RPIC.TButton')
        self.jump_btn.pack(expand=1, fill=tk.BOTH)
        self.jump_btn_wrapper.place(x=self.RED_BTNS_X, y=self.HH * 4.5, anchor=tk.CENTER)

        # na most ugrik a majom a vizes farkának örülve
        self.refresh_data()
        self.show_current_state()

    @staticmethod
    def frame_show(what, hiding_frame):
        what.lift(hiding_frame)

    @staticmethod
    def frame_hide(what, hiding_frame):
        what.lower(hiding_frame)

    def go_around(self):
        self.command(float(self.in_this_rtable.get_radius()), 0, 0, 0, 'a')

    def get_current_position(self):
        return self.current_position

    def validity(self):
        pass

    def next(self):
        p = int(float(self.in_this_rtable.get_partition()))
        r = float(self.in_this_rtable.get_radius())
        s = int(float(self.in_this_rtable.get_skip()))
        n = self.current_position + 1
        if self.are_these_numbers_valid(r, p, s, 0):
            # print("ASDFASDFASDFASDFASDFASDFASDFSAFSADFSADFSAFSAFSDAFSDAF")
            if s > 1:
                if n % s == 0:
                    # print(n, s, n % s)
                    n += 1
            if n > p:
                n = 0
            returning = self.command(r, p, s, n, 'n')  # returns a bool , and the x, y coordinates if True
            if returning[0]:
                # print("'''''''''''''''''''''''''''''''''''''''''''''''''''''''")
                self.current_position = n
                comm.go_to(returning[1], returning[2])

    def jump(self):
        p = int(float(self.in_this_rtable.get_partition()))
        r = float(self.in_this_rtable.get_radius())
        s = int(float(self.in_this_rtable.get_skip()))
        n = self.jump_to
        if self.are_these_numbers_valid(r, p, s, n):
            returning = self.command(r, p, s, n, 'j')
            if returning[0]:
                self.current_position = n
                # print(returning[1], returning[2])
                comm.go_to(returning[1], returning[2])

    def are_these_numbers_valid(self, radius_, partition_, skip_, jump_):
        # if partition 0 vagy 1, partition error
        if partition_ == 0:
            comm.popup("Az osztás nem lehet 0.")
            return False
        if partition_ == 1:
            comm.popup("Az osztás nem lehet 1.")
            return False
        # if partition nem osztható skip-pel, skip error
        if skip_ == 0:
            pass
        elif skip_ == 1:
            comm.popup("A kihagyás nem lehet 1.")
            return False
        elif not partition_ % skip_ == 0:
            comm.popup("Az osztásnak oszthatónak kell lenni a kihagyással.")
            return False
        # if jump nagyobb mint partition
        if jump_ > partition_:
            comm.popup("Az ugrás nagyobb, mint az osztás.")
            return False
        return True

    def set_jump(self, value):
        self.jump_to = int(value)

    def set_name(self, value):
        self.in_this_rtable.set_name(str(value))
        self.save_data_change()

    def set_radius(self, value):
        self.in_this_rtable.set_radius(float(value))
        self.save_data_change()

    def set_partition(self, value):
        self.in_this_rtable.set_partition(int(value))
        self.save_data_change()

    def set_skip(self, value):
        self.in_this_rtable.set_skip(int(value))
        self.save_data_change()

    def add_rtable(self):
        rtable_name = new_unique_name(self.get_rtable_names(), "Körasztal ")
        self.rtables_data.append(RotaryTable(rtable_name, 10, 0, 0))
        # going to the bottom:
        self.show_current_state()
        self.save_data_change()

    def delete_rtable(self):
        # print("delete was pressed")
        self.rtables_data.remove(self.in_this_rtable)
        self.first_rtable_to_show = 0
        self.save_data_change()
        self.get_out_of_rtable()

    def report(self):
        # print("in_this_group" + str(self.in_this_rtable))
        # print("in_edit_mode" + str(self.in_edit_mode))
        pass

    def refresh_data(self):
        self.rtables_data = mm.get_instance().rotary_tables.copy()

    def save_data_change(self):
        # print(self.rtables_data.copy())
        mm.get_instance().overwrite_rtables_data(self.rtables_data.copy())
        self.update_blacklists()

    def show_current_state(self):
        if self.in_this_rtable is None:
            for i in range(self.SLOT):
                if len(self.rtables_data) > i:
                    self.outside_buttons[i]['text'] = self.rtables_data[i + self.first_rtable_to_show].get_name()
                    # self.frames_outside[i].pack(side=ttk.TOP)
                    self.frame_show(self.frames_outside[i], self.outside_btn_hider)
                else:
                    # self.frames_outside[i].pack_forget()
                    self.frame_hide(self.frames_outside[i], self.outside_btn_hider)
            # self.frame_inside.pack_forget()
            self.frame_hide(self.frame_inside, self.full_frame_hider)
            # self.frame_outside.pack(side=ttk.TOP)
            self.frame_show(self.frame_outside, self.outside_btn_hider)
            # self.back_btn.pack_forget()   #todo kell ez?
            # self.frame_hide(self.back_btn, self.full_frame_hider)
        else:
            self.name_setter.btn['text'] = self.in_this_rtable.get_name()
            self.radius_setter.btn['text'] = self.in_this_rtable.get_radius()
            self.partition_setter.btn['text'] = self.in_this_rtable.get_partition()
            self.skip_setter.btn['text'] = self.in_this_rtable.get_skip()
            # self.frame_inside.pack(side=ttk.TOP)
            self.frame_show(self.frame_inside, self.full_frame_hider)
            # self.frame_outside.pack_forget()
            self.frame_show(self.frame_outside, self.full_frame_hider)
            # self.back_btn.pack(side=ttk.RIGHT)
        self.update_blacklists()

    def get_rtable_names(self):
        return [rtable.name for rtable in self.rtables_data]

    def update_blacklists(self):
        blacklist = []
        for rt in self.rtables_data:
            if self.in_this_rtable is not None:
                if rt.get_name() != self.in_this_rtable.get_name():
                    blacklist.append(rt.get_name())
        self.name_setter.set_blacklist(blacklist)

    def refresh_first_rtable_to_show(self):
        """
            Checks if first_point_to_show isn't too big, or too small.
            len(self.in_this_group.get_points()) - (self.SLOT - 1) could be <0 so the order is important
        """
        if self.first_rtable_to_show >= len(self.rtables_data) - self.SLOT + 1:
            self.first_rtable_to_show = len(self.rtables_data) - self.SLOT
        if self.first_rtable_to_show <= -1:
            self.first_rtable_to_show = 0

    def increase_first_rtable_to_show(self):
        self.first_rtable_to_show += 1
        self.refresh_first_rtable_to_show()

    def decrease_first_rtable_to_show(self):
        self.first_rtable_to_show -= 1
        self.refresh_first_rtable_to_show()

    def up(self):
        self.increase_first_rtable_to_show()
        self.show_current_state()

    def down(self):
        self.decrease_first_rtable_to_show()
        self.show_current_state()

    def add(self):
        self.add_rtable()
        self.save_data_change()
        for i in range(len(self.rtables_data)):
            self.increase_first_rtable_to_show()
        self.show_current_state()

    def go_into_rtable(self, i):
        for rt in self.rtables_data:
            if rt.get_name() == self.outside_buttons[i]['text']:
                self.in_this_rtable = rt
        self.current_position = 0
        self.show_current_state()

    def get_out_of_rtable(self):
        self.in_this_rtable = None
        self.show_current_state()
