import RPi.GPIO as GPIO

class Buzzer:
    """
        GPIO szinten fogja fel a joysticket, feljebbi szinteken már csak ennek a function-jait kell hívni
    """
    _pin = 100

    def __init__(self, pin):
        self._pin = pin
        GPIO.setup(self._pin, GPIO.OUT, initial=GPIO.HIGH)

    def turn_off(self):
        """Kikapcsolja."""
        GPIO.output(self._pin, GPIO.HIGH)

    def turn_on(self):
        """Bekapcsolja."""
        GPIO.output(self._pin, GPIO.LOW)
