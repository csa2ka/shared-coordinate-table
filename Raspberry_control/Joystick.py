import RPi.GPIO as GPIO


class Joystick:
    """
    GPIO szinten fogja fel a joysticket, feljebbi szinteken már csak ennek a function-jait kell hívni
    """
    _x_minus=100    #valami fura kezdeti érték ami az init-ben úgyis felül íródik
    _x_plus=100
    _y_minus = 100
    _y_plus = 100


    def __init__(self,parent,x_m,y_m,x_p,y_p):
        #print("joystickinit")
        self._x_minus = x_m
        self._x_plus =x_p
        self._y_minus = y_m
        self._y_plus = y_p
        self.parent = parent
        if self.parent.a_malnan_vagyunk:
            GPIO.setup(self._x_minus, GPIO.IN,pull_up_down=GPIO.PUD_UP)   #TODO lehet még beállítgatni, hogy ezek mik legyenek lo/hi
            GPIO.setup(self._x_plus, GPIO.IN,pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self._y_minus, GPIO.IN,pull_up_down=GPIO.PUD_UP)  # TODO lehet még beállítgatni, hogy ezek mik legyenek lo/hi
            GPIO.setup(self._y_plus, GPIO.IN,pull_up_down=GPIO.PUD_UP)

    def get_x_minus(self):
        """Leolvassa az input pin-t az irányt"""
        # print("get_x_minus", GPIO.input(self._x_minus))
        return GPIO.input(self._x_minus)

    def get_x_plus(self):
        """Leolvassa az input pin-t az irányt"""
        # print("get_x_PLUs", GPIO.input(self._x_minus))
        return GPIO.input(self._x_plus)

    def get_y_minus(self):
        """Leolvassa az input pin-t az irányt"""
        return GPIO.input(self._y_minus)

    def get_y_plus(self):
        """Leolvassa az input pin-t az irányt"""
        return GPIO.input(self._y_plus)