import RPi.GPIO as GPIO
from time import sleep
import threading
import time
from utils.balazs_funcs import one_zero_transistor


class StepperMotor:
    """
    GPIO szinten fogja fel a motort, feljebbi szinteken már csak ennek a function-jait kell hívni
    """
    """
    _____________
    | L | L | L |   fullstep
    |-----------|
    | H | L | L |   halfstep
    |-----------|
    | L | H | L |   quarterstep
    |-----------|
    | H | H | L |   eighth step
    |-----------|
    | H | H | H |   sixteenth step
    ˇˇˇˇˇˇˇˇˇˇˇˇˇ

    """

    _micro1 = None
    _micro2 = None
    _micro3 = None
    _microstep_enabled = False

    locky = threading.Lock()

    def __init__(self, parent, step, dire, enable, microstep_enabled, micro1=None, micro2=None, micro3=None):
        # print("motorinit")
        self.parent = parent
        self._step = step
        self._dire = dire
        self._base_direction = None
        self._enable = enable
        self.enable = False
        self.delay_difference = 1.5

        if microstep_enabled:
            StepperMotor._micro1 = micro1
            StepperMotor._micro2 = micro2
            StepperMotor._micro3 = micro3
            StepperMotor._microstep_enabled = microstep_enabled
            GPIO.setup(self._micro1, GPIO.OUT, initial=GPIO.LOW)
            GPIO.setup(self._micro2, GPIO.OUT, initial=GPIO.LOW)
            GPIO.setup(self._micro3, GPIO.OUT, initial=GPIO.LOW)

        GPIO.setup(self._enable, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(self._step, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self._dire, GPIO.OUT, initial=GPIO.LOW)

        self.time_array = []  # TODO remove after testing...

    def set_abs_direction(self, base_dire):
        """Az alapitányát állíthatjuk be a motornak, -1, 1"""
        self._base_direction = base_dire

    def get_abs_direction(self):
        return self._base_direction

    def set_dir(self, dire):
        """Beállítja az irányt"""
        GPIO.output(self._dire, one_zero_transistor(dire, self._base_direction))

    def set_step(self, dire):
        """Ahhoz hogy egyszerre tudjon lépni több motor ez is kell a step-en kívül"""
        GPIO.output(self._step, dire)

    def get_dir(self):
        """Beállítja az irányt"""
        return self._dire

    def step(self, delay):
        """Lép egyet, delay ödő alatt"""
        GPIO.output(self._step, GPIO.HIGH)
        # self.time_array.append(time.time())
        sleep(delay)
        # self.time_array.append(time.time())
        GPIO.output(self._step, GPIO.LOW)
        sleep(delay * self.delay_difference)
        # self.time_array.append(time.time())
        # if len(self.time_array) > 1000:
        #    self.print_time_stats(delay)
        # with StepperMotor.locky:
        #     self.parent.counter+=1

    def print_time_stats(self, delay):
        print("TIME STATISTICS")
        fs = 3
        n = int(len(self.time_array) / fs)
        # for i in range(n*fs):
        #    print(i, "checkpoint", self.time_array[i])
        print(n, "rounds, official delay:", delay)
        sum_first = 0
        sum_second = 0
        sum_last_part = 0
        szoras_first = 0
        szoras_second = 0
        szoras_last = 0
        for i in range(n - 3):
            if i > 1:
                print(self.time_array[i * fs + 3] - self.time_array[i * fs])
            sum_first += self.time_array[i * fs + 1] - self.time_array[i * fs]
            sum_second += self.time_array[i * fs + 2] - self.time_array[i * fs + 1]
            sum_last_part += self.time_array[i * fs + 3] - self.time_array[i * fs + 2]
        for i in range(n - 3):
            # if i > 1:
            #    print(self.time_array[i * fs + 3] - self.time_array[i * fs])
            szoras_first += pow(((self.time_array[i * fs + 1] - self.time_array[i * fs]) - sum_first / n), 2)

            szoras_second += pow(((self.time_array[i * fs + 2] - self.time_array[i * fs + 1]) - sum_second / n), 2)
            szoras_last += pow(((self.time_array[i * fs + 3] - self.time_array[i * fs + 2]) - sum_second / n), 2)
        print("avg first: ", sum_first / n, "avg second: ", sum_second / n, "last: ", sum_last_part / n)
        print("szorasok: ", pow(szoras_first / n, 0.5), " masodik: ", pow(szoras_second / n, 0.5), "harmadik:",
              pow(szoras_last / n, 0.5))
        print("utolso-elso:", self.time_array[-1] - self.time_array[0])
        x = (sum_second / n - sum_first / n)/(self.delay_difference - 1)
        y = sum_first / n - x
        print("X, y: ", x, y)
        self.time_array = []

    def get_enable(self):
        return self.enable

    def set_microstep(self, m):
        it_was_turned_off = True
        if not self.enable:
            self.turn_on()
            it_was_turned_off = True
        if m == 1:
            GPIO.output(StepperMotor._micro1, GPIO.LOW)
            GPIO.output(StepperMotor._micro2, GPIO.LOW)
            GPIO.output(StepperMotor._micro3, GPIO.HIGH)
        if m == 2:
            GPIO.output(StepperMotor._micro1, GPIO.LOW)
            GPIO.output(StepperMotor._micro2, GPIO.HIGH)
            GPIO.output(StepperMotor._micro3, GPIO.LOW)
        if m == 4:
            GPIO.output(StepperMotor._micro1, GPIO.LOW)
            GPIO.output(StepperMotor._micro2, GPIO.HIGH)
            GPIO.output(StepperMotor._micro3, GPIO.HIGH)
        if m == 8:
            GPIO.output(StepperMotor._micro1, GPIO.HIGH)
            GPIO.output(StepperMotor._micro2, GPIO.LOW)
            GPIO.output(StepperMotor._micro3, GPIO.HIGH)
        if m == 16:
            GPIO.output(StepperMotor._micro1, GPIO.HIGH)
            GPIO.output(StepperMotor._micro2, GPIO.HIGH)
            GPIO.output(StepperMotor._micro3, GPIO.LOW)
        if it_was_turned_off:
            self.turn_off()

    def turn_off(self):
        """Kikapcsolja a motort, ezután a pinjei, csak akkor használhatóak, ha be kapcsoljuk.
        Mi esetünkben össze van kötve a másik motor enable pin-jével, szóval ez kikapcsolja mindkét motort.
        Erre oda kell figyelni."""
        GPIO.output(self._enable, GPIO.HIGH)
        self.enable = False

    def turn_on(self):
        """Bekapcsolja a motor pinjeit. Mi esetünkben mindkét motort bekapcsolja!!!"""
        GPIO.output(self._enable, GPIO.LOW)
        self.enable = True
