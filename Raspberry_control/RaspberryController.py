import RPi.GPIO as GPIO
from time import sleep
import threading
import math
from Raspberry_control.Map import Map
from Raspberry_control.Buzzer import Buzzer
from Raspberry_control.Buttonlike import Buttonlike
from Raspberry_control.Joystick import Joystick
from Raspberry_control.StepperMotor import StepperMotor
from Raspberry_control.CleverButton import CleverButton
from Communicator import Communicator as comm


class RaspberryController:
    a_malnan_vagyunk = True

    # counter=0

    """
    Két motor mozgatásáért felelős class
    """
    # Motorok:
    _X_DIR_PIN = 22
    _X_STEP_PIN = 24
    _Y_DIR_PIN = 27
    _Y_STEP_PIN = 23
    _ENABLE_PIN = 17

    _MICRO1_PIN = 14
    _MICRO2_PIN = 15
    _MICRO3_PIN = 18

    _BUZZER_PIN = 4

    _SWITCH_X_MIN_PIN = 16  # végálláskapcsolók pinjei
    _SWITCH_X_PLUS_PIN = 13
    _SWITCH_Y_MIN_PIN = 12
    _SWITCH_Y_PLUS_PIN = 6

    _J_X_MIN_PIN = 21  # joystick pinjei
    _J_X_PLUS_PIN = 26
    _J_Y_MIN_PIN = 20
    _J_Y_PLUS_PIN = 19

    Y_NULL_BUTTON_PIN = 5  

    X_NULL_BUTTON_PIN = 8   

    X_HALVER_BUTTON_PIN = 2   
    Y_HALVER_BUTTON_PIN = 3

    JOYSTICK_SPEED1 = 0.2

    JOYSTICK_SPEED_CHANGE1 = 8
    JOYSTICK_SPEED2 = 0.04

    JOYSTICK_SPEED_CHANGE2 = 48
    JOYSTICK_SPEED3 = 0.008

    JOYSTICK_SPEED_CHANGE3 = 144
    JOYSTICK_SPEED4 = 0.001

    # counter = 0

    base_tick = 0.1
    joystick_tick = 0.3

    # Beállítások:                              (hosszú nevük van, mert ritkán kell leírni őket)
    """Vezérlés, point-to-point control. 3 lesz: AXIAL, DEG45 és LINEAR. Szakasz, Pont és Pálya"""
    LINEAR = "LINEAR"
    DEG45 = "DEG45"
    AXIAL = "AXIAL"
    _p2p_control = AXIAL
    """Egy bool, hogy van-e vagy nincs végálláskapcsoló."""
    _overtravel_switch = True  # lehetne beszédesebb az elnevezés
    """Microstep: Hű, ne ez vacak egy kicsit, mert pinekkel lehetne állítgatni, de valszeg be lesz állítva
    hardweresen egy félére és lehet kérni, hogy szoftveresen azt hagyjuk figyelmen kívül.
        Hardveres: Full step --> 16-os     Egyre halkabb, kevesebb rezgés a motortól
        Softveres: Ha már a hardweresen nagy (4,k,16) microstep van akkor csoportosan léptetve alacsony microsteppet
                lehet csinálni, negítívuma az hogy lassabb (van minimimum delay: 1 micro sec)
    A softveres microstep  adott mozgatási fajtától is függhet."""
    _microstep_setter_enabled = True

    _microstep_hardware = 1
    _microstep_software = 1
    """step/mm"""
    _x_step_mm = 100
    _y_step_mm = 100
    """léptetési idő, min 0.000001"""

    delay_difference = 1.5
    _speed = 0.001  # 0.001  egészen optimális fullsteppes hardveres microstep esetén
    _delay = 1 / (_x_step_mm * _speed)
    micro_delay = _delay / _microstep_software

    """Irányítási konstansok:"""
    # _J_On = False
    # _SETUP = False
    # _TARGET_MOVE = False
    _JOYSTICK_MODE = "JOYSTICK"
    _SETUP_MODE = "SETUP"
    _TARGET_MODE = "TARGET"
    _ROTATION_MODE = "ROTATION"
    _IDLE_MODE = "IDLE"
    _END_MODE = "END"
    _MODE = "STARTING"
    x_t = 0
    y_t = 0
    radius = 0
    _setup_mode = "mid"

    S_X_MIN = "s_x_min"
    S_Y_MIN = "s_y_min"
    S_X_PLUS = "s_x_plus"
    S_Y_PLUS = "s_y_plus"

    _MANUAL = False

    manual_w = 0  # to store map width in manual setup
    manual_h = 0  # to store map height in manual setup

    coordinate_lock = threading.Lock()
    mode_lock = threading.Lock()

    """Motor warning popup needed all of these, sorry for bad code, this is what I came up with"""
    roty = False  # actually this is seems useless
    previous_target_x = None
    previous_target_y = None
    should_end_motormoving_popup = False
    motor_popup_is_allowed = True

    """Setting wether the motors are moving the table or the a end-effector, usually driller"""
    _x_motor_base_direction = 1
    _y_motor_base_direction = 1

    def __init__(self):
        self.map = Map(0, 0, self)  # TODO ez hogy működik??
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)

        self.buzzer = Buzzer(self._BUZZER_PIN)
        self.x_motor = StepperMotor(self, self._X_STEP_PIN, self._X_DIR_PIN, self._ENABLE_PIN,
                                    self._microstep_setter_enabled, self._MICRO1_PIN, self._MICRO2_PIN,
                                    self._MICRO3_PIN)
        self.y_motor = StepperMotor(self, self._Y_STEP_PIN, self._Y_DIR_PIN, self._ENABLE_PIN,
                                    self._microstep_setter_enabled, self._MICRO1_PIN, self._MICRO2_PIN,
                                    self._MICRO3_PIN)

        self._s_x_min = Buttonlike(self, self._SWITCH_X_MIN_PIN, 1)  # GPIO.PUD_UP
        self._s_y_min = Buttonlike(self, self._SWITCH_Y_MIN_PIN, 1)
        self._s_x_plus = Buttonlike(self, self._SWITCH_X_PLUS_PIN, 1)
        self._s_y_plus = Buttonlike(self, self._SWITCH_Y_PLUS_PIN, 1)

        self._joystick = Joystick(self, self._J_X_MIN_PIN, self._J_Y_MIN_PIN, self._J_X_PLUS_PIN,
                                  self._J_Y_PLUS_PIN)

        # self.stop_button = Buttonlike(self, self.X_NULL_BUTTON_PIN_NUMBER, 1)
        # self.null_button = Buttonlike(self, self.Y_NULL_BUTTON_PIN_NUMBER, 1)

        # Clever buttons
        
        self.x_null_btn = CleverButton(self, self.X_NULL_BUTTON_PIN, 1)
        self.y_null_btn = CleverButton(self, self.Y_NULL_BUTTON_PIN, 1)
        self.x_null_btn.register_long_pressed_func(self.reset_x)
        self.x_null_btn.register_short_pressed_func(self.reset_x_relative)
        self.y_null_btn.register_long_pressed_func(self.reset_y)   # self.reset_y
        self.y_null_btn.register_short_pressed_func(self.reset_y_relative)
        
        
        self.x_halver_btn = CleverButton(self, self.X_HALVER_BUTTON_PIN, 1)
        self.y_halver_btn = CleverButton(self, self.Y_HALVER_BUTTON_PIN, 1)
        self.x_halver_btn.register_short_pressed_func(self.go_to_x_half)
        self.x_halver_btn.register_long_pressed_func(self.go_to_x_half)
        self.y_halver_btn.register_short_pressed_func(self.go_to_y_half)
        self.y_halver_btn.register_long_pressed_func(self.go_to_y_half)
        
        self.tickables = [ self.x_null_btn, self.y_null_btn, self.x_halver_btn, self.y_halver_btn]

        """ TODO check tickables order
            
        """
    def go_to_x_half(self):
        print("GOTO_X_HALF")
        curx = self.thread_safe_coordinate_handler(self.map.get_x_mm, 'r')
        cury = self.thread_safe_coordinate_handler(self.map.get_y_mm, 'r')
        self.thread_safe_coordinate_handler(self.set_xy_targets, 'w', curx / 2, cury)
        print(curx, cury, curx/2, cury/2)
        self.switch_to_target_move()
        self.handling_motor_moving_popup(curx / 2, cury)

    def go_to_y_half(self):
        print("GOTO_Y_HALF")
        curx = self.thread_safe_coordinate_handler(self.map.get_x_mm, 'r')
        cury = self.thread_safe_coordinate_handler(self.map.get_y_mm, 'r')
        print(curx, cury, curx/2, cury/2)
        self.thread_safe_coordinate_handler(self.set_xy_targets, 'w', curx, cury/2)
        self.switch_to_target_move()
        self.handling_motor_moving_popup(curx, cury/2)
    
    def set_setup_mode(self, s):
        self._setup_mode = s

    def set_microstep_setter_enable(self, boool):
        self._microstep_setter_enabled = boool

    def set_control_mode(self, mode_name):
        if mode_name == self.AXIAL or (mode_name == self.DEG45 or mode_name == self.LINEAR):
            self._p2p_control = mode_name
        else:  # hmm ide egy exception kéne, vagy valami olyasmi...
            pass

    def set_overtravel_switch(self, ov_sw):
        self._overtravel_switch = ov_sw
        self.thread_safe_coordinate_handler(self.map.set_overtravel_switch, 'w', ov_sw)

    def set_microstep_hardware(self, mi_hw):
        if self._microstep_setter_enabled:
            self.x_motor.set_microstep(mi_hw)
            self.y_motor.set_microstep(mi_hw)
        self._microstep_hardware = mi_hw
        # setting microstep softver aswell since we don't need it anymore.
        # self.set_microstep_software(mi_hw)

    def set_microstep_software(self, mi_sw):
        self._microstep_software = mi_sw
        self._delay = 1 / (self._speed * self._x_step_mm)
        self.micro_delay = self._delay / self._microstep_software

    def get_microstep_hardware(self):
        return self._microstep_hardware

    def get_microstep_software(self):
        return self._microstep_software

    def set_x_motor_base_direction(self, value):
        if value is not None:
            self._x_motor_base_direction = value
            self.x_motor.set_abs_direction(value)

    def set_y_motor_base_direction(self, value):
        if value is not None:
            self.y_motor.set_abs_direction(value)
            self._y_motor_base_direction = value

    def set_x_step_mm(self, st_mm):
        self._x_step_mm = st_mm
        self._delay = 1 / (self._x_step_mm * self._speed)

    def set_y_step_mm(self, st_mm):
        self._y_step_mm = st_mm
        self._delay = 1 / (self._y_step_mm * self._speed)

    def get_x_step_mm(self):
        return self._x_step_mm

    def get_y_step_mm(self):
        return self._y_step_mm

    def get_x_step_mm(self):
        return self._x_step_mm

    def get_y_step_mm(self):
        return self._y_step_mm

    def set_delay(self, d):
        self._speed = d
        self._delay = 1 / (self._x_step_mm * self._speed)
        self.micro_delay = self._delay / self._microstep_software

    def turn_buzzer_on(self):
        self.buzzer.turn_on()

    def turn_buzzer_off(self):
        self.buzzer.turn_off()

    def turn_on_both(self):
        self.x_motor.turn_on()
        self.y_motor.turn_on()

    def turn_off_both(self):
        self.x_motor.turn_off()
        self.y_motor.turn_off()

    def set_xy_targets(self, x, y):
        self.x_t = x
        self.y_t = y

    def set_previous_xy(self, x, y):
        self.previous_target_x = x
        self.previous_target_y = y

    def get_x_target(self):
        return self.x_t

    def get_y_target(self):
        return self.y_t

    def go_idle(self):
        self.switch_to_idle()

    def fto(self):  # idk what this
        self.turn_off_both()
        self._MODE = self._IDLE_MODE

    def save_map(self):
        self.map.save()
        comm.refresh_gui_in_settings_3()  # TODO this should be on the GUI thread isn't it

    def reset_x_relative(self):
        self.map.set_current_x_to_relative_zero()

    def reset_y_relative(self):
        self.map.set_current_y_to_relative_zero()

    def reset_x(self):
        self.thread_safe_coordinate_handler(self.map.set_abs_x_middle, 'r')
        self.reset_x_relative()
        self.save_map()

    def reset_y(self):
        self.thread_safe_coordinate_handler(self.map.set_abs_y_middle, 'r')
        self.reset_y_relative()
        self.save_map()

    def round_to_mstep(self, v, step_mm):
        """ mm-ert megfelelő microstepre kerekítünk """
        return int(round(step_mm * self._microstep_software * v))

    def valid_move(self, x, y):
        """ellenőrizni hogy a határokon belül maradunk e
        Most még nincs használatban!
        """
        demilitarized_zone = 10
        dm = demilitarized_zone
        if self.x_motor.get_enable():
            if self.is_this_mode_on(self._JOYSTICK_MODE):
                # print("joystickmove bro, it's ok")
                return True
            until_x_min = self.thread_safe_coordinate_handler(self.map.get_until_x_min_border, 'r')
            until_x_plus = self.thread_safe_coordinate_handler(self.map.get_until_x_plus_border, 'r')
            until_y_min = self.thread_safe_coordinate_handler(self.map.get_until_y_min_border, 'r')
            until_y_plus = self.thread_safe_coordinate_handler(self.map.get_until_y_plus_border, 'r')
            cur_x = self.thread_safe_coordinate_handler(self.map.get_x, 'r')
            cur_y = self.thread_safe_coordinate_handler(self.map.get_y, 'r')
            if self.map.get_origo_mode() == "setup":
                return True
            else:
                if cur_x + x > until_x_plus or cur_x + x < -1*until_x_min:
                    comm.end_motor_moving_popup()
                    comm.popup("Túl mennél a határon!")
                    # print("VALID? NOPE! DIDEIEIEIDIDIEI")
                    self.motor_popup_is_allowed = False
                    return False
                if cur_y + y > until_y_plus or cur_y + y < -1*until_y_min:
                    comm.end_motor_moving_popup()
                    comm.popup("Túl mennél a határon!")
                    # print("VALID? NOPE! DIDEIEIEIDIDIEI")
                    self.motor_popup_is_allowed = False
                    return False
        else:
            return False # the motor is not turned on
        return True

    def decide_direction(self, over_trav_swit):
        if over_trav_swit == self.S_X_MIN:
            if self._x_motor_base_direction == -1:
                return self._s_x_plus, "X_P"
            else:
                return self._s_x_min, "X_m"
        if over_trav_swit == self.S_X_PLUS:
            if self._x_motor_base_direction == -1:
                return self._s_x_min, "X_m"
            else:
                return self._s_x_plus, "X_P"
        if over_trav_swit == self.S_Y_MIN:
            if self._y_motor_base_direction == -1:
                return self._s_y_plus, "y_P"
            else:
                return self._s_y_min, "ym"
        if over_trav_swit == self.S_Y_PLUS:
            if self._y_motor_base_direction == -1:
                return self._s_y_min, "ym"
            else:
                return self._s_y_plus, "y_P"

    def overtravel_switch_got_pressed(self, over_travel_switch_name):

        return_stuff = self.decide_direction(over_travel_switch_name)
        over_travel_switch = return_stuff[0]
        # print(return_stuff[1])
        if self._overtravel_switch:
            if over_travel_switch.get_value() > 0.7:
                return True
        if not self.x_motor.get_enable():
            return True
        # print("False!!! (in overtravel_switch got presseds)")
        return False

    def a_combo_step(self, x_dir, y_dir):
        counter = int(round(self._microstep_hardware / self._microstep_software))
        delta_x = int(round((x_dir - 0.5) * 2))  # 1 -> 1  ,    0 -> -1
        delta_y = int(round(- (y_dir - 0.5) * 2))  # 1 -> -1  ,    0 -> 1
        if x_dir == 1:  # jobb
            if self.overtravel_switch_got_pressed(self.S_X_PLUS):
                return False
        if x_dir == 0:  # bal
            if self.overtravel_switch_got_pressed(self.S_X_MIN):
                return False
        if y_dir == 1:  # le
            if self.overtravel_switch_got_pressed(self.S_Y_MIN):
                return False
        if y_dir == 0:  # fel
            if self.overtravel_switch_got_pressed(self.S_Y_PLUS):
                return False
        for j in range(counter):
            self.x_motor.set_step(1)
            self.y_motor.set_step(1)
            sleep(self.micro_delay)
            self.x_motor.set_step(0)
            self.y_motor.set_step(0)
            sleep(self.micro_delay * self.delay_difference)
            self.thread_safe_coordinate_handler(self.map.x_m_add, 'w', delta_x)
            self.thread_safe_coordinate_handler(self.map.y_m_add, 'w', delta_y)
        self.thread_safe_coordinate_handler(self.map.x_add, 'w', delta_x)
        self.thread_safe_coordinate_handler(self.map.y_add, 'w', delta_y)

    def a_step(self, x_or_y, dire):
        counter = int(round(self._microstep_hardware / self._microstep_software))  # 10e-5
        if x_or_y == "x":
            delta_x = int(round((dire - 0.5) * 2))  # 1 -> 1  ,    0 -> -1 #10e-5
            if dire == 1:  # jobb
                if self.overtravel_switch_got_pressed(self.S_X_PLUS):
                    # print("step NOPE: x_plus")
                    return False
            if dire == 0:  # bal
                if self.overtravel_switch_got_pressed(self.S_X_MIN):
                    # print("step NOPE: x_min")
                    return False
            for j in range(counter):
                self.x_motor.step(self.micro_delay)
                self.thread_safe_coordinate_handler(self.map.x_m_add, 'w', delta_x)
            self.thread_safe_coordinate_handler(self.map.x_add, 'w', delta_x)
        if x_or_y == "y":
            delta_y = int(round(- (dire - 0.5) * 2))  # 1 -> -1  ,    0 -> 1
            if dire == 1:  # le
                if self.overtravel_switch_got_pressed(self.S_Y_MIN):
                    # print("step NOPE: y_min ")
                    return False
            if dire == 0:  # fel
                if self.overtravel_switch_got_pressed(self.S_Y_PLUS):
                    # print("step NOPE: y_plus")
                    return False
            for j in range(counter):
                self.y_motor.step(self.micro_delay)
                self.thread_safe_coordinate_handler(self.map.y_m_add, 'w', delta_y)
            self.thread_safe_coordinate_handler(self.map.y_add, 'w', delta_y)

    def stop(self):
        if self._MODE == self._IDLE_MODE:
            return True
        return False

    def axial_move(self, x_soft, y_soft, x_dir, y_dir):
        for i in range(x_soft):
            if self.stopped_by_hand():
                # print("stopped_by_hand")
                return 0
            self.a_step("x", x_dir)
        for i in range(y_soft):
            if self.stopped_by_hand():
                return 0
            self.a_step("y", y_dir)

    def deg45_move(self, x_soft, y_soft, x_dir, y_dir):
        x_soft_2 = x_soft
        y_soft_2 = y_soft
        while x_soft_2 > 0 and y_soft_2 > 0:
            if self.stopped_by_hand():
                return 0
            self.a_combo_step(x_dir, y_dir)
            x_soft_2 -= 1
            y_soft_2 -= 1
        self.axial_move(x_soft_2, y_soft_2, x_dir, y_dir)

    def linear_move(self, x_soft, y_soft, x_dir, y_dir):
        if x_soft == 0 or y_soft == 0:
            self.axial_move(x_soft, y_soft, x_dir, y_dir)
        else:
            q = y_soft / x_soft
            sum_step = y_soft + x_soft
            y_soft_2 = 0
            x_soft_2 = 0
            self.a_step("x", x_dir)
            x_soft_2 += 1
            while sum_step > 1:
                if self.stopped_by_hand():
                    return 0
                if abs(y_soft_2 / (x_soft_2 + 1) - q) < abs((y_soft_2 + 1) / x_soft_2 - q) and abs(
                        y_soft_2 / (x_soft_2 + 1) - q) < abs((y_soft_2 + 1) / (x_soft_2 + 1) - q):
                    self.a_step("x", x_dir)
                    x_soft_2 += 1
                    sum_step -= 1
                else:
                    if abs((y_soft_2 + 1) / x_soft_2 - q) < abs((y_soft_2 + 1) / (x_soft_2 + 1) - q):
                        self.a_step("y", y_dir)
                        y_soft_2 += 1
                        sum_step -= 1
                    else:
                        self.a_combo_step(x_dir, y_dir)
                        y_soft_2 += 1
                        x_soft_2 += 1
                        sum_step -= 2

    def joystick_mode(self, speed, counter):
        """ Joystick: bizonyos időközönkéni nézi, hogy egyes joystick gombok meg vannak e nyomva. Ha igen akkor speed-nyit
        fog mozogni. Ezenkívül ha zyinórba több vizsgálatnál is le volt nyomva valami, akkor a speed nő. """
        dx = 0
        dy = 0
        if self._joystick.get_x_minus() < 0.3:
            dx += speed
        if self._joystick.get_x_plus() < 0.3:
            dx -= speed
        if self._joystick.get_y_minus() < 0.3:
            dy -= speed
        if self._joystick.get_y_plus() < 0.3:
            dy += speed

        # print("joystick mode: dx", dx, "dy", dy)
        if not dx == 0 or not dy == 0:
            counter += 1
            self.move(dx, dy)
        else:
            counter = 0
        return counter

    # def any_overtravel_switch_got_pressed(self):
    #     if self.overtravel_switch_got_pressed(self._s_y_min) \
    #            or (self.overtravel_switch_got_pressed(self._s_y_plus)) \
    #            or (self.overtravel_switch_got_pressed(self._s_x_min)) \
    #            or (self.overtravel_switch_got_pressed(self._s_x_plus)):
    #        return True

    def move_til_end(self, s):
        allowed = True
        counter = 0
        while allowed and counter < 10000 * self._microstep_software:
            if self.stopped_by_hand():
                self.should_end_motormoving_popup = True
                return 0
            if s == "y_min":
                self.move(0, -1)
                if self.overtravel_switch_got_pressed(self.S_Y_MIN):
                    allowed = False
            if s == "y_plus":
                self.move(0, 1)
                if self.overtravel_switch_got_pressed(self.S_Y_PLUS):
                    allowed = False
            if s == "x_min":
                self.move(-1, 0)
                if self.overtravel_switch_got_pressed(self.S_X_MIN):
                    allowed = False
            if s == "x_plus":
                self.move(1, 0)
                if self.overtravel_switch_got_pressed(self.S_X_PLUS):
                    allowed = False
            counter += 1
        return counter

    def setup_move(self):
        if not self._overtravel_switch:
            comm.popup("Nincs végállás kapcsoló!")
            return 0
        self.map.set_origo_mode("setup")
        self.move_til_end("x_plus")
        self.move_til_end("y_plus")
        l = self.move_til_end("x_min")
        h = self.move_til_end("y_min")
        if self._setup_mode == "mid":
            self.move(round(l / 2), round(h / 2))
            self.thread_safe_coordinate_handler(self.map.set_until_x_min_border, 'w', round(l / 2))
            self.thread_safe_coordinate_handler(self.map.set_until_x_plus_border, 'w', round(l / 2))
            self.thread_safe_coordinate_handler(self.map.set_until_y_min_border, 'w', round(h / 2))
            self.thread_safe_coordinate_handler(self.map.set_until_y_plus_border, 'w', round(h / 2))
        if not self._setup_mode == "mid":
            self.move(50, 50)
            self.thread_safe_coordinate_handler(self.map.set_until_x_min_border, 'w', 0)
            self.thread_safe_coordinate_handler(self.map.set_until_x_plus_border, 'w', l)
            self.thread_safe_coordinate_handler(self.map.set_until_y_min_border, 'w', 0)
            self.thread_safe_coordinate_handler(self.map.set_until_y_plus_border, 'w', h)
        self.map.set_origo_mode(self._setup_mode)
        self.thread_safe_coordinate_handler(self.map.set_l_h, 'w', l, h)

        self.thread_safe_coordinate_handler(self.map.set_coordinates_mm, 'w', 0, 0)
        self.thread_safe_coordinate_handler(self.map.set_coordinates, 'w', 0, 0)
        self.save_map()

    def manual_setup(self, phase):
        safety_zone_width = 30  # for testing this should be low
        if phase == "reset":
            self.manual_h = 0
            self.manual_w = 0
        if phase == "x_min":
            self.thread_safe_coordinate_handler(self.map.set_x, 'w', 0)
        if phase == "y_min":
            self.thread_safe_coordinate_handler(self.map.set_y, 'w', 0)
        if phase == "x_plus":
            self.manual_w = self.thread_safe_coordinate_handler(self.map.get_x, 'r') - safety_zone_width
        if phase == "y_plus":
            self.manual_h = self.thread_safe_coordinate_handler(self.map.get_y, 'r') - safety_zone_width
        if phase == "to_mid":
            self.move(-round(self.manual_w / 2), -round(self.manual_h / 2))
            self.manual_w = abs(self.manual_w)
            self.manual_h = abs(self.manual_h)
            self.thread_safe_coordinate_handler(self.map.set_until_x_min_border, 'w', round(self.manual_w / 2))
            self.thread_safe_coordinate_handler(self.map.set_until_x_plus_border, 'w', round(self.manual_w / 2))
            self.thread_safe_coordinate_handler(self.map.set_until_y_min_border, 'w', round(self.manual_h / 2))
            self.thread_safe_coordinate_handler(self.map.set_until_y_plus_border, 'w', round(self.manual_h / 2))
            self.map.set_origo_mode(self._setup_mode)
            self.thread_safe_coordinate_handler(self.map.set_l_h, 'w', self.manual_w, self.manual_h)
            self.thread_safe_coordinate_handler(self.map.set_coordinates_mm, 'w', 0, 0)
            self.thread_safe_coordinate_handler(self.map.set_coordinates, 'w', 0, 0)
            self.save_map()

    def switch_to_setup(self):
        self.thread_safe_mode_handler(self._SETUP_MODE, 'w')

    def switch_to_target_move(self):
        self.thread_safe_mode_handler(self._TARGET_MODE, 'w')

    def switch_to_rotation_move(self):
        self.thread_safe_mode_handler(self._ROTATION_MODE, 'w')

    def switch_to_idle(self):
        self.thread_safe_mode_handler(self._IDLE_MODE, 'w')

    def switch_to_joystick(self):
        self.thread_safe_mode_handler(self._JOYSTICK_MODE, 'w')
        self.should_end_motormoving_popup = True
        comm.end_motor_moving_popup()

    def is_this_mode_on(self, mode_name):
        return self.thread_safe_mode_handler(mode_name, 'r')

    def thread_safe_coordinate_handler(self, function, read_or_write='r', first_param=0, second_param=-10042030.353):
        with self.coordinate_lock:  # na ezzel elvileg most szálbiztosítottuk a koordináta kezelést
            if read_or_write == 'w':
                if second_param == -10042030.353:
                    function(first_param)
                else:
                    function(first_param, second_param)
            if read_or_write == 'r':
                return function()
            return 0

    def thread_safe_mode_handler(self, mode_name, read_or_write='r'):
        with self.mode_lock:  # na ezzel elvileg most szálbiztosítottuk a koordináta kezelést
            if read_or_write == 'w':
                if mode_name == self._IDLE_MODE:
                    self.turn_off_both()
                else:
                    self.turn_on_both()
                self._MODE = mode_name
                return False
            elif read_or_write == 'r':
                return self._MODE == mode_name

    def on_tick(self):
        counter = 0
        base_tick = 0.1
        joystick_tick = 0.3
        tick = base_tick
        while True:
            if self.is_this_mode_on(self._JOYSTICK_MODE):
                counter = self.joystick_mode(1, counter)
                if counter == 0:
                    joystick_tick = 0.001
                if counter == 1:
                    joystick_tick = self.JOYSTICK_SPEED1
                if counter == self.JOYSTICK_SPEED_CHANGE1:
                    joystick_tick = self.JOYSTICK_SPEED2
                if counter == self.JOYSTICK_SPEED_CHANGE2:
                    joystick_tick = self.JOYSTICK_SPEED3
                if counter == self.JOYSTICK_SPEED_CHANGE3:
                    joystick_tick = self.JOYSTICK_SPEED4
                tick = joystick_tick
            elif self.is_this_mode_on(self._SETUP_MODE):
                self.should_end_motormoving_popup = False
                self.setup_move()
                self.switch_to_joystick()
            elif self.is_this_mode_on(self._TARGET_MODE):
                self.should_end_motormoving_popup = False
                x_t = self.thread_safe_coordinate_handler(self.get_x_target, 'r')
                y_t = self.thread_safe_coordinate_handler(self.get_y_target, 'r')
                self.move_to_mm_version(x_t, y_t)
                self.switch_to_joystick()
            elif self.is_this_mode_on(self._ROTATION_MODE):
                self.should_end_motormoving_popup = False
                self.go_around()
                self.switch_to_joystick()
            for t in self.tickables:
                t.tick(tick)
            sleep(tick)
            tick = base_tick

    def a_bit_of_a_projection(self, one_or_zero, direction):
        """So we have a 0, or zero stuff, and we have a 1 or -1 stuff, and we want to get a 0,1 based switching the 1
        and the 0 if -1 not switching if 1, I wanna use math instead of if so:
                0 1= 0
                1 1= 1
                0 -1= 1
                1 -1= 0"""
        # print(one_or_zero, direction, int((2 * (one_or_zero - 0.5) * direction) / 2 + 0.5))
        return int((2 * (one_or_zero - 0.5) * direction) / 2 + 0.5)

    def move(self, x_soft, y_soft):  # d_x, d_y-nek step mértékegységűek, softveres microstep)
        if self.valid_move(x_soft, y_soft):  # TODO van e hely erre a mozgásra
            """Irány"""
            # print("valid")
            # print("Got through valid_move()")
            x_dir = 0
            y_dir = 0
            if x_soft > 0:
                x_dir = 1
            if x_soft < 0:
                x_dir = 0
            if y_soft > 0:
                y_dir = 0
            if y_soft < 0:
                y_dir = 1

            x_soft = int(abs(x_soft))
            y_soft = int(abs(y_soft))

            self.x_motor.set_dir(x_dir)
            self.y_motor.set_dir(y_dir)

            if self._p2p_control == self.AXIAL:
                self.axial_move(x_soft, y_soft, x_dir, y_dir)

            if self._p2p_control == self.DEG45:  # lehet ez egy kicsit csúnya
                self.deg45_move(x_soft, y_soft, x_dir, y_dir)

            if self._p2p_control == self.LINEAR:
                self.linear_move(x_soft, y_soft, x_dir, y_dir)

            if not self.thread_safe_mode_handler(self._ROTATION_MODE, 'r'):
                if not self.thread_safe_mode_handler(self._JOYSTICK_MODE, 'r'):
                    if not self.thread_safe_mode_handler(self._SETUP_MODE, 'r'):
                        self.save_map()  # Not sure if this is a good idea

    def move_to(self, cur_x, cur_y, x, y):  # mm-ben vannak
        """ Az x, y mm-ben megadott helyhez legközelebbi step-pes helyre megy. """
        cur_x_mstep = self.round_to_mstep(cur_x, self._x_step_mm)
        cur_y_mstep = self.round_to_mstep(cur_y, self._y_step_mm)
        x_mstep = self.round_to_mstep(x, self._x_step_mm)
        y_mstep = self.round_to_mstep(y, self._y_step_mm)
        delta_x_mstep = x_mstep - cur_x_mstep
        delta_y_mstep = y_mstep - cur_y_mstep
        self.move(delta_x_mstep, delta_y_mstep)

    def move_to_mm_version(self, x, y):  # mm-ben vannak
        """ Az x, y mm-ben megadott helyhez legközelebbi step-pes helyre megy. """
        cur_x_mstep = self.thread_safe_coordinate_handler(self.map.get_x, 'r')
        cur_y_mstep = self.thread_safe_coordinate_handler(self.map.get_y, 'r')

        x_mstep = self.round_to_mstep(x, self._x_step_mm)
        y_mstep = self.round_to_mstep(y, self._y_step_mm)

        delta_x_mstep = x_mstep - cur_x_mstep
        delta_y_mstep = y_mstep - cur_y_mstep
        if self.a_malnan_vagyunk:
            self.move(delta_x_mstep, delta_y_mstep)

    def move_to_step_version(self, x_mstep, y_mstep):
        cur_x_mstep = self.thread_safe_coordinate_handler(self.map.get_x, 'r')
        cur_y_mstep = self.thread_safe_coordinate_handler(self.map.get_y, 'r')
        delta_x_mstep = x_mstep - cur_x_mstep
        delta_y_mstep = y_mstep - cur_y_mstep
        if self.a_malnan_vagyunk:
            self.move(delta_x_mstep, delta_y_mstep)

    def handling_motor_moving_popup(self, x, y):
        #epsilon = 0.01          # The prototype motors had  ~100 step/mm => 1 step ~= 0.01 mm
        epsilon = 1 / self.get_x_step_mm()
        its_not_the_same = True
        #print(x, y, self.map.get_x_m(), self.map.get_x(), self.map.get_x_mm())
        if x == self.previous_target_x and y == self.previous_target_y\
                and abs(self.map.get_x_mm() - x) < epsilon and abs(self.map.get_y_mm() - y) < epsilon:
            its_not_the_same = False
        if self.previous_target_y is None or self.previous_target_x is None:
            its_not_the_same = True
        self.set_previous_xy(x, y)
        if its_not_the_same:
            if self.motor_popup_is_allowed:
                # print("popup UPP!")
                comm.motor_moving_popup("Motorok mozgásban.")
        self.motor_popup_is_allowed = True

    def target_move(self, x, y):
        # eddig step-ben volt, mostantól mm-ben lesz
        self.thread_safe_coordinate_handler(self.set_xy_targets, 'w', x, y)
        self.switch_to_target_move()
        self.handling_motor_moving_popup(x,y)


    def validity_check_for_current_pos(self, x, y):
        until_x_min = self.thread_safe_coordinate_handler(self.map.get_until_x_min_border_mm, 'r')
        until_x_plus = self.thread_safe_coordinate_handler(self.map.get_until_x_plus_border_mm, 'r')
        until_y_min = self.thread_safe_coordinate_handler(self.map.get_until_y_min_border_mm, 'r')
        until_y_plus = self.thread_safe_coordinate_handler(self.map.get_until_y_plus_border_mm, 'r')
        cur_x = self.thread_safe_coordinate_handler(self.map.get_x, 'r')
        cur_y = self.thread_safe_coordinate_handler(self.map.get_y, 'r')
        if x > until_x_plus or x < -until_x_min:
            return False
        if y > until_y_plus or y < -until_y_min:
            return False
        return True

    def rotary_move(self, r, p, s, n, c):
        self.roty = True
        # radius,partition, skip, n
        retvalue = True
        xx = None
        yy = None
        if c == 'a':
            self.circle_move(r)
        else:
            points = []
            all_points = []
            for i in range(1, p):
                x = round((r * math.sin(2 * math.pi * i / p)) * 1000) / 1000
                y = round((r * math.cos(2 * math.pi * i / p)) * 1000) / 1000
                if s > 0:
                    if i % s == 0:
                        pass
                    else:
                        points.append([x, y])
                else:
                    points.append([x, y])
                all_points.append([x, y])
            if s == 0:
                points.append([0, r])
            all_points.append([0, r])

            if c == 'n' or c == 'j':
                if n == 0:
                    xx, yy = 0, 0
                else:
                    xx, yy = all_points[n - 1][0], all_points[n - 1][1]
                if self.validity_check_for_current_pos(xx, yy):
                    pass
                else:
                    return False, xx, yy
        return True, xx, yy

    def d_from_circle(self, x, y):
        return abs(math.sqrt(x * x + y * y) - self.radius)

    def stopped_by_hand(self):
        if self.should_end_motormoving_popup:
            comm.end_motor_moving_popup()
            self.should_end_motormoving_popup = False
            return True
        return False

    def go_around(self):
        r = self.thread_safe_coordinate_handler(self.get_radius, 'r')
        self.move_to_step_version(0, r)
        x = 0
        y = r
        while abs(x) < abs(y):
            if self.stopped_by_hand():
                return 0
            x += 1
            if self.d_from_circle(x, y - 1) < self.d_from_circle(x, y):
                self.move(1, -1)
                y -= 1
            else:
                self.move(1, 0)
        """
                        |XXX
                        |   XX
                        |
                        |
                        |
            -------------------------
                        |
                        |
                        |
                        |
                        |

        """
        while abs(y) <= abs(x):
            if self.stopped_by_hand():
                return 0
            y -= 1
            if self.d_from_circle(x - 1, y) < self.d_from_circle(x, y):
                if self.d_from_circle(x - 1, y) < self.d_from_circle(x + 1, y):
                    self.move(-1, -1)
                    x -= 1
                else:
                    self.move(1, -1)
                    x += 1
            else:
                if self.d_from_circle(x, y) < self.d_from_circle(x + 1, y):
                    self.move(0, -1)
                else:
                    self.move(1, -1)
                    x += 1
        """
                                |XXXX
                                |    XXX
                                |       XX
                                |          X
                                |          X
                    ------------------------
                                |          X
                                |          X
                                |       XX
                                |
                                |

        """
        while abs(x) <= abs(y):
            if self.stopped_by_hand():
                return 0
            x -= 1
            if self.d_from_circle(x, y - 1) < self.d_from_circle(x, y):
                if self.d_from_circle(x, y - 1) < self.d_from_circle(x, y + 1):
                    self.move(-1, -1)
                    y -= 1
                else:
                    self.move(-1, 1)
                    y += 1
            else:
                if self.d_from_circle(x, y) < self.d_from_circle(x, y + 1):
                    self.move(-1, 0)
                else:
                    self.move(-1, 1)
                    y += 1

        """
                            |XXXX
                            |    XXX
                            |       XX
                            |          X
                            |          X
                ------------+-----------
                            |          X
                            |          X
                            |       XX
                     XXX    |    XXX
                        XXXX|XXXX

        """
        while abs(y) <= abs(x):
            if self.stopped_by_hand():
                return 0
            y += 1
            if self.d_from_circle(x - 1, y) < self.d_from_circle(x, y):
                if self.d_from_circle(x - 1, y) < self.d_from_circle(x + 1, y):
                    self.move(-1, 1)
                    x -= 1
                else:
                    self.move(1, 1)
                    x += 1
            else:
                if self.d_from_circle(x, y) < self.d_from_circle(x + 1, y):
                    self.move(0, 1)
                else:
                    self.move(1, 1)
                    x += 1
        """
                            |XXXX
                            |    XXX
                  XX        |       XX
                X           |          X
                X           |          X
                ------------+-----------
                X           |          X
                X           |          X
                  XX        |       XX
                     XXX    |    XXX
                        XXXX|XXXX

        """
        while x < 0:
            if self.stopped_by_hand():
                return 0
            x += 1
            if self.d_from_circle(x, y + 1) < self.d_from_circle(x, y):
                self.move(1, 1)
                y += 1
            else:
                self.move(1, 0)
        """
                        XXXX|XXXX
                     XXX    |    XXX
                  XX        |       XX
                 X          |          X
                 X          |          X
                 -----------+-----------
                 X          |          X
                 X          |          X
                  XX        |       XX
                     XXX    |    XXX
                        XXXX|XXXX

        """
        self.save_map()

    def set_radius(self, r):
        self.radius = self.round_to_mstep(float(r), self._x_step_mm)

    def get_radius(self):
        return self.radius

    def circle_move(self, r):
        until_x_min = self.thread_safe_coordinate_handler(self.map.get_until_x_min_border, 'r')
        until_x_plus = self.thread_safe_coordinate_handler(self.map.get_until_x_plus_border, 'r')
        until_y_min = self.thread_safe_coordinate_handler(self.map.get_until_y_min_border, 'r')
        until_y_plus = self.thread_safe_coordinate_handler(self.map.get_until_y_plus_border, 'r')

        self.thread_safe_coordinate_handler(self.set_radius, 'w', r)
        r = self.thread_safe_coordinate_handler(self.get_radius, 'r')
        safety = 50
        if r > until_x_min + safety or r > until_x_plus + safety or \
                r > until_y_min + safety or r > until_y_plus + safety:
            comm.popup("Túl nagy lenne a kör!")
        else:
            self.switch_to_rotation_move()
            comm.motor_moving_popup("Kör...")

    def go_this_much(self, x, y):
        curx = self.thread_safe_coordinate_handler(self.map.get_x_mm, 'r')
        cury = self.thread_safe_coordinate_handler(self.map.get_y_mm, 'r')
        self.thread_safe_coordinate_handler(self.set_xy_targets, 'w', curx + x, cury + y)
        self.switch_to_target_move()

    def stop_checking(self):
        while True:
            sleep(0.1)
            if self.stop_button.get_value() == 0:
                self.turn_off_both()

    def null_check(self):
        if self.null_button.get_value() == 0:
            self.thread_safe_coordinate_handler(self.map.set_absolute_middle, 'r')
