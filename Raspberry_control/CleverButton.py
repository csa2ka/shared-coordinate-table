from Raspberry_control.Buttonlike import Buttonlike
from Raspberry_control.Tickable import Tickable


class CleverButton(Buttonlike, Tickable):
    LONG_PRESS_MIN_TIME = 0.5

    def __init__(self, parent, pin, pud):
        super().__init__(parent, pin, pud)
        self.pressed_time = 0
        self.long_pressed_func = None
        self.short_pressed_func = None
        print("cleverbutton pin:", pin)

    def tick(self, delta_time):
        if self.is_pressed():
            self.pressed_time += delta_time
            if self.pressed_time >= self.LONG_PRESS_MIN_TIME:
                self.call_long_func()
        else:
            if self.pressed_time > 0:  # pressing occurred
                if self.pressed_time < self.LONG_PRESS_MIN_TIME:
                    self.call_short_func()
                    #print ("|||||:::", self.pressed_time)

            self.pressed_time = 0

    def call_long_func(self):
        print("long")
        if self.long_pressed_func is not None:
            self.long_pressed_func()

    def call_short_func(self):
        print("short")
        if self.short_pressed_func is not None:
            self.short_pressed_func()

    def register_long_pressed_func(self, func):
        print("registered long press", self._pin)
        self.long_pressed_func = func

    def register_short_pressed_func(self, func):
        
        print("registered short press", self._pin)
        self.short_pressed_func = func
