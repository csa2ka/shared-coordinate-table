from abc import ABC, abstractmethod


class Tickable(ABC):

    @abstractmethod
    def tick(self, delta_time):
        pass
