import RPi.GPIO as GPIO


class Buttonlike:
    """
        GPIO szinten fogja fel a gombot, de végálláskapcsoló érzékelője is ilyen
    """
    _pin = 100  # valami fura kezdeti érték ami az init-ben úgyis felül íródik

    def __init__(self, parent, pin, pud):
        # print("joystickinit")
        self._parent = parent # Raspberrycontroller
        self._pin = pin
        self._pud = pud
        if self._parent.a_malnan_vagyunk:
            if pud == 1:
                GPIO.setup(self._pin, GPIO.IN,
                           pull_up_down=GPIO.PUD_UP)  # TODO lehet még beállítgatni, hogy ezek mik legyenek lo/hi
            if pud == 0:
                GPIO.setup(self._pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    def get_value(self):
        """Leolvassa az input pin-t az irányt"""
        if self._parent.a_malnan_vagyunk:
            return GPIO.input(self._pin)
        else:
            return self._pud

    def is_pressed(self):
        """Leolvassa az input pin-t az irányt"""
        if self._parent.a_malnan_vagyunk:
            if self._pud == 1:
                return GPIO.input(self._pin) < 0.3
            if self._pud == 0:
                return GPIO.input(self._pin) > 0.7
        else:
            return False
