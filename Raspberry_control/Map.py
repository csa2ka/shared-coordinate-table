import sys
from Memory.MemoryManager import MemoryManager as mm

from utils.balazs_funcs import spec_format


class Map:
    """
    Az asztal pozícióját tárolja ezenkívül a határokat és a közepeket vizsgálja.
    """
    origo_mode = "mid"
    safety_steps = 10
    CUR_X = 0  # step                #a fene, ezek nem is konstansok
    CUR_Y = 0
    CUR_X_M = 0  # microstep
    CUR_Y_M = 0
    LENGTH = 0
    HEIGHT = 0
    X_MM = 0
    Y_MM = 0
    X_MM_RELATIVE = 0
    Y_MM_RELATIVE = 0
    X_MM_REL_OFFSET = 0
    Y_MM_REL_OFFSET = 0
    UNTIL_X_MIN_BORDER = 0
    UNTIL_X_PLUS_BORDER = 0
    UNTIL_Y_MIN_BORDER = 0
    UNTIL_Y_PLUS_BORDER = 0
    UNTIL_X_MIN_BORDER_MM = 0
    UNTIL_X_PLUS_BORDER_MM = 0
    UNTIL_Y_MIN_BORDER_MM = 0
    UNTIL_Y_PLUS_BORDER_MM = 0
    OVERTRAVEL_SWITCH = True
    FORMAT = 3

    def __init__(self, l, h, rpic):
        # print("mapinit")
        Map.LENGTH = l
        Map.HEIGHT = h
        self.parent = rpic

    def print_coordinates(self):
        print("CURR_X", self.CUR_X)
        print("CURR_Y", self.CUR_Y)
        print("CURR_X_M", self.CUR_X_M)
        print("CURR_Y_M", self.CUR_Y_M)
        print("X_MM", self.X_MM)
        print("Y_MM", self.Y_MM)

    def save(self):
        mm.get_instance().change(mm.FROM_X_MIN_BORDER, spec_format(self.UNTIL_X_MIN_BORDER_MM, self.FORMAT))
        mm.get_instance().change(mm.FROM_X_PLUS_BORDER, spec_format(self.UNTIL_X_PLUS_BORDER_MM, self.FORMAT))
        mm.get_instance().change(mm.FROM_Y_MIN_BORDER, spec_format(self.UNTIL_Y_MIN_BORDER_MM, self.FORMAT))
        mm.get_instance().change(mm.FROM_Y_PLUS_BORDER, spec_format(self.UNTIL_Y_PLUS_BORDER_MM, self.FORMAT))
        mm.get_instance().change(mm.CUR_X_POS, spec_format(self.X_MM, self.FORMAT))
        mm.get_instance().change(mm.CUR_Y_POS, spec_format(self.Y_MM, self.FORMAT))

    def set_absolute_middle(self):
        self.UNTIL_X_MIN_BORDER += self.CUR_X
        self.UNTIL_X_PLUS_BORDER -= self.CUR_X
        self.UNTIL_Y_MIN_BORDER += self.CUR_Y
        self.UNTIL_Y_PLUS_BORDER -= self.CUR_Y
        self.CUR_X = 0
        self.CUR_Y = 0
        self.refresh_mms()
        return 0  # 0 paraméteres, ezért olvasó

    def set_abs_x_middle(self):
        self.UNTIL_X_MIN_BORDER += self.CUR_X
        self.UNTIL_X_PLUS_BORDER -= self.CUR_X
        self.CUR_X = 0
        self.refresh_mms()
        return 0  # 0 paraméteres, ezért olvasó

    def set_abs_y_middle(self):
        self.UNTIL_Y_MIN_BORDER += self.CUR_Y
        self.UNTIL_Y_PLUS_BORDER -= self.CUR_Y
        self.CUR_Y = 0
        self.refresh_mms()
        return 0  # 0 paraméteres, ezért olvasó

    def set_borders_to_almost_infinity(self):
        # temporary stuff for testing
        self.UNTIL_X_MIN_BORDER = sys.maxsize
        self.UNTIL_X_PLUS_BORDER = sys.maxsize
        self.UNTIL_Y_MIN_BORDER = sys.maxsize
        self.UNTIL_Y_PLUS_BORDER = sys.maxsize

    def refresh_mms(self):
        step_per_mm_x = self.parent.get_x_step_mm() * self.parent.get_microstep_software()
        step_per_mm_y = (self.parent.get_y_step_mm() * self.parent.get_microstep_software())
        self.UNTIL_X_MIN_BORDER_MM = self.UNTIL_X_MIN_BORDER / step_per_mm_x
        self.UNTIL_X_PLUS_BORDER_MM = self.UNTIL_X_PLUS_BORDER / step_per_mm_x
        self.UNTIL_Y_MIN_BORDER_MM = self.UNTIL_Y_MIN_BORDER / step_per_mm_y
        self.UNTIL_Y_PLUS_BORDER_MM = self.UNTIL_Y_PLUS_BORDER / step_per_mm_y
        self.X_MM = self.CUR_X / step_per_mm_x
        self.Y_MM = self.CUR_Y / step_per_mm_y
        self.adjust_relative_coordinates()


    def set_current_x_to_relative_zero(self):
        self.X_MM_RELATIVE = 0
        self.X_MM_REL_OFFSET = self.X_MM

    def set_current_y_to_relative_zero(self):
        self.Y_MM_RELATIVE = 0
        self.Y_MM_REL_OFFSET = self.Y_MM

    def adjust_relative_coordinates(self):
        self.X_MM_RELATIVE = self.X_MM - self.X_MM_REL_OFFSET
        self.Y_MM_RELATIVE = self.Y_MM - self.Y_MM_REL_OFFSET

    def get_x_mm(self):
        return self.X_MM

    def get_y_mm(self):
        return self.Y_MM

    def get_x_mm_relative(self):
        return self.X_MM_RELATIVE

    def get_y_mm_relative(self):
        return self.Y_MM_RELATIVE

    def set_l_h(self, l, h):
        self.LENGTH = l
        self.HEIGHT = h

    def get_l(self):
        return self.LENGTH

    def get_h(self):
        return self.HEIGHT

    def set_overtravel_switch(self, o_s):
        self.OVERTRAVEL_SWITCH = o_s

    def set_coordinates(self, x, y):
        self.CUR_X = x
        self.CUR_Y = y
        self.refresh_mms()

    def x_add(self, x):
        self.CUR_X += x
        self.refresh_mms()

    def y_add(self, y):
        self.CUR_Y += y
        self.refresh_mms()

    def get_x(self):
        return self.CUR_X

    def get_y(self):
        return self.CUR_Y

    def set_x(self, x):
        self.CUR_X = x
        self.refresh_mms()

    def set_y(self, y):
        self.CUR_Y = y
        self.refresh_mms()

    def set_coordinates_mm(self, x, y):
        self.CUR_X_M = x
        self.CUR_Y_M = y
        self.refresh_mms()

    def x_m_add(self, x):
        self.CUR_X_M += x
        self.refresh_mms()

    def y_m_add(self, y):
        self.CUR_Y_M += y
        self.refresh_mms()

    def get_x_m(self):
        return self.CUR_X_M

    def get_y_m(self):
        return self.CUR_Y_M

    def set_origo_mode(self, s):
        self.origo_mode = s

    def get_origo_mode(self):
        return self.origo_mode

    def set_safety_steps(self, s):
        self.safety_steps = s

    def get_safety_steps(self):
        return self.safety_steps

    def set_until_x_min_border(self, value):
        self.UNTIL_X_MIN_BORDER = value

    def set_until_x_plus_border(self, value):
        self.UNTIL_X_PLUS_BORDER = value

    def set_until_y_min_border(self, value):
        self.UNTIL_Y_MIN_BORDER = value

    def set_until_y_plus_border(self, value):
        self.UNTIL_Y_PLUS_BORDER = value

    def get_until_x_min_border(self):
        # if not self.OVERTRAVEL_SWITCH:
        #    return
        # else:
        #    return self.UNTIL_X_MIN_BORDER
        return self.UNTIL_X_MIN_BORDER

    def get_until_x_plus_border(self):
        return self.UNTIL_X_PLUS_BORDER

    def get_until_y_min_border(self):
        return self.UNTIL_Y_MIN_BORDER

    def get_until_y_plus_border(self):
        return self.UNTIL_Y_PLUS_BORDER

    def get_until_x_min_border_mm(self):
        # if not self.OVERTRAVEL_SWITCH:
        #    return
        # else:
        #    return self.UNTIL_X_MIN_BORDER_MM
        return self.UNTIL_X_MIN_BORDER_MM


    def get_until_x_plus_border_mm(self):
        return self.UNTIL_X_PLUS_BORDER_MM

    def get_until_y_min_border_mm(self):
        return self.UNTIL_Y_MIN_BORDER_MM

    def get_until_y_plus_border_mm(self):
        return self.UNTIL_Y_PLUS_BORDER_MM
