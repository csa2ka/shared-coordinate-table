#!/bin/bash

crontab -l | { cat; echo "* * * * * ( sleep 0  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 5  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 10  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 15  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 20  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 25  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 30  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 35  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 40  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 45  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 50  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)
* * * * * ( sleep 55  ; /home/pi/CoordinateTable/filetransfer/application_start.sh)"; } | crontab -
