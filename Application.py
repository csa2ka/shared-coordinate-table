#!/usr/bin/python3
from tkinter import Tk
from Raspberry_control.RaspberryController import RaspberryController
from GUI.Screens.Screen import Screen
from GUI.Screens.ScreenType import ScreenType
from Memory.MemoryManager import MemoryManager
from Communicator import Communicator
from GUI.Screens.TestScreen import TestScreen
from GUI.Screens.SettingsScreen1 import SettingsScreen1
from GUI.Screens.SettingsScreen2 import SettingsScreen2
from GUI.Screens.SettingsScreen3 import SettingsScreen3
from GUI.Screens.CoordinateModeScreen import CoordinateModeScreen
from GUI.Screens.RotarytableModeScreen import RotarytableModeScreen
from GUI.Screens.CamndrillModeScreen import CamndrillModeScreen
from GUI.Screens.MainMenu import MainMenu
from GUI.Screens.KeyboardScreen import KeyboardScreen
from GUI.Screens.NumpadScreen import NumpadScreen
from GUI.Screens.AskSetupScreen import AskSetupScreen
from GUI.Screens.ChooseSetupScreen import ChooseSetupScreen
from GUI.Screens.ManualSetupScreen import ManualSetup
from GUI.Screens.MicrostepSettingSetupScreen import MicrostepSettingSetupScreen
from GUI.Screens.PopupScreen import PopupScreen
from GUI.Screens.MotorMovingPopupScreen import MotorMovingPopupScreen
from GUI.Repeat import Repeat
from GUI.StyleSettings import style_init

"""
import time

N = 100
start = time.time()
for i in range(N):
    time.sleep(0.00001)
end = time.time()
print(end-start)
"""

# RaspberryController:
rpi = RaspberryController()

# Application:
app = Tk()
app.overrideredirect(1)  # TODO to remove titlebar
app.geometry("%dx%d+0+0" % (Screen.WIDTH, Screen.HEIGHT))

# MemoryManager:
mm = MemoryManager("settings.txt", "point_groups.txt", "rotary_tables.txt")

# Styling
style_init()

# The Screen class gets functions so it's children will be able to reach them
Screen.set_up_commands(Communicator.keyboard_come_out, Communicator.numpad_come_out)
Screen.set_rpi(rpi)
# Creating screens
# test_screen = TestScreen(app, layout="grid")
settings1_screen = SettingsScreen1(app)
settings2_screen = SettingsScreen2(app)
settings3_screen = SettingsScreen3(app)
coordinate_mode_screen = CoordinateModeScreen(app, rpi)
rotarytable_mode_screen = RotarytableModeScreen(app, rpi)
camndrill_mode_screen = CamndrillModeScreen(app, rpi)
main_menu = MainMenu(app)
numpad_screen = NumpadScreen(app)
keyboard_screen = KeyboardScreen(app)
ask_setup_screen = AskSetupScreen(app)
choose_setup_screen = ChooseSetupScreen(app)
microstep_setting_setup_screen = MicrostepSettingSetupScreen(app)
manual_setup_screen = ManualSetup(app)
popup_screen = PopupScreen(app)
motor_moving_popup_screen = MotorMovingPopupScreen(app)



screens = [main_menu, settings1_screen, settings2_screen, settings3_screen, numpad_screen,   # test_screen
           coordinate_mode_screen, keyboard_screen, rotarytable_mode_screen, camndrill_mode_screen, ask_setup_screen,
           choose_setup_screen, manual_setup_screen, microstep_setting_setup_screen, popup_screen,
           motor_moving_popup_screen]



# Communicator class init
Communicator.class_init(screens, rpi, app)
settings1_screen.build_if_not_built()
settings2_screen.build_if_not_built()
settings3_screen.build_if_not_built()
Communicator.switch_screen(ScreenType.ASK_SETUP)

# Repeat class init
Repeat.on_tick(app)

# Blocking
app.mainloop()
