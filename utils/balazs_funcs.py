import tkinter as tk
import tkinter.ttk as ttk
from GUI.Screens.Screen import Screen
import os
import shutil
from pathlib import Path, PureWindowsPath
import time


def one_zero_transistor(one_or_zero, direction):
    """So we have a 0, or zero stuff, and we have a 1 or -1 stuff, and we want to get a 0,1 based switching the 1
    and the 0 if -1 not switching if 1, I wanna use math instead of if so:
            0 1= 0
            1 1= 1
            0 -1= 1
            1 -1= 0"""
    # if one_or_zero != 1 or one_or_zero != 0 or direction != 1 or direction != -1:  # shouldn't I do exception instead
    #    print("Error in a one_zero_transistor: the parameters aren't stimpt")
    #    return 10000000000
    # print(one_or_zero, direction, int((2 * (one_or_zero - 0.5) * direction) / 2 + 0.5))
    return int((2 * (one_or_zero - 0.5) * direction) / 2 + 0.5)


# in a range of 0-6 it formats a float to have that many decimals after dot, returns str of such
def spec_format(float_boy, this_many_digits_after_dot):
    float_boy = float(float_boy)
    this_many_digits_after_dot = int(this_many_digits_after_dot)
    if this_many_digits_after_dot < 0:
        this_many_digits_after_dot = 0
    if this_many_digits_after_dot > 6:
        this_many_digits_after_dot = 6
    this_many_digits_after_dot = str(this_many_digits_after_dot)
    format_text = "{:3." + this_many_digits_after_dot + "f}"
    return format_text.format(float_boy)
    # print(format_text.format(float_boy))
    # print(type(format_text.format(float_boy)))


def copyfile(from_, to):
    with open(from_) as f:
        lines = f.readlines()
        # lines = [l for l in lines if "ROW" in l]
        with open(to, "w") as f1:
            f1.writelines(lines)


def copy_memory_files(to_path):
    to_settings = universal_path(to_path + "\\settings.txt")
    to_point_groups = universal_path(to_path + "\\point_groups.txt")
    to_rotary_tables = universal_path(to_path + "\\rotary_tables.txt")

    copyfile(universal_path("settings.txt"), to_settings)
    copyfile(universal_path("point_groups.txt"), to_point_groups)
    copyfile(universal_path("rotary_tables.txt"), to_rotary_tables)


def universal_path(file_path):
    filename_in = PureWindowsPath(file_path)
    return str(Path(filename_in))


def is_this_exist(file_path):
    # I've explicitly declared my path as being in Windows format, so I can use forward slashes in it.
    filename_in = PureWindowsPath(file_path)
    # filename_out = PureWindowsPath("settings.txt")

    # Convert path to the right format for the current operating system
    correct_path_in = Path(filename_in)
    # correct_path_out = Path(filename_out)

    print(correct_path_in)
    # print(correct_path_out)

    if os.path.exists(correct_path_in):
        # print("Létezik!")
        pass

    # if os.path.exists("alma.txt"):
    #    print("a.txt")


def stringing_date(number):
    if number < 10:
        return "0" + str(number)
    else:
        return str(number)


def time_text():
    timi = time.localtime()
    string = stringing_date(timi.tm_year) \
             + stringing_date(timi.tm_mon) \
             + stringing_date(timi.tm_mday) \
             + stringing_date(timi.tm_hour) \
             + stringing_date(timi.tm_min) \
             + stringing_date(timi.tm_sec)
    return string


def backupsave():
    # getsmallest
    folder_path = "backup"
    smallest = "99991231245959"
    for i in os.listdir(folder_path):
        try:
            if int(i) < int(smallest):
                smallest = i
        except ValueError:
            pass
    path = folder_path + "\\" + smallest
    # print(path)
    # renamesmallest
    copy_memory_files(path)
    # savestuffthere
    os.rename(universal_path(path), universal_path(folder_path + "\\" + time_text()))


def motor_moving_message(motor_moving_warning, msg):
    width = 800
    height = 480
    large_font = ("Arial", 22)
    popup = tk.Tk()
    popup.overrideredirect(1)
    popup.geometry("%dx%d+0+0" % (width, height))
    popup.wm_title("Message")
    motor_moving_warning = popup
    label_wrapper = ttk.Frame(popup, width=width, height=100)
    label_wrapper.place(relx=0.5, rely=0.3, anchor=tk.CENTER)
    label_wrapper.pack_propagate(0)

    label = ttk.Label(label_wrapper, font=large_font, text=msg)
    label.pack(side=tk.BOTTOM)

    size_wrapper = ttk.Frame(popup, width=150, height=100)
    size_wrapper.place(relx=0.5, rely=0.8, anchor=tk.CENTER)
    size_wrapper.pack_propagate(0)
    B1 = tk.Button(size_wrapper, text="Állj!", command=popup.destroy)
    B1.pack(side=tk.BOTTOM, expand=True, fill=tk.BOTH)
    B1['font'] = large_font

    B1.bind("<Button-1>", Screen.pitty)

    popup.mainloop()
