import tkinter as tk
import tkinter.ttk as ttk

from GUI.Screens.Screen import Screen


def new_unique_name(name_list, prefix):
    suffix = 1
    while prefix+str(suffix) in name_list:
        suffix += 1
    return prefix+str(suffix)


def fraction_string_to_float(fraction):
    try:
        return float(fraction)
    except ValueError:
        [numerator, denominator] = fraction.split('/')
        return float(numerator)/float(denominator)


def number_digit_length(number_string):
    lenge = len(number_string)
    if '.' in number_string:
        lenge -= 1
    if '-' in number_string:
        lenge -= 1
    return lenge


def popup_message(msg):
    width = 800
    height = 480
    large_font = ("Arial", 22)
    popup = tk.Tk()
    popup.overrideredirect(1)
    popup.geometry("%dx%d+0+0" % (width, height))
    popup.wm_title("Message")
    label_wrapper = ttk.Frame(popup, width=width, height=100)
    label_wrapper.place(relx=0.5, rely=0.3, anchor=tk.CENTER)
    label_wrapper.pack_propagate(0)

    label = ttk.Label(label_wrapper, font=large_font, text=msg)
    label.pack(side=tk.BOTTOM)

    size_wrapper = ttk.Frame(popup, width=150, height=100)
    size_wrapper.place(relx=0.5, rely=0.8, anchor=tk.CENTER)
    size_wrapper.pack_propagate(0)
    B1 = tk.Button(size_wrapper, text="OK", command=popup.destroy)
    B1.pack(side=tk.BOTTOM, expand=True, fill=tk.BOTH)
    B1['font'] = large_font

    B1.bind("<Button-1>", Screen.pitty)

    popup.mainloop()
